# -*- coding: utf-8 -*-
import random
import string
from flask import jsonify, request, g, abort
from sqlalchemy.sql import and_, or_, desc
from odyssey import app, db, auth
from odyssey.utils import send_fcm_message, format_phone_number, send_sms_to_phone
from odyssey.groups.models import Group, GroupUserMapping
from odyssey.user.models import User, Admin, Agent
from odyssey.tasks.models import Task
from odyssey.customer.models import Customer, Address
from odyssey.customer.models import CustomerAddress, CustomerAdmin
from odyssey.events.models import Event, UserEvent
from odyssey.customer.views import add_address, add_customer, edit_customer_at_edit_task, edit_address
import datetime
from Constants import *
from from_hell import ConvertingJsonForms


@app.route('/v3/tasks', methods=['POST'])
@auth.login_required
def create_task():
    """
    Creates task. Adds to activity log
    :return: 200 with Task info
    """
    user = g.user
    task = Task(user)
    if not task:
        return not_found_response()
    task = fill_task(task, request, user)
    if not task:
        return not_found_response()
    task.make_url()

    db.session.add(task)
    db.session.commit()
    send_message_to_agent_related_to_task(task)
    if not task.group_id:
        task_event_and_map_with_users(task, user, CREATED)
    elif not task.admin:
        task_event_and_map_with_users(task, user, ADDED)
    elif not task.agent:
        task_event_and_map_with_users(task, user, ADDED)
    else:
        task_event_and_map_with_users(task, user, ASSIGNED)

    return jsonify(
        status=200,
        message='Ok',
        task=task.serialize
    )


@app.route('/v3/tasks/<task_id>/edit', methods=['PUT'])
@auth.login_required
def edit_task(task_id):
    """
    Edit Task.
    Checks various conditions if task is editable.
    Add message to activity log.
    :param task_id:
    :return:
    """
    task = Task.query.get(task_id)
    user = g.user

    if not task:
        return jsonify(
            status=404,
            message="Not Found"
        )

    # Can not be edited if task is among these
    if task.status in ['COMPLETED', 'CANCELLED', 'REJECTED']:
        return jsonify(
            status=404,
            message="This Task can't be edited anymore. It is either completed or cancelled."
        )

    is_agent_assigned = task.agent

    # Checks if admin assigned, task are in same group
    if task.group_id is True:
        if not check_admin_related_to_task(task, user):
            return forbidden_response()

    # Fill details in task using request object
    task = fill_task(task, request, g.user)

    # Some conditions must have failed
    if not task:
        return jsonify(
            status=404,
            message="Not Found or Some parts can't be edited"
        )
    # send_message_to_agent_related_to_task(task)
    task_event_and_map_with_users(task, user, EDITED)

    # if agent has been assigned in this edit. Earlier no agent was assigned
    if not is_agent_assigned:
        if task.admin:
            task_event_and_map_with_users(task, user, ASSIGNED)

    return jsonify(
        status=200,
        message="Ok",
        task=task.serialize
    )


@app.route('/v3/tasks/<task_id>', methods=['GET'])
@auth.login_required
def fetch_a_task(task_id):
    """
    Fetch task for Admin,
    Checks authorization
    :param task_id:
    :return:
    """
    task = Task.query.get(task_id)
    user = g.user
    user_in_groups = []
    for group_map in user.group_maps:
        user_in_groups.append(group_map.group_id)
    if not task:
        return not_found_response()

    if user.username in [task.creator, task.admin, task.agent]:
        return jsonify(
            status=200,
            message="Ok",
            task=task.serialize
        )

    if task.group_id in user_in_groups:
        return jsonify(
            status=200,
            message="Ok",
            task=task.serialize
        )

    return not_found_response()


@app.route('/v3/tasks/agent', methods=['GET'])
@auth.login_required
def fetch_all_tasks_for_agent():
    user = g.user
    if user.is_agent:
        tasklist1 = Task.query.filter(
            Task.agent == user.username,
            or_(
                Task.status == 'ASSIGNED'
            )
        ).order_by(desc(Task.assigned_on))

        tasklist2 = Task.query.filter(
            Task.agent == user.username,
            or_(
                Task.status == 'STARTED'
            )
        ).order_by(desc(Task.actual_start_time))
    else:
        return not_found_response()

    task_details = [task.serialize for task in tasklist2]
    task_details += [task.serialize for task in tasklist1]
    return jsonify(
        status=200,
        message='Ok',
        tasks=task_details
    )


@app.route('/v3/tasks/admin', methods=['GET'])
@auth.login_required
def fetch_all_tasks_for_admin():
    user = g.user
    args_dict = fill_dict_with_args(request, user)
    task_list = get_task_list(args_dict)
    status_search = args_dict.get('status_search')
    if len(task_list) > 0:
        # if status_search == 'COMPLETED':
        #     tasks = [task.serialize for task in task_list if task.status == 'COMPLETED']
        # elif status_search == 'STARTED':
        #     tasks = [task.serialize for task in task_list if task.status == 'STARTED']
        # else:
        #     tasks = [task.serialize for task in task_list]
        tasks = [task.serialize for task in task_list]
        return jsonify(
            status=200,
            message='Ok',
            task_count=len(task_list),
            tasks=tasks
        )
    else:
        # earlier not_found response was used to sent
        return jsonify(
            status=200,
            message='Ok',
            task_count=len(task_list),
            tasks=[]
        )


# TODO: Many more condition needed
@app.route('/v3/tasks/<task_id>/delete', methods=['DELETE'])
@auth.login_required
def delete_task(task_id):
    task = Task.query.get(task_id)
    user = g.user
    if not task:
        return not_found_response()
    if not check_admin_related_to_task(task, user):
        return forbidden_response()
    if task.status in ['STARTED', 'COMPLETED']:
        return not_found_response()

    task_event_and_map_with_users(task, user, DELETED)
    db.session.delete(task)
    db.session.commit()
    return jsonify(
        status=200,
        message='Ok'
    )


# TODO: What is the use?
@app.route('/v3/tasks/<task_id>/url', methods=['POST'])
@auth.login_required
def share_url(task_id):
    task = Task.query.get(task_id)
    if not task:
        return not_found_response()
    if not check_admin_related_to_task(task, g.user):
        return forbidden_response()


@app.route('/v3/tasks/<task_id>/start', methods=['POST'])
@auth.login_required
def start_task(task_id):
    task = Task.query.get(task_id)
    if not task:
        return not_found_response()
    if task.agent != g.user.username:
        return not_found_response()

    if task.status == 'STARTED':
        return all_well_response()
    if task.status != 'ASSIGNED':
        return not_found_response()

    loc = get_latlong(request.json.get('location'))
    start_time_from_agent = date_finder(request.json.get('time'))
    task.update_status_start_task(loc[0], loc[1], start_time_from_agent)
    db.session.commit()
    task_event_and_map_with_users(task, g.user, STARTED)
    send_message_to_admin_on_task_action(task, STARTED)
    # if task.customer:
    #     customer = Customer.query.get(task.customer)
    #     if customer.username:
    #         url_share_sms(task, customer.username)
    return all_well_response()


@app.route('/v3/tasks/<task_id>/complete', methods=['POST'])
@auth.login_required
def complete_task(task_id):
    task = Task.query.get(task_id)
    if not task:
        return not_found_response()
    if check_agent_related_to_task(task, g.user) is False:
        return forbidden_response()

    if task.status == 'COMPLETED':
        return all_well_response()
    if task.status != 'STARTED':
        return not_found_response()

    loc = get_latlong(request.json.get('location'))
    end_time_from_agent = date_finder(request.json.get('time'))
    task.update_status_complete_task(loc[0], loc[1], end_time_from_agent)
    pic_url = request.json.get('pictures')
    sig_url = request.json.get('signature')
    comment_by_agent = request.json.get('comment_by_agent')
    task.details_by_agent_on_completion(pic_url,
                                        sig_url,
                                        comment_by_agent)
    db.session.commit()
    task_event_and_map_with_users(task, g.user, COMPLETED)
    send_message_to_admin_on_task_action(task, COMPLETED)
    return all_well_response()


@app.route('/v3/tasks/<task_id>/cancel', methods=['POST'])
@auth.login_required
def cancel_task(task_id):
    task = Task.query.get(task_id)
    if not task:
        return not_found_response()
    if check_agent_related_to_task(task, g.user) is False:
        return forbidden_response()

    if task.status == 'CANCELLED':
        return all_well_response()
    if task.status != 'STARTED':
        return not_found_response()

    loc = get_latlong(request.json.get('location'))
    end_time_from_agent = date_finder(request.json.get('time'))
    task.update_status_cancel_task(loc[0], loc[1], end_time_from_agent)
    db.session.commit()
    task_event_and_map_with_users(task, g.user, CANCELLED)
    send_message_to_admin_on_task_action(task, CANCELLED)
    return all_well_response()


@app.route('/v3/tasks/<task_id>/rejected', methods=['POST'])
@auth.login_required
def reject_task(task_id):
    task = Task.query.get(task_id)
    if not task:
        return not_found_response()
    if check_agent_related_to_task(task, g.user) is False:
        return forbidden_response()
    if task.status not in ['UNASSIGNED', 'ASSIGNED']:
        return forbidden_response()

    task.update_status_rejected_task()
    db.session.commit()
    task_event_and_map_with_users(task, g.user, REJECTED)
    send_message_to_admin_on_task_action(task, REJECTED)
    return all_well_response()


def not_found_response():
    return jsonify(
        status=404,
        message=NOT_FOUND
    )


def all_well_response():
    return jsonify(
        status=200,
        message=ALL_WELL
    )


def bad_request_response():
    return jsonify(
        status=400,
        message=BAD_REQUEST
    )


def forbidden_response():
    return jsonify(
        status=403,
        message=FORBIDDEN
    )


# Used in Add/Edit task. Checks authorization. Creates Customer/Address link them with Current User
def fill_task(task, request, current_user):

    task.title = request.json.get(TASK_TITLE)
    task.type = request.json.get(TASK_TYPE)
    group_id = request.json.get(TASK_GROUP)
    admin_username = request.json.get(TASK_ADMIN)
    agent_username = request.json.get(TASK_AGENT)
    customer = None
    address = None
    if task_has_been_started(task, group_id, agent_username):
        return False

    if check_credentials_in_task(task, current_user, group_id, admin_username, agent_username) is False:
        return False
    customer_detail = request.json.get('customer_detail')

    if customer_detail:
        customer = customer_detail.get('id')

    address_detail = request.json.get('address_detail')
    if address_detail:
        address = address_detail.get('id')
    
    if customer is not None:
        task.customer = int(customer)
        edit_customer_at_edit_task(task.customer, request)
    else:
        customer = add_customer(request)
        task.customer = customer.id
        check_customeradmin(current_user, customer)

    if address is not None:
        task.address = int(address)
        address = Address.query.get(task.address)
        edit_address(request, address)
    else:
        address = add_address(request)
        task.address = address.id
        if type(customer) is unicode or type(customer) is int:
            customer = Customer.query.get(customer)
        check_customeraddress(customer, address)

    if task.agent is not None:
        if task.status == 'STARTED':
            pass
        else:
            task.status = 'ASSIGNED'
    else:
        task.status = 'UNASSIGNED'

    # task.agent = request.json.get(TASK_AGENT)
    task.note = request.json.get(NOTE)
    if request.json.get(TASK_END_TIME) is not None:
        task.estimated_end_time = date_finder(request.json.get(TASK_END_TIME))
    db.session.commit()
    return task


def check_customeradmin(current_user, customer):
    cust_admin = CustomerAdmin.query.filter_by(
        cust=customer.id,
        admin=current_user.id
    ).first()
    if not cust_admin:
        ca = CustomerAdmin(customer.id, current_user.id)
        db.session.add(ca)
        db.session.commit()


def check_customeraddress(customer, address):
    cust_addr = CustomerAddress.query.filter_by(
        cust=customer.id,
        addr=address.id
    ).first()
    if not cust_addr:
        ca = CustomerAddress(customer.id, address.id)
        db.session.add(ca)
        db.session.commit()


def check_admin_related_to_task(task, user):
    groups = user.group_maps
    group_ids = [group.group_id for group in groups]
    if task.group_id in group_ids:
        return True
    return False


def check_agent_related_to_task(task, user):
    if task.agent == user.username:
        return True
    return False


def check_user_belongs_to_group(user, group):
    if group in user.groups:
        return True
    return False


def check_if_group_exist(group_id):
    if group_id is not None:
        group_id = int(group_id)
        group_obj = Group.query.get(group_id)
        return group_obj
    return False


def check_if_user_exist(username):
    if username is not None:
        user_obj = User.query.filter_by(username=str(username)).first()
        return user_obj
    return False


def check_if_agent_exist(username):
    if username is not None:
        user_obj = User.query.filter_by(username=str(username)).first()
        if user_obj:
            return True
    return False


def check_credentials_in_task(task, current_user, group_id, admin_username, agent_username):
    group_obj = check_if_group_exist(group_id)
    if group_obj:
        group_current_user = GroupUserMapping.query.filter_by(
            is_admin=True,
            user_id=current_user.id,
            group_id=group_obj.id
        ).first()
        if group_current_user:
            task.group_id = group_obj.id
            admin_obj = check_if_user_exist(admin_username)
            if admin_obj:
                group_user_admin = GroupUserMapping.query.filter_by(
                    is_admin=True,
                    user_id=admin_obj.id,
                    group_id=group_obj.id
                ).first()
                if group_user_admin:
                    task.admin = admin_obj.username
                    agent_obj = check_if_user_exist(agent_username)
                    if agent_obj:
                        group_user_agent = GroupUserMapping.query.filter_by(
                            is_agent=True,
                            user_id=agent_obj.id,
                            group_id=group_obj.id
                        ).first()
                        if group_user_agent:
                            task.agent = agent_obj.username
                            task.assigned_on = datetime.datetime.now()
        else:
            return False
    return True


def fill_dict_with_args(request, user):
    grp_id = request.args.get('group_id')
    if grp_id and grp_id is not None:
        grp_id = int(grp_id)
    else:
        grp_id = 0

    args_dict = {
        "user": user,
        "limit": request.args.get('limit'),
        "min_time": request.args.get('min_time'),  # yyyy-mm-dd <> yyyy-mm-dd HH:MM
        "max_time": request.args.get('max_time'),
        "status_search": request.args.get('status_search'),
        "group_id": grp_id,
        "pageno": request.args.get('pageno')
    }
    return args_dict


def get_task_list(param_dict):
    user = param_dict['user']
    tasklist = []
    groups = []
    group_maps = user.group_maps
    all_group_ids = [Group.query.get(group_map.group_id).id for group_map in group_maps]
    status = param_dict.get('status_search')
    if param_dict['group_id'] and param_dict['group_id'] in all_group_ids:
        groups.append(param_dict['group_id'])
    else:
        groups = all_group_ids

    # Tell me how to sort: sort by started time,
    for group in groups:
        tasklist += get_group_tasklist(
            group,
            param_dict['min_time'],
            param_dict['max_time'],
            status
        )
    return tasklist


# resolved in desc completed: sorted on time at task completed, started: sorted on task was started, all: sorted on
# time the task was created
def get_group_tasklist(group_id, min_time, max_time, status):
    if not min_time:
        min_time = '1999-01-01 10:10'
    if not max_time:
        max_time = '2100-01-01 10:10'

    if status == 'COMPLETED':
        return Task.query.filter(
                Task.group_id == group_id,
                Task.created_on >= min_time,
                Task.created_on < max_time,
                Task.status == status
            ).order_by(desc(Task.actual_end_time))
    elif status == 'STARTED':
        return Task.query.filter(
            Task.group_id == group_id,
            Task.created_on >= min_time,
            Task.created_on < max_time,
            Task.status == status
        ).order_by(desc(Task.actual_start_time))
    else:
        return Task.query.filter(
            Task.group_id == group_id,
            Task.created_on >= min_time,
            Task.created_on < max_time
        ).order_by(desc(Task.modified_on))


def date_finder(date_string):
    """
    Converts "%Y-%m-%d %H:%M" --> in datetime format
    :param date_string: 2016-10-10T01:01:01.21233Z
    :return: datetime.datetime.now()
    """
    return datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%fZ")


def get_latlong(geo_dict):
    coordinates = geo_dict.get('coordinates')
    return coordinates[0], coordinates[1]


def task_event_and_map_with_users(task, user, task_action):

    event = Event(task.id, task.group_id, task.agent, task.admin, task.creator, task_action)
    db.session.add(event)
    db.session.commit()

    if task_action == 'CREATED':
        people = [task.creator]
    else:
        people = find_people_who_can_see_this_activity(task.group_id, user)
        if len(people) < 1:
            people = [task.creator]
    map_users_with_event(people, event)


def map_users_with_event(people, event):

    for person in people:
        user_event = UserEvent(person, event.id, event.event_ocurred_time)
        db.session.add(user_event)
        db.session.commit()


# All admins of a group
def find_people_who_can_see_this_activity(group_id, user):
    group_user_mapping = GroupUserMapping.query.filter_by(group_id=group_id)
    user_ids = []

    for group_user in group_user_mapping:
        if group_user.is_admin:
            admin = Admin.query.filter_by(user_id=group_user.user_id).first()
            if admin:
                user_ids.append(admin)
    user_objs = [User.query.get(user_id.user_id) for user_id in user_ids]
    user_objs = list(set(user_objs))
    people = [user.username for user in user_objs]
    if not people:
        people = [user.username]
    return people


def group_created_event_and_map_with_users(group, created_by, group_action, current_user):
    event = Event(None, group.id, None, current_user.username, created_by, group_action)
    db.session.add(event)
    db.session.commit()

    people = find_people_who_can_see_this_activity(group.id, current_user)
    map_users_with_event(people, event)


def admins_group_event_and_map_with_users(group, admins, group_action, current_user):

    if isinstance(admins, dict):
        created_by = User.query.get(group.created_by)
        event = Event(None, group.id, None, admins['phone'], created_by.username, group_action)
        db.session.add(event)
        db.session.commit()

        people = find_people_who_can_see_this_activity(group.id, current_user)
        map_users_with_event(people, event)
    else:
        for admin in admins:
            created_by = User.query.get(group.created_by)
            event = Event(None, group.id, None, admin['phone'], created_by.username, group_action)
            db.session.add(event)
            db.session.commit()

            people = find_people_who_can_see_this_activity(group.id, current_user)
            map_users_with_event(people, event)


def agents_group_event_and_map_with_users(group, agents, group_action, current_user):

    if isinstance(agents, dict):
        created_by = User.query.get(group.created_by)
        event = Event(None, group.id, agents['phone'], None, created_by.username, group_action)
        db.session.add(event)
        db.session.commit()

        people = find_people_who_can_see_this_activity(group.id, current_user)
        map_users_with_event(people, event)
    else:
        for agent in agents:
            created_by = User.query.get(group.created_by)
            event = Event(None, group.id, agent['phone'], None, created_by.username, group_action)
            db.session.add(event)
            db.session.commit()

            people = find_people_who_can_see_this_activity(group.id, current_user)
            map_users_with_event(people, event)


def send_message_to_agent_related_to_task(task):
    agent_registration_ids = []
    if task and task.agent:
        user = User.query.filter_by(username=task.agent).first()
        agent = Agent.query.filter_by(user_id=user.id).first()
        if agent:
            agent_registration_ids.append(agent.gcm_registration_id)
    if len(agent_registration_ids) > 0:
        message = agent_task_added_message(
            task
        )
        send_fcm_message(
            message,
            agent_registration_ids
        )


def agent_task_added_message(task_obj):
    invite_message = {
        "notification_version": "1",
        "notification_type": "task",
        "notification_action": "added",
        "notification_title": "Task {0} assigned to you in group {1}".format(
            task_obj.title.encode('utf-8'),
            task_obj.group_id
        ),
        "notification_text": "A task has been assigned to you",
        "notification_time": readable_datetime_formatter(datetime.datetime.now()),
        "tasks": task_obj.serialize
    }
    return invite_message


def send_message_to_admin_on_task_add(task, current_user):
    group_obj = Group.query.filter_by(id=task.group_id).first()
    user_obj = group_obj.users
    user_obj.remove(current_user)
    admin_registration_ids = []
    for usr in user_obj:
        if usr.is_admin is True:
            admin_obj = Admin.query.filter_by(user_id=usr.id).first()
            admin_registration_ids.append(admin_obj.gcm_registration_id)

    if len(admin_registration_ids) > 0:
        message = admin_task_added_message(task)
        send_fcm_message(
            message,
            admin_registration_ids
        )


def send_message_to_admin_on_task_action(task, action):
    user_obj = User.query.filter_by(username=task.agent).first()
    group_obj = Group.query.filter_by(id=task.group_id).first()
    notification_title = "{0} has {1} task: {2} in group: {3}".format(
            user_obj.name.encode('utf-8'),
            action.lower().encode('utf-8'),
            task.title.encode('utf-8'),
            group_obj.name.encode('utf-8')
        )
    admin = User.query.filter_by(username=task.admin).first()
    admin = Admin.query.filter_by(user_id=admin.id).first()
    admin_registration_ids = []
    if admin:
        admin_registration_ids = [admin.gcm_registration_id]
    message = admin_task_action_message(task, notification_title)
    send_fcm_message(
        message,
        admin_registration_ids
    )


def admin_task_action_message(task, notification_title):
    invite_message = {
        "notification_version": "1",
        "notification_type": "task",
        "notification_action": "status_changed",
        "notification_title": notification_title,
        "notification_text": "Task status changed",
        "notification_time": readable_datetime_formatter(datetime.datetime.now()),
        "task": task.serialize
    }
    return invite_message


def admin_task_added_message(task):
    user_obj = User.query.filter_by(username=task.created_by).first()
    group_obj = Group.query.filter_by(id=task.group_id).first()
    invite_message = {
        "notification_version": "1",
        "notification_type": "task",
        "notification_action": "added",
        "notification_title": "{0} has created task in group: {1}".format(
            user_obj.name.encode('utf-8'),
            group_obj.name.encode('utf-8')
        ),
        "notification_text": "A task is added in Group",
        "notification_time": readable_datetime_formatter(datetime.datetime.now()),
        "task": task.serialize
    }
    return invite_message


def readable_datetime_formatter(date_string):
    fmt = '%a %d, %b %I:%M %p'  # Mon 08,Aug 01:12 PM
    return date_string.strftime(fmt)


def task_has_been_started(task, group_id, agent):
    if task.status == 'STARTED':
        if task.group_id != int(group_id):
            return True
        else:
            if task.agent != agent:
                return True
    return False


# @app.route('/v3/fill_till_death', methods=['POST'])
# def fill_till_death():
#     tasklist = request.json.get('tasks')
#     for info in tasklist:
#         username = info.get("created_by")
#         user = User.query.filter_by(username=str(username)).first()
#         task = Task(user)
#         task.agent = info.get("assigned_to")
#         task.admin = info.get("assigned_by")
#         task.created_on = info.get("creation_time")
#         task.assigned_on = info.get("assigned_time")
#
#         try:
#             task.actual_end_time = date_formatter(info["subtasks"]["actual_end_time"])
#         except:
#             pass
#
#         try:
#             task.actual_start_time = date_formatter(info["subtasks"]["actual_start_time"])
#         except:
#             pass
#
#         try:
#             task.estimated_end_time = date_formatter(info["subtasks"]["end_time"])
#         except:
#             pass
#
#         try:
#             task.estimated_start_time = date_formatter(info["subtasks"]["start_time"])
#         except:
#             pass
#
#         customer = get_customer_from_hell(info['subtasks'][0])
#         address = get_address_from_hell(info['subtasks'][0].get('address'))
#         task.customer = customer.id
#         task.address = address.id
#
#         customer_admin = CustomerAdmin(customer.id, user.id)
#         customer_address = CustomerAddress(customer.id, address.id)
#         task.group_id = info.get("group_id")
#         task.note = info.get("description")
#         task.status = info.get("status")
#         task.type = 'PICK'
#         task.title = info.get("title")
#         db.session.add(task)
#         db.session.add(customer_admin)
#         db.session.add(customer_address)
#         db.session.commit()
#         print task.id, task.group_id
#
#     return jsonify(
#         status=200,
#         message='Ok'
#     )
#
#
# def date_formatter(strin):
#     fmt = "'%Y-%m-%d %H:%M"
#     if string:
#         return datetime.datetime.strptime(strin, fmt).utcnow()
#
#
# def get_customer_from_hell(info):
#     customer_name = info.get('customer_name', 'None')
#     customer_phone = info.get('customer_phone', 'None')
#
#     customer = Customer(customer_name, customer_phone)
#     db.session.add(customer)
#     db.session.commit()
#     return customer
#
#
# def get_address_from_hell(info):
#     street_address = info.get('street_address', 'None')
#     landmark = info.get('landmark', 'None')
#     zip_code = info.get('zipcode', 'None')
#     city = info.get('city', 'None')
#     state = info.get('state', 'None')
#     country = info.get('country', 'None')
#
#     address = Address(
#         street_address,
#         landmark,
#         zip_code,
#         city,
#         state,
#         country
#     )
#
#     db.session.add(address)
#     db.session.commit()
#     return address


@app.route('/v2/tasks/add', methods=['POST'])
@auth.login_required
def v2_add_task_and_subtasks():
    cjf = ConvertingJsonForms()
    return cjf.request_task_add_old_json_to_new_json(request, g.user)


@app.route('/v2/tasks/<task_id>', methods=['GET'])
@auth.login_required
def v2_single_task_details(task_id):
    cjf = ConvertingJsonForms()
    return cjf.fetch_single_task_oldjson_newjson(request, g.user, task_id)


@app.route('/v2/tasks/agent', methods=['GET'])
@auth.login_required
def v2_fetch_tasks_for_agent():
    cjf = ConvertingJsonForms()
    return cjf.fetch_all_task_for_agent_oldjson_newjson(g.user)


@app.route('/v2/tasks/admin', methods=['POST'])
@auth.login_required
def v2_tasks_seen_by_admin():
    cjf = ConvertingJsonForms()
    return cjf.fetch_all_task_for_admin_oldjson_newjson(request, g.user)


@app.route('/v2/subtask/<subtask_id>/update', methods=['PUT'])
@auth.login_required
def v2_update_subtask(subtask_id):
    cjf = ConvertingJsonForms()
    return cjf.action_task_oldjson_newjson(request, g.user, subtask_id)


def url_share_sms(task, phone):
    url_share = 'http://www.loktra.com/track.html?id='
    invite_message = "You have been invited to track a task {0} by visiting this {1}".format(
        task.title.encode('utf-8'), url_share + str(task.id)
    )
    country_code = '91'
    phone = '+' + country_code + phone
    phone = format_phone_number(phone)
    print send_sms_to_phone(phone, invite_message)