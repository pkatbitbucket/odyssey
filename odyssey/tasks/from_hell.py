import requests
import json
import datetime
from flask import jsonify


class ConvertingJsonForms(object):
    def __init__(self):
        pass

    def request_task_add_old_json_to_new_json(self, request, user):
        json_from_v2 = request.json
        print("from v2 app")
        print json_from_v2
        dict_for_v3 = get_dict_for_task_add(request)
        print "dict_for_v3"
        print dict_for_v3
        url = 'http://test.loktra.in/v3/tasks'
        headers = {'content-type': 'application/json'}
        payloads = json.dumps(dict_for_v3)
        auth = (user.username, user.username)
        print url
        r = requests.post(url, data=payloads, headers=headers, auth=auth)
        data = r.json()
        print "response_from_server"
        print data
        serialized_subtask = self.serialize_subtask(data.get('task'))
        serialized_task = self.serialize_task(data.get('task'))
        task = self.make_new_task_into_task_plus_subtask(serialized_task, serialized_subtask)
        print "converte_to_v2_app"
        print task
        return jsonify(
            status=200,
            message='Ok',
            tasks=task
        )

    def fetch_single_task_oldjson_newjson(self, request, user, task_id):

        url = 'http://test.loktra.in/v3/tasks/{0}'.format(task_id)
        auth = (user.username, user.username)

        print url
        r = requests.get(url, auth=auth)
        data = r.json()
        serialized_subtask = self.serialize_subtask(data.get('task'))
        serialized_task = self.serialize_task(data.get('task'))
        task = self.make_new_task_into_task_plus_subtask(serialized_task, serialized_subtask)

        return jsonify(
            status=200,
            message='Ok',
            task=task
        )

    def fetch_all_task_for_agent_oldjson_newjson(self, user):

        url = 'http://test.loktra.in/v3/tasks/agent'
        auth = (user.username, user.username)

        print url
        r = requests.get(url, auth=auth)
        datalist = r.json()
        datalist = datalist.get('tasks')
        subtasks = []
        for data in datalist:
            subtasks.append(self.get_subtask_agent(data))

        return jsonify(
            status=200,
            message='Ok',
            subtasks=subtasks
        )

    def fetch_all_task_for_admin_oldjson_newjson(self, request, user):

        url = 'http://test.loktra.in/v3/tasks/admin'
        url = self.url_maker(url, request.json)
        auth = (user.username, user.username)

        print url
        r = requests.get(url, auth=auth)
        datalists = r.json()
        datalist = datalists.get('tasks')
        tasks = []

        for data in datalist:
            serialized_subtask = self.serialize_subtask(data)
            # print serialized_subtask
            serialized_task = self.serialize_task(data)
            # print serialized_task
            tasks.append(self.make_new_task_into_task_plus_subtask(serialized_task, serialized_subtask))

        return jsonify(
            status=200,
            message='Ok',
            tasks=tasks
        )

    def action_task_oldjson_newjson(self, request, user, subtask_id):
        time = simple_to_iso_string(request.json.get('time'))
        task_id = request.json.get('subtask_id')
        task_status = request.json.get('subtask_status')
        url = 'http://test.loktra.in/v3/tasks/{0}/'.format(task_id)
        if task_status == 'CANCELLED':
            url += 'cancel'
        elif task_status == 'STARTED':
            url += 'start'
        elif task_status == 'COMPLETED':
            url += 'complete'

        payload = {
            "time": str(time),
            "location": {
                "type": "Point",
                "coordinates": [89.00, 89.00]
            }
        }

        auth = (user.username, user.username)
        headers = {'content-type': 'application/json'}   
        print url
        r = requests.post(url, data=json.dumps(payload), headers=headers, auth=auth)
        data = r.json()
        if data.get('status') == 200:
            return jsonify(
                status=200,
                message='Ok'
            )
        else:
            return jsonify(
                status=404,
                message='Not found'
            )

    def url_maker(self, url, params):
        if not params:
            return url
        else:
            url = url + "?" + "&".join("{0}={1}".format(param, params[param]) for param in params)
            url = url.replace('status', 'status_search')
            url = url.replace('IN-PROGRESS', 'STARTED')
            return url

    def serialize_subtask(self, data):
        address = {}
        if data.get('address_detail'):
            address = data.get('address_detail')
            address = address.get('address')

        customer = {}
        if data.get('customer_detail'):
            customer = data.get('customer_detail')

        start = {}
        if data.get('start'):
            start = data.get('start')

        end = {}
        if data.get('end'):
            end = data.get('end')

        return {
            'parent_task': data.get('id'),
            'type': data.get('type'),
            'status': data.get('status'),
            'id': data.get('id'),
            'description': data.get('note'),
            'start_time': iso_to_simple(start.get('estimated_start_time')),
            'end_time': iso_to_simple(end.get('estimated_end_time')),
            'actual_start_time': iso_to_simple(start.get('actual_start_time')),
            'actual_end_time': iso_to_simple(end.get('actual_end_time')),
            'customer_name': customer.get('name'),
            'customer_phone': customer.get('phone'),
            'customer_feedback': None,
            'note_by_customer': None,
            'address': {
                'street_address': address.get('street_address'),
                'city': address.get('city'),
                'state': address.get('state'),
                'country': address.get('country'),
                'country_code': None,
                'zipcode': address.get('zipcode')
            },
        }

    def serialize_task(self, data):
        time = data.get('time')
        person = data.get('person')
        address = data.get('address_detail')
        customer = data.get('customer_detail')

        return {
            'title': data.get('title'),
            'type': 'SINGLE',
            'status': data.get('status'),
            'created_by': person.get('creator_phone'),
            'group_id': person.get('group_id'),
            'description': data.get('note'),
            'task_id': data.get('id'),
            'creation_time': iso_to_simple(time.get('created_on')),
            'assigned_time': iso_to_simple(time.get('assigned_on')),
            'assigned_by': person.get('admin_phone'),
            'assigned_to': person.get('agent_phone'),
            'customer_name': customer.get('name'),
            'customer_phone': customer.get('phone')
        }

    def get_subtask_agent(self, data):
        address = {}
        if data.get('address_detail'):
            address = data.get('address_detail')
            address = address.get('address')

        customer = {}
        if data.get('customer_detail'):
            customer = data.get('customer_detail')
            customer = customer.get('customer')
        person = data.get('person')

        end = {}
        if data.get('end'):
            end = data.get('end')

        return {
            'task_id': data.get('id'),
            'group_id': person.get('group_id'),
            "subtask_id": data.get('id'),
            "task_title": data.get('title', 'title'),
            "task_description": data.get('note', 'note'),
            'type': data.get('type'),
            'note': data.get('note', 'note'),
            "subtask_status": data.get('status'),
            'customer_name': data.get('customer_name'),
            'customer_phone': data.get('customer_phone'),
            "assigned_by": person.get('admin_phone'),
            "assigned_by_name": person.get('admin_name'),
            'time': iso_to_simple(end.get('estimated_end_time')),
            'address': {
                'street_address': address.get('street_address', ''),
                'city': address.get('city', ''),
                'state': address.get('state', ''),
                'country': address.get('country', ''),
                'country_code': '',
                'zipcode': address.get('zipcode', '')
            },
        }

    def make_new_task_into_task_plus_subtask(self, task, subtask):
        task['subtasks'] = [subtask]
        return task


def get_dict_for_task_add(request):
    subtask_details = request.json.get('subtasks')
    if len(subtask_details) > 0:
        subtask = subtask_details[0]
        address = subtask.get('address', {})
    else:
        subtask = dict()
        address = {}

    return {
        "title": request.json.get('title'),
        "type": subtask.get('type'),
        "group_id": request.json.get('group_id'),
        "agent": request.json.get('assigned_to'),
        "admin": request.json.get('assigned_by'),
        "customer_id": None,
        "address_id": None,
        "note": request.json.get('description'),
        "estimated_end_time": simple_to_iso_string(subtask.get('end_time')),
        "customer_detail": {
            "name": subtask.get('customer_name', None),
            "phone": subtask.get('customer_phone', None),
            "email": None,
            "details": None
        },
        "address_detail": {
            "street_address": address.get('street_address', ''),
            "landmark": None,
            "zip_code": address.get('zipcode', ''),
            "city": address.get('city', ''),
            "state": address.get('state', ''),
            "country": address.get('country', '')
        }
    }


def simple_to_iso_string(datetime_string_simple):
    if datetime_string_simple:
        fmt = '%Y-%m-%d %H:%M'
        d = datetime.datetime.strptime(datetime_string_simple, fmt)
        d = d - datetime.timedelta(hours=5, minutes=30)
        date_string = str(d.isoformat()) + '.0Z'
        return date_string
    else:
        return None


def iso_to_simple(datetime_string_isoformat):
    if datetime_string_isoformat:
        fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
        simple_fmt = '%Y-%m-%d %H:%M'
        d = datetime.datetime.strptime(datetime_string_isoformat, fmt)
        d = d + datetime.timedelta(hours=5,minutes=30)
        return d.strftime(simple_fmt)


def date_finder(date_string):
    """
    Converts "%Y-%m-%d %H:%M" --> in datetime format
    :param date_string: 2016-10-10T01:01:01.21233Z
    :return: datetime.datetime.now()
    """
    return datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%fZ")