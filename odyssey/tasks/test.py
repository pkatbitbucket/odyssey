import os
import sys
import unittest
import datetime
import odyssey
from odyssey.tasks.models import Task
from odyssey.user.models import User
from odyssey.groups.models import Group

import boto3
from sqlalchemy.sql import and_

sys.path.append(os.path.join(
    os.path.dirname(os.path.realpath(__file__)), os.pardir
    ))

print "working on ", os.environ['APP_SETTINGS']
if odyssey.app.config["TESTING"]:
    odyssey.app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['TEST_DATABASE_URL']

print "using database", odyssey.app.config['SQLALCHEMY_DATABASE_URI']


class ModelTestCase(unittest.TestCase):

    # Creating task with created_by field only
    def test_task_create(self):
        create_user()
        create_basic_task()

    def test_task_create_within_group(self):
        create_user()
        create_group()
        create_basic_task()


def create_user():
    user_test = User(
        username="9703128274",
        avatar="https://s3-ap-southeast-1.amazonaws.com/com.loktra.testavatars/u_1",
        password_hash="123456",
        is_admin=True,
        is_agent=True,
        name="Ankush",
        organization="Loktra",
        description="For testing",
        email="ankush@loktra.com",
        enabled=True,
    )
    odyssey.db.session.add(user_test)
    odyssey.db.session.commit()
    return user_test


def create_group():
    group_test = Group(
        name="Test Group",
        description="Test group",
        avatar="http://goo.gl/2pJynl",
        created_by=1
    )
    odyssey.db.session.add(group_test)
    odyssey.db.session.commit()
    return group_test


def create_basic_task():
    task_test = Task(
        task_name="Test Task",
        task_creation_time=datetime.datetime.now(),
        task_type="Pick"
    )

    odyssey.db.session.add(task_test)
    odyssey.db.session.commit()
    return task_test


def create_advanced_task():
    task_test = create_basic_task()

    task_test.task_status = "assigned"
    task_test.assigned_by = "919703128274"
    task_test.assigned_time =  "2016-03-09T06:00:18.648755Z"
    task_test.assigned_to = "919703128274"
    task_test.created_by = "919703128274"


def create_group_user_mapping_agent():
    group_user_mapping = GroupUserMapping(
        group_id=1,
        user_id=1,
    )
    odyssey.db.session.add(group_user_mapping)
    odyssey.db.session.commit()
    group_user_mapping.make_agent()
    group_user_mapping.update_agent_status(2)
    group_user_mapping.update_agent_join_group()

def create_agent():
    agent_test = Agent(
        driving_license="pksingh12345",
        user_id=1,
    )
    odyssey.db.session.add(agent_test)
    odyssey.db.session.commit()

def create_admin():
    admin_test = Admin(
        user_id=1,
    )
    odyssey.db.session.add(admin_test)
    odyssey.db.session.commit()

def add_verification_code():
    code_test = VerificationCode(
        phone="917506134455",
        code="100000"
    )
    odyssey.db.session.add(code_test)
    odyssey.db.session.commit()

def add_locations():
    location1 = UserLocation(
        user_id=1,
        latitude=45.7597,
        longitude=4.8422,
        accuracy=01.09,
        timestamp='2016-07-28 21:09:00',
        bearing=120
    )
    location2 = UserLocation(
        user_id=1,
        latitude=48.8567,
        longitude=2.3508,
        accuracy=01.09,
        timestamp='2016-06-23 09:09:00',
        bearing=120
    )
    odyssey.db.session.add_all([location1, location2])
    odyssey.db.session.commit()

# def create_task():
#     Task()


if __name__ == '__main__':
    unittest.main()



