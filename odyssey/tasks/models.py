# -*- coding: utf-8 -*-
from odyssey import db
import datetime
import random
import string
from passlib.apps import custom_app_context as pwd_context
from odyssey.utils import simple_datetime_formatter
from odyssey.user.models import Agent, Admin, User
from odyssey.groups.models import GroupUserMapping, Group
from odyssey.customer.models import Address, Customer


class Task(db.Model):
    # Basic Details
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, default='Untitled')
    # PICK/DROP/VISIT
    type = db.Column(db.String)
    # UNASSIGNED/ASSIGNED/STARTED/COMPLETED/CANCELLED
    status = db.Column(db.String, default='Unassigned')
    note = db.Column(db.String)

    # Customer and Address
    address = db.Column(db.Integer, db.ForeignKey('address.id'))
    customer = db.Column(db.Integer, db.ForeignKey('customer.id'))

    # Users
    creator = db.Column(db.String, db.ForeignKey('user.username'))
    admin = db.Column(db.String, db.ForeignKey('user.username'))
    agent = db.Column(db.String, db.ForeignKey('user.username'))
    group_id = db.Column(db.Integer, db.ForeignKey('group.id'))

    # User specific Time
    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    modified_on = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    assigned_on = db.Column(db.DateTime)

    # Task specific time
    estimated_start_time = db.Column(db.DateTime)
    estimated_end_time = db.Column(db.DateTime)
    actual_start_time = db.Column(db.DateTime)
    actual_end_time = db.Column(db.DateTime)

    # Task specific location
    start_latitude = db.Column(db.Float)
    start_longitude = db.Column(db.Float)
    destination_latitude = db.Column(db.Float)
    destination_longitude = db.Column(db.Float)

    # Other useful attributes
    url = db.Column(db.String, unique=True)
    distance_covered = db.Column(db.Float)

    # on_completion_picture
    picture_url = db.Column(db.String)
    # on_completion_signature
    signature_url = db.Column(db.String)
    # on_completion_note
    comment_by_agent = db.Column(db.String)

    def __init__(self, user):
        self.creator = user.username

    def __repr__(self):
        return "Task of <Title: {0}> in <Group: {1}>.".format(self.title, self.group_id)

    def get_all_admin_gcm_related(self):
        if not self.group_id:
            return []
        group_user_mappings = GroupUserMapping.query.filter(
            GroupUserMapping.is_admin is True,
            GroupUserMapping.group_id == self.group_id
        ).all()
        admins = [Admin.query.filter_by(user_id=x.user_id).first() for x in group_user_mappings]
        return [x.gcm_registration_id for x in admins]

    def get_all_admin(self):
        if not self.group_id:
            return []
        group_user_mappings = GroupUserMapping.query.filter(
            GroupUserMapping.is_admin is True,
            GroupUserMapping.group_id == self.group_id
        ).all()
        admins_user_id = [User.query.filter_by(id=x.user_id).first() for x in group_user_mappings]
        return admins_user_id

    def get_agent(self):
        if not self.group_id:
            return []
        group_user_mappings = GroupUserMapping.query.filter(
            GroupUserMapping.is_admin is True,
            GroupUserMapping.group_id == self.group_id
        ).all()
        agents = [Agent.query.filter_by(user_id=x.user_id).first() for x in group_user_mappings]
        return [x.gcm_registration_id for x in agents]

    def update_customer(self, customer_name, customer_phone):
        pass

    def update_address(self, address):
        pass

    def update_actual_start_time(self, latitude, longitude, start_time_from_agent):
        self.actual_start_time = start_time_from_agent
        self.start_latitude = latitude
        self.start_longitude = longitude

    def update_actual_end_time(self, latitude, longitude, end_time_from_agent):
        self.actual_end_time = end_time_from_agent
        self.destination_latitude = latitude
        self.destination_longitude = longitude

    def update_distance_covered(self, current_location):
        pass

    def update_users_related(self, admin, agent):
        self.admin = admin
        self.agent = agent
        self.status = 'ASSIGNED'
        self.modified_on = datetime.datetime.now()
        # update agent, admin

    def update_status_start_task(self, latitude, longitude, start_time_from_agent):
        self.status = 'STARTED'
        self.update_actual_start_time(latitude, longitude, start_time_from_agent)

    def update_status_cancel_task(self, latitude, longitude, end_time_from_agent):
        self.status = 'CANCELLED'
        self.update_actual_end_time(latitude, longitude, end_time_from_agent)

    def update_status_complete_task(self, latitude, longitude, end_time_from_agent):
        self.status = 'COMPLETED'
        self.update_actual_end_time(latitude, longitude, end_time_from_agent)

    def update_status_rejected_task(self):
        self.status = 'REJECTED'
        self.agent = None

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def make_url(self):
        url = ''.join(
            random.SystemRandom().choice(
                string.ascii_uppercase + string.digits + string.ascii_lowercase
            ) for _ in range(11)
        )

        self.url = pwd_context.encrypt(url)

    def verify_url(self, task_id, url):
        if self.id == task_id:
            return pwd_context.verify(url, self.url)
        return False

    def details_by_agent_on_completion(self, pic_url, sig_url, agent_note):
        if isinstance(pic_url, list):
            pic_url = ",".join(pic_url)
        self.picture_url = pic_url
        self.signature_url = sig_url
        self.comment_by_agent = agent_note

    @property
    def serialize(self):
        creator = User.query.filter_by(username=self.creator).first()
        admin_name = ''
        agent_name = ''
        group_name = ''
        if self.admin and self.admin is not None:   
            admin = User.query.filter_by(username=self.admin).first()
            if admin.name:
                admin_name = admin.name
        if self.agent and self.agent is not None:
            agent = User.query.filter_by(username=self.agent).first()
            if agent.name:
                agent_name = agent.name
        if self.group_id and self.group_id is not None:
            group = Group.query.get(self.group_id)
            if group.name:
                group_name = group.name

        customer_obj = Customer.query.get(self.customer)
        address_obj = Address.query.get(self.address)
        picture_url_in_list = None
        if self.picture_url:
            pic_url = self.picture_url
            picture_url_in_list = pic_url.split(',')

        time_diff = None
        if self.actual_start_time and self.actual_end_time:
            delta = self.actual_end_time - self.actual_start_time
            seconds = int(delta.total_seconds())
            time_diff = str(seconds)

        return {
            'id': self.id,
            'title': self.title,
            'status': self.status,
            'type': self.type,
            'note': self.note,
            'person': {
                'creator_name': creator.name,
                'creator_phone': self.creator,
                'admin_name': admin_name,
                'admin_phone': self.admin,
                'agent_name': agent_name,
                'agent_phone': self.agent,
                'group_id': self.group_id,
                'group_name': group_name
            },
            'time': {
                'created_on': simple_datetime_formatter(self.created_on),
                'modified_on': simple_datetime_formatter(self.modified_on),
                'assigned_on': simple_datetime_formatter(self.assigned_on)
            },
            'start': {
                'estimated_start_time': simple_datetime_formatter(self.estimated_start_time),
                'actual_start_time': simple_datetime_formatter(self.actual_start_time),
                "location": {
                    "type": "Point",
                    "coordinates": [self.start_latitude, self.start_longitude]
                }
            },
            'end': {
                'estimated_end_time': simple_datetime_formatter(self.estimated_end_time),
                'actual_end_time': simple_datetime_formatter(self.actual_end_time),
                "location": {
                    "type": "Point",
                    "coordinates": [self.destination_latitude, self.destination_longitude]
                }
            },
            'url': self.url,
            'distance_covered': self.distance_covered,
            'address_detail': address_obj.serialize,
            'customer_detail': customer_obj.serialize,
            'time_diff': time_diff,
            'on_completion': {
                'pictures': picture_url_in_list,
                'signature': self.signature_url,
                'comment_by_agent': self.comment_by_agent
            }
        }


# class Feedback(db.Model):
#     id = db.Column(db.Integer)
#     photo = db.Column(db.String)
#     note = db.Column(db.String)
#     rating = db.Column(db.Integer)
#
#     task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
#
#     def __init__(self, photo, note, rating):
#         self.photo = photo
#         self.note = note
#         self.rating = rating
#
#     def __repr__(self):
#         print("Customer gave {0} rating to task done.")
#
#     # For multiple photos change this function
#     def attach_photo(self, url_string):
#         self.photo = url_string
#
#     def attach_note(self, note):
#         self.note = note
#
#     def set_rating(self, rating):
#         self.rating = rating















