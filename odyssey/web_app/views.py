#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import json
from flask import jsonify, request, g, abort
from geopy.geocoders import Nominatim
from haversine import haversine
from odyssey import app, sockets, auth
from odyssey.groups.models import GroupUserMapping, Group
from odyssey.locations.models import UserLocation
from odyssey.user.models import User, Agent
from odyssey.tasks.models import Task
from odyssey.customer.models import Address
from flask import jsonify
from odyssey import redis_store
from sqlalchemy.sql import and_
from geoalchemy2.elements import WKTElement
from odyssey import celery_tasks
from flask import render_template
from odyssey.utils import send_sms_to_phone, format_phone_number, send_fcm_message
from odyssey import db
from flask_cors import cross_origin
from odyssey.locations.views import websocket_queue
from geventwebsocket import WebSocketError

websocket_queue_customer = websocket_queue


@app.route('/v3/send_url_notification/<country_code>/<phone_number>', methods=['POST'])
@auth.login_required
def send_sms_to_customer_with_url(country_code, phone_number):
    user = g.user
    task_id = request.json.get('task_id')
    task = Task.query.get(int(task_id))
    message_type = request.json.get('type')
    url_share = 'http://www.loktra.com/track.html?id='
    url_get = 'http://test.loktra.in/v3/get_customer_location/'
    if message_type == 'SHARE' is message_type is None:
        invite_message = "{0} has invited you to track a task {1} by visiting this {2}".format(
                user.name.encode('utf-8'), task.title.encode('utf-8'), url_share + str(task.id)
            )
    if message_type == 'GET':
        invite_message = "Our agent want your exact location. Please share by visiting this link {0}".format(
            url_get + task.url.encode('utf-8')
        )

    phone = '+' + country_code + phone_number
    phone = format_phone_number(phone)
    print send_sms_to_phone(phone, invite_message)

    return jsonify(
        status=200,
        message='Ok'
    )


@app.route('/v3/share_location/<task_id>', methods=['GET'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def live_location(task_id):

    task = Task.query.filter_by(id=task_id).first()

    if not task:
        return jsonify(
            status=404,
            message='Task Not Found'
        )
    else:
        if task.status != 'STARTED':
            return jsonify(
                status=404,
                message='Task already completed'
            )

    agent_username = task.agent
    admin_username = task.admin
    agent_as_user = User.query.filter_by(username=agent_username).first()
    admin_as_user = User.query.filter_by(username=admin_username).first()
    context = dict()
    context['agent_avatar'] = agent_as_user.avatar
    context['agent_name'] = agent_as_user.name
    context['agent_phone'] = agent_username
    context['admin_avatar'] = admin_as_user.avatar
    context['admin_name'] = admin_as_user.name
    context['admin_phone'] = admin_username
    context['task_name'] = task.title
    context['task_id'] = task.id

    return jsonify(
        status=200,
        message='Ok',
        context=context
    )


@sockets.route('/v3/live_location')
def get_agent_location(ws):
    """
    accepts web socket connection for agent location
    :param ws: websocket
    :return:
    """
    # on connect
    websocket_queue_customer.append(ws)
    info = ws.receive()
    if info is None:
        websocket_queue_customer.remove(ws)
        ws.close()
    info = json.loads(info)
    agent_username = info.get('agent_phone')
    task_id = int(info.get('task_id'))

    # fetch agent and task
    user = User.query.filter_by(username=agent_username).first()
    task = Task.query.get(task_id)
    last_time = None

    if not task:
        not_started_yet = 'Not Found'
        # ws.send(not_started_yet)
        websocket_queue_customer.remove(ws)
        ws.close()
    else:
        if task.agent != agent_username:
            websocket_queue_customer.remove(ws)
            ws.close()

    while True:
        if task.actual_start_time:
            last_time = task.actual_start_time
            break
        not_started_yet = 'Not Started'
        # ws.send(not_started_yet)
        websocket_queue_customer.remove(ws)
        ws.close()

    while True:
        if task.status == 'COMPLETED':
            ws.send('Task Completed')
            ws.receive()
            break
        user_current_locations = UserLocation.query.filter(
            UserLocation.user_id == user.id,
            UserLocation.timestamp > last_time
        ).order_by(UserLocation.timestamp)
        final_string = "No Location"

        for user_current_location in user_current_locations:

            final_string = "{0}, {1}, {2}".format(
                user_current_location.latitude,
                user_current_location.longitude,
                user_current_location.timestamp + datetime.timedelta(hours=5, minutes=30)
            )

            last_time = user_current_location.timestamp
            ws.send(final_string)
            ws.receive()

        if final_string == "No Location":
            ws.send(final_string)
            ws.receive()
        # new_user_location = ["{0}, {1}, {2}" for user_current_location in user_current_locations]
        # if new_user_location:
        #     if new_user_location[-1]:
        #         location_info = new_user_location[-1]
        #         last_time = location_info.get('timestamp')
        # output = json.dumps(user_current_location)
    websocket_queue_customer.remove(ws)
    ws.close()


@app.route('/v3/get_customer_location/<random_string>', methods=['GET'])
def map_page_for_customer(random_string):
    task = Task.query.filter_by(url=random_string).first()

    if not task:
        return jsonify(
            status=404,
            message='Task Not Found'
        )

    agent_username = task.agent
    admin_username = task.admin
    agent_as_user = User.query.filter_by(username=agent_username).first()
    admin_as_user = User.query.filter_by(username=admin_username).first()
    context = dict()
    context['agent_avatar'] = agent_as_user.avatar
    context['agent_name'] = agent_as_user.name
    context['agent_phone'] = agent_username
    context['admin_avatar'] = admin_as_user.avatar
    context['admin_name'] = admin_as_user.name
    context['admin_phone'] = admin_username
    context['task_name'] = task.title
    context['task_id'] = task.id

    return render_template('map_me_page.html', context=context)


@app.route('/v3/get_customer_location/<random_string>', methods=['POST'])
def get_customer_location(random_string):
    location = request.json.get('locations')
    task = Task.query.filter_by(url=random_string).first()
    if not task:
        return jsonify(
            status=404,
            message='Url not found'
        )

    if task.status == 'COMPLETE':
        return jsonify(
            status=404,
            message='Url not found'
        )

    address_id = task.address
    address = Address.query.get(address_id)

    if location:
        coordinates = request.json.get('coordinates')
        if coordinates:
            address.latitude = coordinates[0]
            address.longitude = coordinates[1]

    db.session.commit(address)
    send_message_to_agent_related_to_task(task)

    return jsonify(
        status=200,
        message='Address Updated'
    )


def send_message_to_agent_related_to_task(task):
    agent_registration_ids = []
    if task and task.agent:
        user = User.query.filter_by(username=task.agent).first()
        agent = Agent.query.filter_by(user_id=user.id).first()
        if agent:
            agent_registration_ids.append(agent.gcm_registration_id)
    if len(agent_registration_ids) > 0:
        message = agent_task_added_message(
            task
        )
        send_fcm_message(
            message,
            agent_registration_ids
        )
    print "Sending message to Agent"


def agent_task_added_message(task_obj):
    invite_message = {
        "notification_version": "1",
        "notification_type": "task",
        "notification_action": "edited",
        "notification_title": "Address in task {0} is edited.".format(
            task_obj.title.encode('utf-8'),
            task_obj.group_id
        ),
        "notification_text": "Task information updated.",
        "notification_time": readable_datetime_formatter(datetime.datetime.now()),
        "tasks": task_obj.serialize
    }
    return invite_message


def readable_datetime_formatter(date_string):
    fmt = '%a %d, %b %I:%M %p'  # Mon 08,Aug 01:12 PM
    return date_string.strftime(fmt)
