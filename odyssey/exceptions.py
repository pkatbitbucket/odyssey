# -*- coding: utf-8 -*-
from flask import jsonify, request
from odyssey import app
import sys
import traceback
import datetime


# TODO: All exceptions to be logged for better error logs.
@app.errorhandler(500)
def internal_error(exceptions):
    """
    Handles server error like when query not found, bugs, etc. It needs to be logged.
    :return: HTTP status message
    """
    log_error(request)
    return jsonify(status=500, message='Internal Server Error')


@app.errorhandler(404)
def not_found_error(exceptions):
    """
    Handles 404.
    :param :
    :return: HTTP status message
    """
    log_error(request)
    return jsonify(status=404, message='Invalid URL')


@app.errorhandler(Exception)
def all_exception_handler(exceptions):
    """
    Handles all other exceptions.
    :param :
    :return: HTTP status message
    """
    log_error(request)
    return jsonify(status=500, message="Internal server Error")


def log_error(request):
    exc_type, exc_value, exc_traceback = sys.exc_info()
    app.logger.error(
        """
        Request:   {method} {path}
        IP:        {ip}
        Agent:     {agent_platform} | {agent_browser} {agent_browser_version}
        Raw Agent: {agent}
        Time:      {time}
        """.format(
            method=request.method,
            path=request.path,
            ip=request.remote_addr,
            agent_platform=request.user_agent.platform,
            agent_browser=request.user_agent.browser,
            agent_browser_version=request.user_agent.version,
            agent=request.user_agent.string,
            time=datetime.datetime.now()
        ), exc_info=traceback.format_exception(exc_type, exc_value, exc_traceback)
    )