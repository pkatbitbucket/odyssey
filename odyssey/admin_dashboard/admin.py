from odyssey import app, db
from flask_admin import Admin, BaseView, expose
from flask_admin.contrib.sqla import ModelView
from odyssey.user.models import User, Agent
import odyssey
from odyssey.groups.models import Group, GroupUserMapping
from odyssey.locations.models import UserLocation
from odyssey.tasks.models import Task
from odyssey.admin_dashboard.models import LoktraUser, Role
from wtforms.fields import PasswordField
from flask_security import current_user, Security, utils


class UserTrack(BaseView):

    def is_accessible(self):
        return current_user.has_role('admin')
    
    @expose('/')
    def index(self):
        admins = User.query.filter_by(is_admin=True).all()
        admin_groups = {}
        admin_track_list = {}
        for admin in admins:
            group_maps = admin.group_maps
            admin_group_ids = [group_map.group_id for group_map in group_maps \
                 if group_map.is_admin]
            
            agent_list = []
            groups = []
            for admin_group_id in admin_group_ids:
                all_user_mappings = GroupUserMapping.query.filter_by(
                    group_id=admin_group_id
                ).all()
                groups.append(Group.query.get(admin_group_id).name)
                for one_user_mapping in all_user_mappings:
                    if one_user_mapping.is_agent:
                        agent_list.append(
                            one_user_mapping.user.name \
                            if one_user_mapping.user.name != ''
                            else one_user_mapping.user.username
                        )
            admin_track_list[admin.username] = set(agent_list)
            admin_groups[admin.username] = groups

        return self.render(
            'admin_dashboard/index.html', admins=admins,
            admin_groups=admin_groups, admin_track_list=admin_track_list
        )

    @expose('/<username>')
    def tracked_agent(self, username):
        admin = User.query.filter_by(username=username).first()
        group_maps = admin.group_maps
        admin_group_ids = [group_map.group_id for group_map in group_maps \
            if group_map.is_admin
        ]
        all_user_mappings = []
        for admin_group_id in admin_group_ids:
            all_user_mappings = GroupUserMapping.query.filter(
                GroupUserMapping.group_id==admin_group_id,
                GroupUserMapping.is_agent==True,
                GroupUserMapping.has_agent_joined_group==True
            ).all()
        return self.render(
            'admin_dashboard/agent.html',
            agents=all_user_mappings
        )


admin = Admin(name="Loktra Administration")

admin.add_view(UserTrack(name='UserTrack'))

# Customized User model for SQL-Admin
class LoktraUserAdmin(ModelView):

    # Don't display the password on the list of Users
    column_exclude_list = ('password',)

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = ('password',)

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return current_user.has_role('admin')

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):

        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(LoktraUserAdmin, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = PasswordField('New Password')
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):

        # If the password field isn't blank...
        if len(model.password2):

            # ... then encrypt the new password prior to storing it in the database. If the password field is blank,
            # the existing password in the database will be retained.
            model.password = utils.encrypt_password(model.password2)


# Customized Role model for SQL-Admin
class RoleAdmin(ModelView):

    # Prevent administration of Roles unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return current_user.has_role('admin')

admin.add_view(LoktraUserAdmin(LoktraUser, db.session))
admin.add_view(RoleAdmin(Role, db.session))


class LoktraModelView(ModelView):
    # Disable model creation, edition and deletion
    can_create = False
    can_delete = False
    can_edit = False

    def is_accessible(self):
        return current_user.has_role('admin')


class UserAdmin(LoktraModelView):
    # Override displayed fields
    column_list = (
        'username', 'name', 'email', 'organization',
        'description', 'is_admin', 'is_agent'
    )
    column_searchable_list = (
        'username', 'name', 'email', 'organization',
        'description'
    )

    def __init__(self, session, **kwargs):
        # You can pass name and other parameters if you want to
        super(UserAdmin, self).__init__(User, session, **kwargs)

admin.add_view(UserAdmin(db.session))


class AgentAdmin(LoktraModelView):
    column_list = (
        'username','driving_license'
    )
    column_filters = ('username', )
    def __init__(self, session, **kwargs):
        super(AgentAdmin, self).__init__(Agent, session, **kwargs)


admin.add_view(AgentAdmin(db.session))


class GroupAdmin(LoktraModelView):
    column_list = (
        'name', 'description', 'created_at', 'created_by_user', 'users'
    )
    column_searchable_list = ('name', )
    column_filters =  ('users', 'name')

    def __init__(self, session, **kwargs):
        super(GroupAdmin, self).__init__(Group, session, **kwargs)

admin.add_view(GroupAdmin(db.session))


class GroupUserMappingAdmin(LoktraModelView):
    column_list = (
        'user', 'group', 'is_admin', 'is_agent', 'has_admin_joined_group',
        'admin_joined_group_on', 'group_request_agent_on',
        'has_agent_joined_group', 'agent_joined_group_on'
    )
    column_filters =  ('user', 'group')
    def __init__(self, session, **kwargs):
        super(GroupUserMappingAdmin, self).__init__(
            GroupUserMapping, session, **kwargs
        )
admin.add_view(GroupUserMappingAdmin(db.session))


class UserLocationAdmin(LoktraModelView):

    column_list = (
        'username', 'latitude', 'longitude', 'timestamp',
        'battery_status', 'speed', 'running', 'still', 'walking'
    )
    column_filters = ('username', )
    def __init__(self, session, **kwargs):
        super(UserLocationAdmin, self).__init__(UserLocation, session, **kwargs)

admin.add_view(UserLocationAdmin(db.session))


class TaskAdmin(LoktraModelView):
    column_exclude_list = (
        'picture_url', 'signature_url', 'comment_by_agent',
        'start_latitude', 'start_longitude', 'destination_latitude',
        'destination_longitude', 'url', 
    )
    def __init__(self, session, **kwargs):
        super(TaskAdmin, self).__init__(Task, session, **kwargs)

admin.add_view(TaskAdmin(db.session))

admin.init_app(app)