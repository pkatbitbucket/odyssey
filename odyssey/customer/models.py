# -*- coding: utf-8 -*-
import datetime
from geoalchemy2 import Geography
from geopy.geocoders import GoogleV3
from geopy.exc import GeocoderServiceError
from odyssey import db


class Customer(db.Model): 
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String)
    name = db.Column(db.String)
    email = db.Column(db.String)
    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    modified_on = db.Column(
        db.DateTime,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now
    )
    details = db.Column(db.Text)

    def __init__(self, name, username):
        self.name = name
        self.username = username

    def __repr__(self): 
        return '<ID {0}, Name {1}>'.format(self.id, self.name.encode('utf-8'))

    def update(self, name, username): 
        self.name = name,
        self.username = username

    def update_phone(self, phone): 
        self.username = phone

    def add_details(self, details): 
        self.details = details

    @property
    def serialize(self): 
        return {
            "id": self.id,
            "name": self.name,
            "phone": self.username,
            "email": self.email,
            "details": self.details
        }

    @property
    def customer_with_addresses(self):
        cust_addr_obj = CustomerAddress.query.filter_by(cust=self.id)
        addresses = [Address.query.get(obj.addr) for obj in cust_addr_obj]
        address_detail = [address.fetch_address for address in addresses]
        return {
            "customer": self.serialize,
            "addresses": address_detail
        }


class Address(db.Model): 
    id = db.Column(db.Integer, primary_key=True)
    street_address = db.Column(db.String)
    landmark = db.Column(db.String)
    zip_code = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    country = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    modified_on = db.Column(
        db.DateTime,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now
    )

    def __init__(
            self,
            street_address=None,
            landmark=None,
            zip_code=None,
            city=None,
            state=None,
            country=None
    ): 
        self.street_address = street_address
        self.landmark = landmark
        self.zip_code = zip_code
        self.city = city
        self.state = state
        self.country = country

    def update(
        self,
        street_address=None,
        landmark=None,
        zip_code=None,
        city=None,
        state=None,
        country=None
    ): 
        self.street_address = street_address
        self.landmark = landmark
        self.zip_code = zip_code
        self.city = city
        self.state = state
        self.country = country

    def __repr__(self):
        return 'Street Address: {0}'
        'Landmark: {1}'
        'Zip_code: {2}'
        'City: {3}'
        'State: {4}'
        'Country: {5}'.format(self.street_address,
                              self.landmark,
                              self.zip_code,
                              self.city,
                              self.state,
                              self.country
                             )
    def fetch_location(self): 
        return {
            "location":  self.location
        }

    @property
    def fetch_address(self):
        return {
            "id": self.id,
            "street_address":  self.street_address,
            "landmark":  self.landmark,
            "zip_code":  self.zip_code,
            "city":  self.city,
            "state":  self.state,
            "country":  self.country
        }

    def simple_address_format(self):
        """
        self.street_address sometime returns tuple or unicode. Couldn't find reason.
        :return: address string
        """
        address = ""
        if self.street_address is not None:
            try:
                address += self.street_address.encode('utf-8') + ', '
            except AttributeError as e:
                address += str(self.street_address).encode('utf-8') + ', '

        if self.landmark is not None:
            try:
                address += self.landmark.encode('utf-8') + ', '
            except AttributeError as e:
                address += str(self.landmark).encode('utf-8') + ', '

        if self.city is not None:
            try:
                address += self.city.encode('utf-8') + ', '
            except AttributeError as e:
                address += str(self.city).encode('utf-8') + ', '

        if self.state is not None:
            try:
                address += self.state.encode('utf-8') + ', '
            except AttributeError as e:
                address += str(self.state).encode('utf-8') + ', '

        if self.zip_code is not None:
            try:
                address += self.zip_code.encode('utf-8') + ', '
            except AttributeError as e:
                address += str(self.zip_code).encode('utf-8') + ', '

        return address

    def geocoder(self, address):
        """
        takes address string as argument. Tries to find location for an address.
        timeout made me to put geocoder error
        :param address:
        :return:
        """
        geocoder = GoogleV3()
        try:
            location = geocoder.geocode(address)
        except GeocoderServiceError as e:
            print e

        if location:
            if location.latitude:
                self.latitude = location.latitude
            if location.longitude:
                self.longitude = location.longitude

    @property
    def serialize(self): 
        return {
            "id": self.id,
            "address": {
                "street_address":  self.street_address,
                "landmark":  self.landmark,
                "zip_code":  self.zip_code,
                "city":  self.city,
                "state":  self.state,
                "country":  self.country
            },
            "location": {
                "type": "Point",
                "coordinates": [self.latitude, self.longitude]
            }
        }


class CustomerAddress(db.Model): 
    cust = db.Column(
        db.Integer,
        db.ForeignKey('customer.id', ondelete='CASCADE'),
        primary_key=True
    )
    addr = db.Column(
        db.Integer,
        db.ForeignKey('address.id', ondelete='CASCADE'),
        primary_key=True
    )
    details = db.Column(db.String)

    def __init__(self, customer_id, address_id): 
        self.cust = customer_id
        self.addr = address_id

    def add_details(self, details): 
        self.details = details


class CustomerAdmin(db.Model): 
    cust = db.Column(
        db.Integer,
        db.ForeignKey('customer.id', ondelete='CASCADE'),
        primary_key=True
    )
    admin = db.Column(
        db.Integer,
        db.ForeignKey('user.id', ondelete='CASCADE'),
        primary_key=True
    )
    details = db.Column(db.String)

    def __init__(self, customer_id, user_id): 
        self.cust = customer_id
        self.admin = user_id

    def add_details(self, details): 
        self.details = details
