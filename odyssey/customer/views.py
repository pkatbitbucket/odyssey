# -*- coding: utf-8 -*-
import datetime
from flask import jsonify, request, Blueprint, g
from odyssey import app, db, auth
from odyssey.customer.models import Customer, Address, CustomerAddress, CustomerAdmin
from odyssey.user.models import Admin


@app.route('/v3/customers/add', methods=["POST"])
@auth.login_required
def create_customer():
    customer_obj = add_customer(request)
    user = g.user
    CustomerAdmin(customer_obj.id, user.id)
    return jsonify(
        status=200,
        message='Ok',
        id=customer_obj.id
    )


@app.route('/v3/customers/<customer_id>/edit', methods=["POST"])
@auth.login_required
def edit_customer(customer_id):
    customer_dict = request.json.get(customer_id)
    customer_obj = Customer.query.get(customer_id)
    customer_obj.name = customer_dict.get('customer_name')
    customer_obj.username = customer_dict.get('customer_phone')
    customer_obj.details = customer_dict.get('details')
    customer_obj.email = customer_dict.get('email')
    db.session.commit()
    return jsonify(
        status=200,
        message='Ok',
        id=customer_obj.id
    )


@app.route('/v3/address/add', methods=["POST"])
@auth.login_required
def create_address():
    address = add_address(request)
    return jsonify(
        status=200,
        message='Ok',
        id=address.id
    )


@app.route('/v3/address/<address_id>/edit', methods=["POST"])
@auth.login_required
def edit_address_of_a_customer(address_id):
    address = Address.query.get(address_id)
    address = edit_address(request, address)
    return jsonify(
        status=200,
        message='Ok',
        id=address.id
    )


@app.route('/v3/customers/<customer_id>/address', methods=['GET'])
@auth.login_required
def fetch_all_address_of_customer(customer_id):
    customeraddresses = CustomerAddress.query.filter_by(cust=customer_id)
    addresses = [Address.query.get(addr.addr) for addr in customeraddresses]
    return jsonify(
        status=200,
        message='Ok',
        customer_id=customer_id,
        addresses=[address.serialize for address in addresses]
    )


@app.route('/v3/customers/<customer_id>/add_address', methods=['POST'])
@auth.login_required
def add_address_to_customer(customer_id):
    address = add_address(request)
    customer = Customer.query.get(customer_id)
    if customer and address:
        ca = CustomerAddress(customer.id, address.id)
        db.session.add(ca)
        db.session.commit()
    return jsonify(
        status=200,
        message='Ok'
    )


@app.route('/v3/customer_address/<customer_id>/<address_id>', methods=['POST'])
@auth.login_required
def link_customer_and_address(customer_id, address_id):
    c_a_object = CustomerAddress(customer_id, address_id)
    if not c_a_object:
        return jsonify(
        status=404,
        message='Ok'
        )
    db.session.add(c_a_object)
    db.session.commit()
    return jsonify(
        status=200,
        message='Ok'
    )


@app.route('/v3/customers/<customer_id>/delete', methods=['DELETE'])
@auth.login_required
def delete_customer(customer_id):
    user = g.user
    customer_admin = CustomerAdmin.query.filter_by(
        cust=customer_id,
        admin=user.id
    ).first()
    customer = Customer.query.get(customer_admin.cust)
    db.session.delete(customer)
    db.session.commit()

    return jsonify(
        status=200,
        message='Ok'
    )


@app.route('/v3/customers/<customer_id>/address/<address_id>/delete', methods=['DELETE'])
@auth.login_required
def delete_address_for_customer(customer_id, address_id):
    user = g.user
    customer_addr = CustomerAddress.query.filter_by(
        cust=customer_id,
        addr=address_id
    ).first()
    customer_addr_row = CustomerAddress.query.get(customer_addr.cust)
    db.session.delete(customer_addr_row)
    db.session.commit()

    return jsonify(
        status=200,
        message='Ok'
    )


@app.route('/v3/customers', methods=['GET'])
@auth.login_required
def fetch_all_customers():
    user = g.user
    customer_admin = CustomerAdmin.query.filter_by(
        admin=user.id
    ).all()

    customer_ids = [customer.cust for customer in customer_admin]
    customer_objs = [Customer.query.get(customer_id) for customer_id in customer_ids]
    customer_details = [customer.customer_with_addresses for customer in customer_objs]
    return jsonify(
        status=200,
        message='Ok',
        customer_count=len(customer_admin),
        length=len(customer_ids),
        customers=customer_details
    )


@app.route('/v3/address')
@auth.login_required
def fetch_all_address():
    user = g.user
    customers = CustomerAdmin.query.filter_by(
        admin=user.id
    ).all()
    address = []
    for customer in customers:
        address += CustomerAddress.query.filter_by(
            cust=customer.id
        ).all()

    addresses = list(set(address))
    address_details = [address.serialize for address in addresses]

    return jsonify(
        status=200,
        message='Ok',
        address=address_details
    )


def add_address(request):
    address_dict = request.json.get('address_detail')
    if address_dict:
        address = Address(
            street_address=address_dict.get('street_address'),
            landmark=address_dict.get('landmark'),
            zip_code=address_dict.get('zip_code'),
            city=address_dict.get('city'),
            state=address_dict.get('state'),
            country=address_dict.get('country')
        )
    else:
        address = Address(
            street_address=None,
            landmark=None,
            zip_code=None,
            city=None,
            state=None,
            country=None
        )

    addr = address.simple_address_format()
    if addr != "":
        address.geocoder(addr)
    db.session.add(address)
    db.session.commit()
    return address


def edit_address(request, address):
    address_dict = request.json.get('address_detail')

    address.street_address = address_dict.get('street_address'),
    address.landmark = address_dict.get('landmark'),
    address.zip_code = address_dict.get('zip_code'),
    address.city = address_dict.get('city'),
    address.state = address_dict.get('state'),
    address.country = address_dict.get('country')

    addr = address.simple_address_format()
    if addr != "":
        address.geocoder(addr)

    db.session.commit()
    return address


def add_customer(request):
    customer_dict = request.json.get('customer_detail')
    if customer_dict:
        name = customer_dict.get('name')
        username = customer_dict.get('phone')
        customer_obj = Customer(name, username)
        customer_obj.details = customer_dict.get('details')
        customer_obj.email = customer_dict.get('email')
        db.session.add(customer_obj)
        db.session.commit()
        return customer_obj
    else:
        customer_obj = Customer(None, None)
        db.session.add(customer_obj)
        db.session.commit()
        return customer_obj


def edit_customer_at_edit_task(customer_id, request):
    customer_dict = request.json.get('customer_detail')
    customer_obj = Customer.query.get(customer_id)
    customer_obj.name = customer_dict.get('name')
    customer_obj.username = customer_dict.get('phone')
    customer_obj.details = customer_dict.get('details')
    customer_obj.email = customer_dict.get('email')
    db.session.commit()
