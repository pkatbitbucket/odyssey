#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from flask import jsonify, request, g
from sqlalchemy.sql import and_
from odyssey import app, db, auth
from config import ADMIN_APP_PLAY_STORE_LINK, AGENT_APP_PLAY_STORE_LINK
from odyssey.utils import format_phone_number, send_sms_to_phone, send_fcm_message
from odyssey.groups.models import Group, GroupUserMapping
from odyssey.user.models import User, Admin, Agent
from odyssey.user.views import find_or_create_user
from odyssey.user.views import get_username_from_user_id
from odyssey.tasks.views import group_created_event_and_map_with_users
from odyssey.tasks.views import admins_group_event_and_map_with_users
from odyssey.tasks.views import agents_group_event_and_map_with_users


# status dict to convert status code into corresponding status string 
STATUS_DICT = {
    0: "Not Connected",
    1: "Pending",
    2: "Accepted",
    3: "Declined",
    4: "Blocked",
    -1: "Unknown",

    '0': "Not Connected",
    '1': "Pending",
    '2': "Accepted",
    '3': "Declined",
    '4': "Blocked",
    '-1': "Unknown"
}

NOTIFICATION_TEXT_DICT = {
    "Not Connected": "",
    "Pending": "He has not accepted the request yet",
    "Accepted": "You can track now",
    "Declined": "You can send request again",
    "Blocked": "You can not contact him",
    "Unknown": ""
}
AGENT_STATUS_DICT = {
    "agent": 1,
    "admin": 0,
}
ADMIN_STATUS_DICT = {
    "admin": 2,
    "agent": 0,
}
AGENT_DICT = {
    "agent": True,
    "admin": False,
}
ADMIN_DICT = {
    "admin": True,
    "agent": False,
}


@app.route('/v3/admin/initialize', methods=['GET'])
@auth.login_required
def initialize_admin():
    """
    initializes admin app, provides 
    all the groups, members of the groups
    and the mapping of members with groups
    related to the user
    """
    status = 200
    message = "OK"
    groups = []
    members = []
    map_group_members = []
    user_groups = []
    current_user = g.user
    user_group_mappings = current_user.group_maps
    for user_group_mapping in user_group_mappings:
        if user_group_mapping.is_admin:
            user_groups.append(Group.query.get(user_group_mapping.group_id))
    if len(user_groups) == 0:

        # creating a new default group
        new_group = create_group()

        # creating the mapping for new group with user
        update_or_create_group_user_mapping(
            new_group.id, current_user.id, admin=True,
            agent=False, admin_status=2
        )

        groups = [
            {
                "group_id": new_group.id,
                "name": new_group.name,
                "description": new_group.description,
                "avatar": new_group.avatar,
                "created_at": new_group.created_at,
                "created_by": get_username_from_user_id(new_group.created_by),
            }
        ]
        members = []
        map_group_members = []
    else:
        for group in user_groups:
            groups.append({
                "group_id": group.id,
                "name": group.name,
                "description": group.description,
                "avatar": group.avatar,
                "created_at": group.created_at,
                "created_by": get_username_from_user_id(group.created_by),
            })
            for user in group.users:
                member = {
                    "avatar": user.avatar,
                    "name": user.name,
                    "phone": user.username,
                    "organization": user.organization,
                    "description": user.description,
                    "email": user.email,
                }
                if member not in members:
                    members.append(member)

                group_map_user = filter(
                    lambda x: x.group_id == group.id, user.group_maps
                )
                group_map = {
                    "group_id": group.id,
                    "phone": user.username,
                    "is_admin": group_map_user[0].is_admin,
                    "is_agent": group_map_user[0].is_agent,
                    "status_admin": group_map_user[0].admin_status,
                    "status_agent": group_map_user[0].agent_status
                }
                map_group_members.append(group_map)
        
    return jsonify(
        status=status,
        message=message,
        groups=groups,
        members=members,
        map_group_members=map_group_members,
    )


@app.route('/v3/agent/groups', methods=['GET'])
@auth.login_required
def get_agent_groups():
    """
    
    """
    status = 200
    message = "OK"
    groups = []
    user = g.user
    for group_map in user.group_maps:
        if group_map.is_agent and group_map.has_agent_joined_group:
            group_id = group_map.group_id
            group = get_group(group_id)
            if group:
                groups.append({
                    "group_id": group.id,
                    "name": group.name,
                    "description": group.description,
                    "avatar": group.avatar,
                    "created_by": get_username_from_user_id(group.created_by),
                    "created_at": group.created_at,
                })
    return jsonify(status=status, message=message, groups=groups)


@app.route('/v3/agent/requests', methods=['GET'])
@auth.login_required
def get_pending_agent_requests():
    notifications = get_pending_requests('agent')
    return jsonify(status=200, message="OK", group_requests=notifications)


# TODO: Return pending users list.
@app.route('/v3/groups', methods=['POST'])
@auth.login_required
def create_new_group():
    name = request.json.get('name')
    avatar = request.json.get('avatar')
    description = request.json.get('description')
    agents = request.json.get('agents')
    admins = request.json.get('admins')     # current user or creator in not included
    group = create_group(name=name, avatar=avatar, description=description)
    new_agents = []
    new_admins = []
    # activity log
    current_user = g.user
    group_created_event_and_map_with_users(group,
                                           current_user.username,
                                           'GROUP_CREATED',
                                           current_user
                                           )

    # adding agents to group_user_mapping as agent and agent_status pending-1
    for agent in agents:
        user = add_member(
            group=group,
            user=agent,
            agent=True,
            agent_status=1,
        )

        new_agents.append({'user_id': user.id, 'phone': user.username})

    # add current user to admins list
    add_member(
        group=group,
        user=g.user.username,
        admin=True,
        admin_status=2,
    )
    # adding admins to group_user_mapping as agent and admin_status accepted-2
    for admin in admins:
        user = add_member(
            group=group,
            user=admin,
            admin=True,
            admin_status=2,
        )
        new_admins.append({'user_id': user.id, 'phone': user.username})
    invited_by = g.user.name if g.user.name else g.user.username

    send_group_invite(
        new_agents, agent_invite_message(group, invited_by), 'agent'
    )
    send_group_invite(
        new_admins, admin_invite_message(group, invited_by), 'admin'
    )

    admins_group_event_and_map_with_users(group, new_admins, 'ADMIN_ADDED', current_user)
    agents_group_event_and_map_with_users(group, new_agents, 'AGENT_ADDED', current_user)
    return jsonify(status=200, group_id=group.id)
    # Return pending invitations list


@app.route('/v3/groups/<group_id>/add', methods=['PUT'])
@auth.login_required
def add_member_to_group(group_id):
    agents = request.json.get('agents')
    admins = request.json.get('admins')
    new_agents = []
    new_admins = []
    members = []
    group = get_group(group_id)
    if group:
        for agent in agents:
            username = format_phone_number(agent)
            user = User.query.filter_by(username=username).first()
            group_user_mapping = []
            if user:
                group_user_mapping = GroupUserMapping.query.filter(
                    and_(GroupUserMapping.group_id == group_id),
                    (GroupUserMapping.user_id == user.id),
                    (GroupUserMapping.is_agent == True)
                ).all()
            if len(group_user_mapping) == 0:
                user = add_member(
                    group=group,
                    user=agent,
                    agent=True,
                    agent_status=1,
                )
                new_agents.append({'user_id': user.id, 'phone': user.username})
            
                member = {
                    "avatar": user.avatar,
                    "name": user.name,
                    "phone": user.username,
                    "organization": user.organization,
                    "description": user.description,
                    "email": user.email,
                }

                members.append(member)
        for admin in admins:
            username = format_phone_number(admin)
            user = User.query.filter_by(username=username).first()
            if user:
                group_user_mapping = GroupUserMapping.query.filter(
                    and_(GroupUserMapping.group_id == group_id),
                    (GroupUserMapping.user_id == user.id),
                    (GroupUserMapping.is_admin == True)
                ).all()
            if len(group_user_mapping) == 0:
                user = add_member(
                    group=group,
                    user=admin,
                    admin=True,
                    admin_status=2
                )
                member = {
                    "avatar": user.avatar,
                    "name": user.name,
                    "phone": user.username,
                    "organization": user.organization,
                    "description": user.description,
                    "email": user.email,
                }
                members.append(member)
                new_admins.append({'user_id': user.id, 'phone': user.username})
        invited_by = g.user.name if g.user.name else g.user.username
        send_group_invite(
            new_agents, agent_invite_message(group, invited_by), 'agent'
        )
        send_group_invite(
            new_admins, admin_invite_message(group, invited_by), 'admin'
        )
        current_user = g.user
        admins_group_event_and_map_with_users(group, new_admins, 'ADMIN_ADDED', current_user)
        agents_group_event_and_map_with_users(group, new_agents, 'AGENT_ADDED', current_user)
        return jsonify(status=200, message="OK", members=members)
    return jsonify(status=404, message="Group Not Found")


@app.route('/v3/groups/<group_id>', methods=['PUT'])
@auth.login_required
def update_group(group_id):

    pre_agents = request.json.get('pre_agents')   # previous
    now_agents = request.json.get('now_agents')   #
    pre_admins = request.json.get('pre_admins')     # includes existing admins
    now_admins = request.json.get('now_admins')
    name = request.json.get('name')        # Old name or updated name
    avatar = request.json.get('avatar')     # avatar old avatar or updated avatar
    description = request.json.get('description')       # old description or updated one
    group = get_group(group_id=group_id)
    members = []
    # update group info
    if group:
        group.update_group_info(name, avatar, description)
        group_created_event_and_map_with_users(group,
                                               g.user.username,
                                               'GROUP_UPDATED',
                                               g.user
                                               )
    else:
        return jsonify(status=400, message="Group not found")
        # add new members
    invited_by = g.user.name if g.user.name else g.user.username
    members = add_new_member(
        now_agents, pre_agents, group, members, "agent", invited_by
    )
    members = add_new_member(
        now_admins, pre_admins, group, members, "admin", invited_by
    )
    # removing members
    remove_member_in_bulk(pre_admins, now_admins, "admin", group)
    remove_member_in_bulk(pre_agents, now_agents, "agent", group)
    return jsonify(status=200, message="OK", members=members)


@app.route('/v3/groups/<group_id>', methods=['GET'])
@auth.login_required
def display_group(group_id):
    group = get_group(group_id)
    members = []
    map_group_members = []
    if group:
        for user in group.users:
            member = {
                "avatar": user.avatar,
                "name": user.name,
                "phone": user.username,
                "organization": user.organization,
                "description": user.description,
                "email": user.email,
            }
            members.append(member)
            group_map_user = filter(
                        lambda x: x.group_id == group.id, user.group_maps
                    )
            map_group_members.append(
                {
                    "phone": user.username,
                    "is_admin": group_map_user[0].is_admin,
                    "is_agent": group_map_user[0].is_agent,
                    "status_admin": group_map_user[0].admin_status,
                    "status_agent": group_map_user[0].agent_status,
                }
            )
        return jsonify(
            status=200,
            message="OK",
            name=group.name,
            description=group.description,
            avatar=group.avatar,
            members=members,
            map_group_members=map_group_members,
            )
    else:
        return jsonify(status=404, message="Group Not found")


@app.route('/v3/groups/<group_id>', methods=['DELETE'])
@auth.login_required
def delete_group(group_id):
    status = 404
    message = "Group not found"
    current_user_id = g.user.id
    group = Group.query.filter_by(id=group_id).first()
    all_groups = Group.query.filter_by(created_by=current_user_id).all()
    if not group:
        return jsonify(status=status, message=message)
    elif group.created_by == current_user_id and len(all_groups) > 1:
        # group_created_event_and_map_with_users(
        #     group,
        #     g.user.username,
        #     'GROUP_DELETED',
        #     g.user
        # )
        # db.session.delete(group)
        # db.session.commit()
        status = 200
        message = "OK"
    else:
        status = 403,
        message = "You are not allowed to delete the group"
    return jsonify(status=status, message=message)


@app.route('/v3/groups/relationship', methods=['PUT'])
@auth.login_required
def update_group_user_mapping():
    group_id = request.json.get('group_id')
    status = request.json.get('status')
    app_type = request.json.get('app_type')
    user_id = g.user.id
    user_group_user_mapping = None
    group_mappings = GroupUserMapping.query.filter_by(
        group_id=group_id
    ).all()
    admins_ids = []
    for group_mapping in group_mappings:
        admins_ids, user_group_user_mapping = get_admin_ids_and_mapping_ins(
            group_mapping, admins_ids, user_group_user_mapping, 
            app_type, user_id 
        )  
    if user_group_user_mapping:
        update_group_user_mapping_admin_agent(
            user_group_user_mapping, group_id, user_id,
            app_type, status, admins_ids
        )
    return jsonify(status=200, message="OK")


@app.route(
    '/v3/groups/<group_id>/<username>/<app_type>/leave_group', methods=['PUT']
)
@auth.login_required
def leave_group(group_id, username, app_type):
    user_id = User.query.filter_by(username=username).first().id
    group_user_mapping = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group_id),
        (GroupUserMapping.user_id == user_id)
    ).first()
    if not group_user_mapping:
        return jsonify(status=404, message="Mapping not found")
    if app_type == 'admin':
        group_user_mapping.remove_admin_mapping()
    if app_type == 'agent':
        group_user_mapping.remove_agent_mapping()
    return jsonify(status=200, message="OK")


def get_pending_requests(app_type='agent'):
    user = g.user
    notifications = []

    if app_type == 'agent':
        for group_map in user.group_maps:
            # agent status 1 for pending requests
            if group_map.is_agent and not group_map.has_agent_joined_group \
            and group_map.agent_status == 1:
                group = get_group(group_map.group_id)
                if group:
                    user = User.query.get(group.created_by)
                    invited_by = user.name if user.name else user.username
                    notification = {
                        "notification_version": "1",
                        "notification_type": "user_relationship",
                        "notification_action": "added",
                        "notification_title": "{0} has invited you to join {1}".format(
                            invited_by.encode('utf-8'), group.name
                        ),
                        "notification_text": "Would you like to join?",
                        'group_id': group.id,
                        'group_name': group.name,
                        'group_avatar': group.avatar,
                        'group_description': group.description,
                        "notification_time": str(datetime.datetime.now())
                    }
                    notifications.append(notification)
    return notifications


def notify_admin(requesting_user_ids, group_id, user_id, status):
    admin_gcm_ids = []
    for requesting_user_id in requesting_user_ids:
        admin = Admin.query.filter_by(user_id=requesting_user_id).first()
        if admin:
            admin_gcm_ids.append(admin.get_gcm())
    group = Group.query.get(group_id)
    user = User.query.get(user_id)
    send_fcm_message(admin_notify_message(user, group, status), admin_gcm_ids)


# TODO: do not send sms to users who have the app
def send_group_invite(user_list, message, app_type):

    if app_type == 'admin':
        send_group_invite_to_admins(user_list, message)

    if app_type == 'agent':
        send_group_invite_to_agents(user_list, message)


def send_group_invite_to_agents(user_list, message):
    agent_registration_ids = []
    agent_sms_list = []
    for user in user_list:
        gcm_user = Agent.query.filter_by(user_id=user['user_id']).first()
        if gcm_user and gcm_user.get_gcm() == '':
            agent_sms_list.append(user['phone'])
        elif gcm_user and gcm_user.get_gcm() != '':
            agent_registration_ids.append(gcm_user.get_gcm())
        else:
            agent_sms_list.append(user['phone'])
    send_fcm_or_sms_to_members(
        agent_sms_list, "agent", agent_registration_ids, message
    )


def send_group_invite_to_admins(user_list, message):
    admin_registration_ids = []
    admin_sms_list = []
    for user in user_list:
        gcm_user = Admin.query.filter_by(user_id=user['user_id']).first()
        if gcm_user and gcm_user.get_gcm() == '':
            admin_sms_list.append(user['phone'])
        elif gcm_user and gcm_user.get_gcm() != '':
            admin_registration_ids.append(gcm_user.get_gcm())
        else:
            admin_sms_list.append(user['phone'])
    send_fcm_or_sms_to_members(
        admin_sms_list, "admin", admin_registration_ids, message
    )


def send_fcm_or_sms_to_members(
    member_sms_list, app_type, member_registration_ids, message
):
    if len(member_sms_list) > 0:
        send_sms_invite(member_sms_list, app_type)
    if len(member_registration_ids) > 0:
        send_fcm_message(message, member_registration_ids)


def send_sms_invite(recipients, app_type):
    invite_message = ''
    username = g.user.name if not g.user.name == "" else g.user.username
    if app_type == 'agent':
        invite_message = username + \
            " has invited you to try Loktra Agent app. Download it here: " \
            + AGENT_APP_PLAY_STORE_LINK
    if app_type == 'admin':
        invite_message = username + \
            " has invited you to try Loktra app.\ Download it here: " + \
            ADMIN_APP_PLAY_STORE_LINK
    for recipient in recipients:
        send_sms_to_phone(recipient, invite_message)


def get_group(group_id):
    return Group.query.get(group_id)


def create_group(name="Welcome", description="First Group",
                 avatar='https://goo.gl/rBaqKu'):
    group = Group(name=name, description=description,
                  avatar=avatar, created_by=g.user.id)
    db.session.add(group)
    db.session.commit()
    return group


# TODO: Use bulk remove query and use sets
def remove_member_in_bulk(pre_members, now_members, app_type, group):
    for pre_member in pre_members:
        if pre_member not in now_members:
            user = User.query.filter_by(username=pre_member).first()
            if user:
                remove_member(group, user, app_type)


def remove_member(group, user, app_type):
    user_map = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group.id),
        (GroupUserMapping.user_id == user.id)
    ).first_or_404()
    if app_type == 'admin':
        user_map.remove_admin_mapping()
        remove_user = {'user_id': user.id, 'phone': user.username}
        admins_group_event_and_map_with_users(group, remove_user, 'ADMIN_REMOVED', g.user)
    elif app_type == 'agent':
        user_map.remove_agent_mapping()
        remove_user = {'user_id': user.id, 'phone': user.username}
        agents_group_event_and_map_with_users(group, remove_user, 'AGENT_REMOVED', g.user)


def add_member(
        group=None, user=None, agent=False,
        admin=False, agent_status=0, admin_status=0
):
    if group and user:
        agent_formatted = format_phone_number(user)
        if admin:
            user = find_or_create_user(agent_formatted, 'admin')
        else:
            user = find_or_create_user(agent_formatted, 'agent')

        update_or_create_group_user_mapping(
            group_id=group.id,
            user_id=user.id,
            admin=admin,
            agent=agent,
            admin_status=admin_status,
            agent_status=agent_status,
        )
    return user


def update_or_create_group_user_mapping(
        group_id, user_id, admin=False,
        agent=False, agent_status=0, admin_status=0
):

    group_user_mapping = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group_id),
        (GroupUserMapping.user_id == user_id)
    ).first()
    if group_user_mapping:
        update_user_mapping(
            group_user_mapping, admin, agent, agent_status, admin_status
        )
    else:
        group_user_mapping = create_user_mapping(
            group_id, user_id, agent, admin, agent_status, admin_status
        )
    return group_user_mapping


def create_user_mapping(
        group_id, user_id, agent, admin, agent_status, admin_status
):
    group_user_mapping = GroupUserMapping(group_id, user_id)
    if agent:
        group_user_mapping.make_agent()
    if admin:
        group_user_mapping.make_admin()
    if agent_status == 2:
        group_user_mapping.update_agent_join_group()
        group_user_mapping.update_agent_status(agent_status)
    elif agent_status:
        group_user_mapping.update_agent_status(agent_status)
    if admin_status == 2:
        group_user_mapping.update_admin_join_group()
        group_user_mapping.update_admin_status(admin_status)
    elif admin_status:
        group_user_mapping.update_admin_status(admin_status)
    db.session.add(group_user_mapping)
    db.session.commit()
    return group_user_mapping


def update_user_mapping(
        group_user_mapping, admin, agent, agent_status, admin_status
):
    if agent:
        group_user_mapping.make_agent()
    if admin:
        group_user_mapping.make_admin()
    if agent_status == 1 and group_user_mapping.agent_status == 2:
        pass
    elif agent_status == 2:
        group_user_mapping.update_agent_join_group()
        group_user_mapping.update_agent_status(agent_status)
    elif agent_status:
        group_user_mapping.update_agent_status(agent_status)
    if admin_status == 2:
        group_user_mapping.update_admin_join_group()
        group_user_mapping.update_admin_status(admin_status)
    elif admin_status:
        group_user_mapping.update_admin_status(admin_status)    
    db.session.commit()
    return group_user_mapping


def agent_invite_message(group, invited_by):
    invite_message = {
        "notification_version": "1",
        "notification_type": "user_relationship",
        "notification_action": "added",
        "notification_title": "{0} has invited you to join {1}".format(
            invited_by.encode('utf-8'), group.name.encode('utf-8')
        ),
        "notification_text": "Would you like to join?",
        'group_id': group.id,
        'group_name': group.name.encode('utf-8'),
        'group_avatar': group.avatar,
        'group_description': group.description.encode('utf-8'),
        "notification_time": str(
            datetime.datetime.now().strftime("%a %d, %b %I:%M %p"))
    }
    return invite_message


def admin_invite_message(group, invited_by):
    invite_message = {
        "notification_version": "1",
        "notification_type": "user_relationship",
        "notification_action": "added",
        "notification_title": "{0} has added you to {1}".format(
            invited_by.encode('utf-8'), group.name.encode('utf-8')
        ),
        "notification_text": "Enjoy the group activities",
        'group_id': group.id,
        'group_name': group.name.encode('utf-8'),
        'group_avatar': group.avatar,
        'group_description': group.description.encode('utf-8'),
        "notification_time": str(
            datetime.datetime.now().strftime("%a %d, %b %I:%M %p"))
    }
    return invite_message


def admin_notify_message(user, group, status):
    user_name = user.name if user.name else user.username
    notify_message = {
        "notification_version": "1",
        "notification_type": "user_relationship",
        "notification_action": STATUS_DICT[status],
        "notification_title": "{0} has {1} to {2}".format(
            user_name.encode('utf-8'), STATUS_DICT[status],
            group.name.encode('utf-8')
        ),
        "notification_text": NOTIFICATION_TEXT_DICT[STATUS_DICT[status]],
        "status": status,
        "phone": user.username,
        "name": user.name,
        'group_id': group.id,
        'group_name': group.name.encode('utf-8'),
        "notification_time": str(
            datetime.datetime.now().strftime("%a %d, %b %I:%M %p"))
    }
    return notify_message


def add_new_member(
    now_members, pre_members, group, members, app_type, invited_by
):
    new_members = []
    for now_member in now_members:
        if now_member not in pre_members:
            user = add_member(
                group,
                now_member,
                agent=AGENT_DICT[app_type],
                admin=ADMIN_DICT[app_type],
                agent_status=AGENT_STATUS_DICT[app_type],
                admin_status=ADMIN_STATUS_DICT[app_type],
            )
            new_members.append(
                {'user_id': user.id, 'phone': user.username}
            )
            member = {
                "avatar": user.avatar,
                "name": user.name,
                "phone": user.username,
                "organization": user.organization,
                "description": user.description,
                "email": user.email,
            }
            members.append(member)
    send_group_invite(
        new_members, admin_invite_message(group, invited_by), app_type
    )
    return members


def update_group_user_mapping_admin_agent(
    user_group_user_mapping, group_id, user_id, app_type, status, admins_ids
):
    if app_type == 'admin':
        user_group_user_mapping.update_admin_status(status)
        if status == 2:
            user_group_user_mapping.update_admin_join_group()
        db.session.commit()
        notify_admin(admins_ids, group_id, user_id, status)

    if app_type == 'agent':
        user_group_user_mapping.update_agent_status(status)
        if status == 2:
            user_group_user_mapping.update_agent_join_group()
        if status == 3:
            user_group_user_mapping.remove_agent_mapping()
        db.session.commit()
        notify_admin(admins_ids, group_id, user_id, status)
        group = Group.query.get(group_id)
        current_user = User.query.get(user_id)
        new_agents = [{'user_id': current_user.id, 'phone': current_user.username}]
        agents_group_event_and_map_with_users(group, new_agents, 'REQUEST_ACCEPTED', current_user)


def get_admin_ids_and_mapping_ins(
    group_mapping, admins_ids, user_group_user_mapping, app_type, user_id
):
    if group_mapping.is_admin and group_mapping.has_admin_joined_group:
        admins_ids.append(group_mapping.user_id)
    if app_type == 'agent' and group_mapping.is_agent \
            and group_mapping.user_id == user_id:
        user_group_user_mapping = group_mapping
    elif app_type == 'admin' and group_mapping.is_admin \
            and group_mapping.user_id == user_id:
        user_group_user_mapping = group_mapping
    return admins_ids, user_group_user_mapping

########### v2 apis below #################################

@app.route('/v2/admin/initialize', methods=['GET'])
@auth.login_required
def v2_initialize_admin():
    """
    initializes admin app, provides 
    all the groups, members of the groups
    and the mapping of members with groups
    related to the user
    """
    status = 200
    message = "OK"
    groups = []
    members = []
    map_group_members = []
    user_groups = []
    current_user = g.user
    user_group_mappings = current_user.group_maps
    for user_group_mapping in user_group_mappings:
        if user_group_mapping.is_admin:
            user_groups.append(Group.query.get(user_group_mapping.group_id))
    if len(user_groups) == 0:

        # creating a new default group
        new_group = create_group()

        # creating the mapping for new group with user
        update_or_create_group_user_mapping(
            new_group.id, current_user.id, admin=True,
            agent=False, admin_status=2
        )

        groups = [
            {
                "group_id": new_group.id,
                "name": new_group.name,
                "description": new_group.description,
                "avatar": new_group.avatar,
                "created_at": new_group.created_at,
                "created_by": get_username_from_user_id(new_group.created_by),
            }
        ]
        members = []
        map_group_members = []
    else:
        for group in user_groups:
            groups.append({
                "group_id": group.id,
                "name": group.name,
                "description": group.description,
                "avatar": group.avatar,
                "created_at": group.created_at,
                "created_by": get_username_from_user_id(group.created_by),
            })
            for user in group.users:
                member = {
                    "avatar": user.avatar,
                    "name": user.name,
                    "phone": user.username,
                    "organization": user.organization,
                    "description": user.description,
                    "email": user.email,
                }
                if member not in members:
                    members.append(member)

                group_map_user = filter(
                    lambda x: x.group_id == group.id, user.group_maps
                )
                group_map = {
                    "group_id": group.id,
                    "phone": user.username,
                    "is_admin": group_map_user[0].is_admin,
                    "is_agent": group_map_user[0].is_agent,
                    "status_admin": group_map_user[0].admin_status,
                    "status_agent": group_map_user[0].agent_status
                }
                map_group_members.append(group_map)
        
    return jsonify(
        status=status,
        message=message,
        groups=groups,
        members=members,
        map_group_members=map_group_members,
    )


@app.route('/v2/agent/groups', methods=['GET'])
@auth.login_required
def v2_get_agent_groups():
    """
    
    """
    status = 200
    message = "OK"
    groups = []
    user = g.user
    for group_map in user.group_maps:
        if group_map.is_agent and group_map.has_agent_joined_group:
            group_id = group_map.group_id
            group = get_group(group_id)
            if group:
                groups.append({
                    "group_id": group.id,
                    "name": group.name,
                    "description": group.description,
                    "avatar": group.avatar,
                    "created_by": get_username_from_user_id(group.created_by),
                    "created_at": group.created_at,
                })
    return jsonify(status=status, message=message, groups=groups)


@app.route('/v2/agent/requests', methods=['GET'])
@auth.login_required
def v2_get_pending_agent_requests():
    notifications = get_pending_requests('agent')
    return jsonify(status=200, message="OK", group_requests=notifications)


# TODO: Return pending users list.
@app.route('/v2/groups', methods=['POST'])
@auth.login_required
def v2_create_new_group():
    name = request.json.get('name')
    avatar = request.json.get('avatar')
    description = request.json.get('description')
    agents = request.json.get('agents')
    admins = request.json.get('admins')     # current user or creator in not included
    group = create_group(name=name, avatar=avatar, description=description)
    new_agents = []
    new_admins = []
    # activity log
    current_user = g.user
    group_created_event_and_map_with_users(group,
                                           current_user.username,
                                           'GROUP_CREATED',
                                           current_user
                                           )

    # adding agents to group_user_mapping as agent and agent_status pending-1
    for agent in agents:
        user = add_member(
            group=group,
            user=agent,
            agent=True,
            agent_status=1,
        )

        new_agents.append({'user_id': user.id, 'phone': user.username})

    # add current user to admins list
    add_member(
        group=group,
        user=g.user.username,
        admin=True,
        admin_status=2,
    )
    # adding admins to group_user_mapping as agent and admin_status accepted-2
    for admin in admins:
        user = add_member(
            group=group,
            user=admin,
            admin=True,
            admin_status=2,
        )
        new_admins.append({'user_id': user.id, 'phone': user.username})
    invited_by = g.user.name if g.user.name else g.user.username

    send_group_invite(
        new_agents, agent_invite_message(group, invited_by), 'agent'
    )
    send_group_invite(
        new_admins, admin_invite_message(group, invited_by), 'admin'
    )

    admins_group_event_and_map_with_users(group, new_admins, 'ADMIN_ADDED', current_user)
    agents_group_event_and_map_with_users(group, new_agents, 'AGENT_ADDED', current_user)
    return jsonify(status=200, group_id=group.id)
    # Return pending invitations list


@app.route('/v2/groups/<group_id>/add', methods=['PUT'])
@auth.login_required
def v2_add_member_to_group(group_id):
    agents = request.json.get('agents')
    admins = request.json.get('admins')
    new_agents = []
    new_admins = []
    members = []
    group = get_group(group_id)
    if group:
        for agent in agents:
            username = format_phone_number(agent)
            user = User.query.filter_by(username=username).first()
            group_user_mapping = []
            if user:
                group_user_mapping = GroupUserMapping.query.filter(
                    and_(GroupUserMapping.group_id == group_id),
                    (GroupUserMapping.user_id == user.id),
                    (GroupUserMapping.is_agent == True)
                ).all()
            if len(group_user_mapping) == 0:
                user = add_member(
                    group=group,
                    user=agent,
                    agent=True,
                    agent_status=1,
                )
                new_agents.append({'user_id': user.id, 'phone': user.username})
            
                member = {
                    "avatar": user.avatar,
                    "name": user.name,
                    "phone": user.username,
                    "organization": user.organization,
                    "description": user.description,
                    "email": user.email,
                }

                members.append(member)
        for admin in admins:
            username = format_phone_number(admin)
            user = User.query.filter_by(username=username).first()
            if user:
                group_user_mapping = GroupUserMapping.query.filter(
                    and_(GroupUserMapping.group_id == group_id),
                    (GroupUserMapping.user_id == user.id),
                    (GroupUserMapping.is_admin == True)
                ).all()
            if len(group_user_mapping) == 0:
                user = add_member(
                    group=group,
                    user=admin,
                    admin=True,
                    admin_status=2
                )
                member = {
                    "avatar": user.avatar,
                    "name": user.name,
                    "phone": user.username,
                    "organization": user.organization,
                    "description": user.description,
                    "email": user.email,
                }
                members.append(member)
                new_admins.append({'user_id': user.id, 'phone': user.username})
        invited_by = g.user.name if g.user.name else g.user.username
        send_group_invite(
            new_agents, agent_invite_message(group, invited_by), 'agent'
        )
        send_group_invite(
            new_admins, admin_invite_message(group, invited_by), 'admin'
        )
        current_user = g.user
        admins_group_event_and_map_with_users(group, new_admins, 'ADMIN_ADDED', current_user)
        agents_group_event_and_map_with_users(group, new_agents, 'AGENT_ADDED', current_user)
        return jsonify(status=200, message="OK", members=members)
    return jsonify(status=404, message="Group Not Found")


@app.route('/v2/groups/<group_id>', methods=['PUT'])
@auth.login_required
def v2_update_group(group_id):

    pre_agents = request.json.get('pre_agents')   # previous
    now_agents = request.json.get('now_agents')   #
    pre_admins = request.json.get('pre_admins')     # includes existing admins
    now_admins = request.json.get('now_admins')
    name = request.json.get('name')        # Old name or updated name
    avatar = request.json.get('avatar')     # avatar old avatar or updated avatar
    description = request.json.get('description')       # old description or updated one
    group = get_group(group_id=group_id)
    members = []
    # update group info
    if group:
        group.update_group_info(name, avatar, description)
        group_created_event_and_map_with_users(group,
                                               g.user.username,
                                               'GROUP_UPDATED',
                                               g.user
                                               )
    else:
        return jsonify(status=400, message="Group not found")
        # add new members
    invited_by = g.user.name if g.user.name else g.user.username
    members = add_new_member(
        now_agents, pre_agents, group, members, "agent", invited_by
    )
    members = add_new_member(
        now_admins, pre_admins, group, members, "admin", invited_by
    )
    # removing members
    remove_member_in_bulk(pre_admins, now_admins, "admin", group)
    remove_member_in_bulk(pre_agents, now_agents, "agent", group)
    return jsonify(status=200, message="OK", members=members)


@app.route('/v2/groups/<group_id>', methods=['GET'])
@auth.login_required
def v2_display_group(group_id):
    group = get_group(group_id)
    members = []
    map_group_members = []
    if group:
        for user in group.users:
            member = {
                "avatar": user.avatar,
                "name": user.name,
                "phone": user.username,
                "organization": user.organization,
                "description": user.description,
                "email": user.email,
            }
            members.append(member)
            group_map_user = filter(
                        lambda x: x.group_id == group.id, user.group_maps
                    )
            map_group_members.append(
                {
                    "phone": user.username,
                    "is_admin": group_map_user[0].is_admin,
                    "is_agent": group_map_user[0].is_agent,
                    "status_admin": group_map_user[0].admin_status,
                    "status_agent": group_map_user[0].agent_status,
                }
            )
        return jsonify(
            status=200,
            message="OK",
            name=group.name,
            description=group.description,
            avatar=group.avatar,
            members=members,
            map_group_members=map_group_members,
            )
    else:
        return jsonify(status=404, message="Group Not found")


@app.route('/v2/groups/<group_id>', methods=['DELETE'])
@auth.login_required
def v2_delete_group(group_id):
    status = 404
    message = "Group not found"
    current_user_id = g.user.id
    group = Group.query.filter_by(id=group_id).first()
    all_groups = Group.query.filter_by(created_by=current_user_id).all()
    if not group:
        return jsonify(status=status, message=message)
    elif group.created_by == current_user_id and len(all_groups) > 1:
        group_created_event_and_map_with_users(group,
                                               g.user.username,
                                               'GROUP_DELETED',
                                               g.user
                                               )
        db.session.delete(group)
        GroupUserMapping.query.filter_by(group_id=group_id).delete()
        db.session.commit()
        status = 200
        message = "OK"
    else:
        status = 403,
        message = "You are not allowed to delete the group"
    return jsonify(status=status, message=message)


@app.route('/v2/groups/relationship', methods=['PUT'])
@auth.login_required
def v2_update_group_user_mapping():
    group_id = request.json.get('group_id')
    status = request.json.get('status')
    app_type = request.json.get('app_type')
    user_id = g.user.id
    user_group_user_mapping = None
    group_mappings = GroupUserMapping.query.filter_by(
        group_id=group_id
    ).all()
    admins_ids = []
    for group_mapping in group_mappings:
        admins_ids, user_group_user_mapping = get_admin_ids_and_mapping_ins(
            group_mapping, admins_ids, user_group_user_mapping, 
            app_type, user_id 
        )  
    if user_group_user_mapping:
        update_group_user_mapping_admin_agent(
            user_group_user_mapping, group_id, user_id,
            app_type, status, admins_ids
        )
    return jsonify(status=200, message="OK")


@app.route(
    '/v2/groups/<group_id>/<username>/<app_type>/leave_group', methods=['PUT']
)
@auth.login_required
def v2_leave_group(group_id, username, app_type):
    user_id = User.query.filter_by(username=username).first().id
    group_user_mapping = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group_id),
        (GroupUserMapping.user_id == user_id)
    ).first()
    if not group_user_mapping:
        return jsonify(status=404, message="Mapping not found")
    if app_type == 'admin':
        group_user_mapping.remove_admin_mapping()
    if app_type == 'agent':
        group_user_mapping.remove_agent_mapping()
    return jsonify(status=200, message="OK")
