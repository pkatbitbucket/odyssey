# -*- coding: utf-8 -*-
from odyssey import db
import datetime


class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, default="Welcome")
    description = db.Column(db.String, default='First Group')
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    avatar = db.Column(db.String)
    users = db.relationship(
        'User', secondary='group_user_mapping', backref='groups',
    )
    created_by_user = db.relationship('User')
    tasks = db.relationship('Task', backref='groups', lazy='dynamic')

    def __init__(self, created_by, name='Welcome', description='First Group',
                 avatar='https://goo.gl/rBaqKu'):
        self.name = name
        self.description = description
        self.avatar = avatar
        self.created_by = created_by

    def __repr__(self):
        return self.name

    def update_group_info(self, name, avatar, description):
        if name:
            self.name = name
        if avatar:
            self.avatar = avatar
        if description:
            self.description = description
        db.session.commit()

class GroupUserMapping(db.Model):
    """
       Mapping of users and corresponding groups.
    """
    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey(
        'group.id', ondelete='CASCADE'
    ))
    user_id = db.Column(db.Integer, db.ForeignKey(
        'user.id', ondelete='CASCADE'
    ))
    is_admin = db.Column(db.Boolean, default=False)
    is_agent = db.Column(db.Boolean, default=False)
    group_request_admin_on = db.Column(db.DateTime, nullable=True)
    group_request_agent_on = db.Column(db.DateTime, nullable=True)

    has_agent_joined_group = db.Column(db.Boolean, default=False)
    agent_joined_group_on = db.Column(db.DateTime, nullable=True)
    has_admin_joined_group = db.Column(db.Boolean, default=False)
    admin_joined_group_on = db.Column(db.DateTime, nullable=True)

    agent_status = db.Column(db.Integer, default=0, nullable=False)  
    # 0-Not connected, 1-Pending, 2-Accepted, 3-Declined, 4-Blocked
    admin_status = db.Column(db.Integer, default=0, nullable=False)
    # time at which admin acted
    admin_action_on = db.Column(db.DateTime, nullable=True)
    # time at which agent acted
    agent_action_on = db.Column(db.DateTime, nullable=True)
    group = db.relationship('Group', backref='group_user_mappings')

    def __init__(self, group_id, user_id):
        self.group_id = group_id
        self.user_id = user_id

    def __repr__(self):
        return '<Group %r Mapped with %r>' % (self.group_id, self.user_id)
    
    def make_admin(self):
        self.is_admin = True
        self.group_request_admin_on = datetime.datetime.now()

    def make_agent(self):
        self.is_agent = True
        self.group_request_agent_on = datetime.datetime.now()

    def update_agent_status(self, status):
        self.agent_status = status
        self.agent_action_on = datetime.datetime.now()

    def update_admin_status(self, status):
        self.admin_status = status
        self.admin_action_on = datetime.datetime.now()

    def update_admin_join_group(self):
        self.has_admin_joined_group = True
        self.admin_joined_group_on = datetime.datetime.now()

    def update_agent_join_group(self):
        self.has_agent_joined_group = True
        self.agent_joined_group_on = datetime.datetime.now()

    def remove_admin_mapping(self):
        self.is_admin = False
        self.has_admin_joined_group = False
        self.admin_status = 0
        if self.is_agent is False:
            db.session.delete(self)
        db.session.commit()

    def remove_agent_mapping(self):
        self.is_agent = False
        self.has_agent_joined_group = False
        self.agent_status = 0
        if self.is_admin is False:
            db.session.delete(self)
        db.session.commit()

    @property
    def serialize(self):
        return {
            'user_id': self.user_id,
            'admin': self.is_admin,
            'agent': self.is_agent,
            'admin_status': self.admin_status,
            'agent_status': self.agent_status,
        }
