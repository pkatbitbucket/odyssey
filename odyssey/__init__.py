# -*- coding: utf-8 -*-
import os

import logging
import logging.config
import loggly.handlers

from redis import Redis
from celery import Celery
from flask import Flask, request, jsonify
from werkzeug.contrib.fixers import ProxyFix
from flask_sqlalchemy import SQLAlchemy
from flask_sockets import Sockets
from flask_httpauth import HTTPBasicAuth
from flask_redis import FlaskRedis
from redis import StrictRedis
from opbeat.contrib.flask import Opbeat
from flask_cors import CORS, cross_origin
import filters

# Flask odyssey initialization parameters
app = Flask(__name__)  # The WSGI compliant web application object
app.register_blueprint(filters.blueprint)
app.config.from_object(os.environ['APP_SETTINGS'])

cors = CORS(app, resources={r"/foo": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'

db = SQLAlchemy(app)  # Setup Flask-SQLAlchemy
sockets = Sockets(app)
auth = HTTPBasicAuth()
# Fixer for handling proxied request due to headers in Nginx configuration.
app.wsgi_app = ProxyFix(app.wsgi_app)
redis_store = FlaskRedis.from_custom_provider(StrictRedis, app)

opbeat = Opbeat(
    app,
    organization_id=app.config["OPBEAT_ORGANIZATION_ID"],
    app_id=app.config["OPBEAT_APP_ID"],
    secret_token=app.config["OPBEAT_SECRET_TOKEN"],
    logging=True
)


@app.route('/')
def homepage():
    """
    Unauthenticated page to test if web server is running.
    :return: Text with home page.
    """
    return jsonify(home='Home page')


# @app.before_first_request
# def configure_logging():
#     """
#     Configures the log files and logging formats
#     :return: HTTP 200 status code
#     """
#     logging.config.fileConfig('python.conf')
#     logger = logging.getLogger('myLogger')

#     logger.info('Test log')
