# -*- coding: utf-8 -*-
import os
from random import randint
from uuid import uuid4
from pyfcm import FCMNotification
from config import FCM_API_KEY
import boto3
import plivo
import xmltodict
from phonenumbers import PhoneNumberFormat, NumberParseException
from phonenumbers import parse, is_valid_number, format_number

from odyssey import app
from config import PLIVO_AUTH_TOKEN, PLIVO_AUTH_ID, PLIVO_PHONE_NUMBER

AMAZON_S3_VERIFICATION_CODE_URL = app.config['AMAZON_S3_VERIFICATION_CODE_URL']


def get_id():
    """
    Generates UUID and converts it in hex.
    :return: last part of UUID4 converted to hex
    """
    return hex(uuid4().fields[-1])[2:-1]


def generate_random_code():
    """
    Generates a six digit random code.
    :return: random number between 100000, 999999
    """
    return randint(100000, 999999)


def generate_n_digit_random_code(N):
    """
    Generates a N digit random code.
    :param N: Random code which need to be generated.
    :return: random number of N digits.
    """
    range_start = 10 ** (N - 1)
    range_end = (10 ** N) - 1
    return randint(range_start, range_end)


def send_to_phone(phone, code):
    """
    sends a verification code to phone.
    :param phone: phone number to which to send the code. Should be in E164 format without a "+".
    :param code: verification code that gets sent
    :return: plivo response message with status code.
    """
    message = u'Loktra-' + code + u' is your verification code.'
    return send_sms_to_phone(phone, message)


def send_sms_to_phone(phone, message):
    """
    send sms to phone.
    :param phone: phone number to which to send the code. Should be in E164 format without a "+".
    :param message: message to be sent.
    :return: plivo response message with status code.
    """
    p = plivo.RestAPI(PLIVO_AUTH_ID, PLIVO_AUTH_TOKEN)
    params = {
        'src': PLIVO_PHONE_NUMBER,  # Sender's phone number with country code
        'dst': phone,  # Receiver's phone Number with country code
        'text': message,  # Your SMS Text Message - English
        'url': "http://example.com/report/",  # The URL to which with the status of the message is sent
        'method': 'POST'  # The method used to call the url
    }
    return p.send_message(params)


def write_to_s3(phone, code):
    """
    Saves the verification code on S3 for making it readable by plivo API.
    Repeats the message thrice for easy understanding.
    :param phone: phone number to which to send the code. Should be in E164 format without a "+".
    :param code: verification code that gets sent
    :return: None
    """
    message = "Your verification code is " + convert_number_to_text(code)
    speaking_text = {
        "Response": {
            "Speak": message + ". " + message + ". " + message + ". "
        }
    }
    xml_data = xmltodict.unparse(speaking_text, pretty=True)
    s3 = boto3.resource('s3')
    file_name = str(phone) + ".xml"
    s3.Bucket('com.loktra.verification').put_object(Key=file_name, Body=xml_data, ContentType='text/xml')


def call_to_phone(phone):
    """
    Calls and speaks the verification code.
    :param phone: phone number to which to send the code. Should be in E164 format without a "+".
    :return: plivo response message with status code.
    """
    p = plivo.RestAPI(PLIVO_AUTH_ID, PLIVO_AUTH_TOKEN)
    params = {
        'to': phone,  # The phone numer to which the call will be placed
        'from': PLIVO_PHONE_NUMBER,  # The phone number to be used as the caller id

        # answer_url is the URL invoked by Plivo when the outbound call is answered
        # and contains instructions telling Plivo what to do with the call
        'answer_url': AMAZON_S3_VERIFICATION_CODE_URL + str(phone) + ".xml",
        'answer_method': "GET",  # The method used to call the answer_url

        # Example for asynchronous request
        # callback_url is the URL to which the API response is sent.
        # 'callback_url' => "http://myvoiceapp.com/callback/",
        # 'callback_method' => "GET" # The method used to notify the callback_url.
    }

    # Make an outbound call and print the response
    # print params['answer_url']
    return p.make_call(params)


def convert_number_to_text(number):
    """
    Takes number as input and returns its corresponding string. Eg: convert_number_to_text(123) returns one two three.
    :param number: number
    :return: string after converting string literally.
    """
    NUM_TO_STR_DICT = {
        '0': 'zero',
        '1': 'one',
        '2': 'two',
        '3': 'three',
        '4': 'four',
        '5': 'five',
        '6': 'six',
        '7': 'seven',
        '8': 'eight',
        '9': 'nine',
    }
    number_code = str(number)
    text_string = []
    for i in range(0, len(number_code)):
        number = number_code[i]
        text_string.append(NUM_TO_STR_DICT[number])

    return ' '.join(text_string)


def get_access_code():
    """
    A code that can be used as secret key for flask odyssey.
    :return: return 16 digit random code in hex.
    """
    return os.urandom(16).encode('hex')


def validate_phone(phone_num):
    """
    Validates if the number is valid, i.e., it's an assigned exchange.
    :param phone_num:
    :return: True if number is valid, False otherwise.
    """
    try:
        z = parse(phone_num)
        return is_valid_number(z)  # Checks if this number is a valid phone number based on E.164 standards
        # return is_possible_number(z) # checks if it has right number of digits.
    except NumberParseException:
        return False


def format_phone_number(phone_num):
    """
    Returns a phone number formatted in E.164 format.
    :param phone_num: Given any kind of input format: +1 (213) 785-4934 or +1-213-785-4934 or any other.
    :return: Phone number in format: +12137854934
    """
    try:

        z = parse(phone_num, "IN")
        e164_number = format_number(z, PhoneNumberFormat.E164)
        return e164_number[1:]
    except NumberParseException:
        return phone_num


def simple_datetime_formatter(datetime_format):
    if datetime_format:
        fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
        return datetime_format.strftime(fmt)
    else:
        return None


def send_fcm_message(message, send_to):
    push_service = FCMNotification(api_key=FCM_API_KEY)
    if len(send_to) > 0:
        push_service.notify_multiple_devices(
            registration_ids=send_to, data_message=message
        )
