# -*- coding: utf-8 -*-
import datetime
import random
from sqlalchemy import desc
from sqlalchemy.sql import and_, or_
from flask import jsonify, request, Blueprint, g
from odyssey import app, db, auth
from odyssey.user.models import User
from odyssey.groups.models import Group
from odyssey.tasks.models import Task
from odyssey.events.models import Event, UserEvent, PhoneEvent
from odyssey.utils import simple_datetime_formatter


@app.route('/v3/activity', methods=['GET'])
@auth.login_required
def get_activity():
    user = g.user
    user_event_objs = UserEvent.query.filter_by(
        username=user.username
    ).order_by(desc(UserEvent.event_ocurred_time))
    events = []
    for user_event in user_event_objs:
        events.append(Event.query.get(user_event.event_id))
        if user_event.read_by_user is False:
            user_event.read()

    details = [get_event_details(event, event.action, user) for event in events]
    db.session.commit()
    return jsonify(
        status=200,
        messgae='Ok',
        activity=details[:50]
    )


@app.route('/v3/activity/unread', methods=['GET'])
@auth.login_required
def get_unread_activity():
    user = g.user
    user_event_objs = UserEvent.query.filter_by(
        username=user.username,
        read_by_user=False).order_by(desc(UserEvent.event_ocurred_time))
    events = []
    for user_event in user_event_objs:
        events.append(Event.query.get(user_event.event_id))
        user_event.read()
    details = [get_event_details(event, event.action) for event in events]
    db.session.commit()
    return jsonify(
        status=200,
        messgae='Ok',
        activity=details
    )


def get_event_details(event, action, user):
    if action == 'task_assigned' or action == 'ASSIGNED':
        return admin_notified_for_task_assigned(event, user)
    elif action == 'task_created' or action == 'CREATED':
        return admin_notified_for_task_created(event, user)
    elif action == 'task_added' or action == 'ADDED':
        return group_notified_for_task_add(event, user)
    elif action == 'task_deleted':
        return agent_notified_for_task_deletion(event)
    elif action == 'agent_group_request':
        return agent_notified_for_group_request(event)
    elif action == 'join_group':
        return agent_notified_for_agent_added_in_a_group(event)
    elif action == 'task_edit' or action == 'EDITED':
        return group_notified_for_task_edit(event, user)
    elif action == 'task_started' or action == 'STARTED':
        return group_notified_for_task_started(event, user)
    elif action == 'task_completed' or action == 'COMPLETED':
        return group_notified_for_task_complete(event, user)
    elif action == 'task_rejected' or action == 'REJECTED':
        return group_notified_for_task_rejection(event, user)
    elif action == 'task_cancelled' or action == 'CANCELLED':
        return group_notified_for_task_cancel(event, user)
    elif action == 'task_cancelled' or action == 'DELETED':
        return group_notified_for_task_delete(event, user)
    elif action == 'agent_added':
        return group_notified_on_agent_added_to_group(event, user)
    elif action == 'admin_added':
        return group_notified_for_member_added(event, user)
    elif action == 'GROUP_CREATED':
        return group_created_notification(event, user)
    elif action == 'AGENT_ADDED':
        return agent_joins_notification(event, user)
    elif action == 'ADMIN_ADDED':
        return admin_joins_notification(event, user)
    elif action == 'ADMIN_REMOVED':
        return admin_remove_notification(event, user)
    elif action == 'AGENT_REMOVED':
        return agent_remove_notification(event, user)
    elif action == 'GROUP_DELETED':
        return group_deleted_notification(event)
    elif action == 'GROUP_UPDATED':
        return group_updated_notification(event, user)
    elif action == 'REQUEST_ACCEPTED':
        return group_request_accepted(event, user)
    else:
        return {
            'message': 'Not Found'
        }


@app.route('/v3/event/<event_id>', methods=['GET'])
@auth.login_required
def get_event(event_id):
    user = g.user
    user_event = UserEvent.query.filter_by(
        username=user.username,
        event_id=event_id
    ).first()
    if not user_event:
        return jsonify(
            status=404,
            message='Not Found'
        )
    event = Event.query.get(event_id)
    details = get_event_details(event, event.action)
    return jsonify(
        status=200,
        message='Ok',
        details=details
    )


def admin_notified_for_task_assigned(event, user):
    name_dict = get_names_for_event(event)
    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{3} been {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


# No use
def admin_notified_for_task_created(event, user):
    name_dict = get_names_for_event(event)
    noun = "{0} has".format(name_dict['creator_name'])
    if event.created_by == user.username:
        noun = "You have"

    text = "{2} created a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        noun
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict,
    }


def admin_notified_for_task_edit(event):
    name_dict = get_names_for_event(event)
    text = "{0} has been {2} by {3}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        name_dict['admin_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.admin_id,
        'info': name_dict
    }


def agent_notified_for_task_deletion(event):
    name_dict = get_names_for_event(event)
    text = "{0} has been {2} by {3}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        name_dict['creator_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict
    }


def agent_notified_for_group_request(event):
    name_dict = get_names_for_event(event)
    text = "{0} have been invited to {1} as agent.".format(
        name_dict['agent_name'],
        name_dict['action_name'],
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def agent_notified_for_agent_added_in_a_group(event):
    name_dict = get_names_for_event(event)
    text = "{0} has been invited to {1} as agent.".format(
        name_dict['agent_name'],
        name_dict['action_name'],
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.admin_id, # admin avatar will be needed
        'info': name_dict
    }


def group_notified_for_task_add(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['creator_name'])
    if event.created_by == user.username:
        noun = "You have"

    text = "{3} {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        name_dict['creator_name']
    )

    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict
    }


def group_notified_for_task_edit(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['admin_name'])
    if event.admin_id == user.username:
        noun = "You have"

    text = "{3} {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )

    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.admin_id,
        'info': name_dict
    }


def group_notified_for_member_added(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} is".format(name_dict['admin_name'])
    if event.admin_id == user.username:
        noun = "You are"

    text = "{0} now an admin.".format(
        noun,
        name_dict['group_name'],
        name_dict['action_name'],
        name_dict['creator_name']
    )

    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.admin_id,
        'info': name_dict
    }


def group_notified_for_task_started(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{3} {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )

    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def group_notified_for_task_complete(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{3} {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )

    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def group_notified_for_task_cancel(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{3} {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )

    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def group_notified_for_task_delete(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['creator_name'])
    if event.created_by == user.username:
        noun = "You have"

    text = "{3} {2} a task: {0}.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict
    }


def group_notified_for_task_rejection(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{3} rejected task: {0} request.".format(
        name_dict['task_name'],
        name_dict['group_name'],
        name_dict['action_name'],
        noun
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def group_notified_on_agent_added_to_group(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{0} been added by {2}.".format(
        noun,
        name_dict['action_name'],
        name_dict['admin_name'],
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def group_created_notification(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['creator_name'])
    if event.created_by == user.username:
        noun = "You have"

    text = "{0} created a group.".format(
        noun,
        name_dict['action_name'],
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict
    }


def admin_joins_notification(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} is".format(name_dict['admin_name'])
    if event.admin_id == user.username:
        noun = "You are"

    text = "{0} now an admin.".format(
        noun,
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.admin_id,
        'info': name_dict
    }


def agent_joins_notification(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{0} been invited as agent.".format(
        noun,
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def agent_remove_notification(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{0} left group {1} as agent.".format(
        noun,
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def admin_remove_notification(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['admin_name'])
    if event.admin_id == user.username:
        noun = "You have"

    text = "{0} left group {1} as admin.".format(
        noun,
        name_dict['group_name']
    )
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def group_deleted_notification(event):
    name_dict = get_names_for_event(event)
    text = "Group deleted.".format(name_dict['group_name'])
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict
    }


def group_updated_notification(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['creator_name'])
    if event.created_by == user.username:
        noun = "You have"

    text = "{0} edited the group.".format(noun)
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.created_by,
        'info': name_dict
    }


def group_request_accepted(event, user):
    name_dict = get_names_for_event(event)

    noun = "{0} has".format(name_dict['agent_name'])
    if event.agent_id == user.username:
        noun = "You have"

    text = "{0} accepted group request.".format(
        noun,
        name_dict['group_name'])
    return {
        'text': text,
        'time': simple_datetime_formatter(event.event_ocurred_time),
        'group_id': event.group_id,
        'user_name': event.agent_id,
        'info': name_dict
    }


def get_names_for_event(event):
    name_dict = {}
    task_name = 'task_name'
    group_name = 'group_name'
    agent_name = 'agent_name'
    admin_name = 'admin_name'
    creator_name = 'creator_name'
    action_name = 'action_name'
    task_id = 'task_id'
    if event:
        if event.task_id:
            name_dict[task_name] = get_task_name(event.task_id)
            name_dict[task_id] = event.task_id
        else:
            name_dict[task_name] = "Untitled"
        if event.group_id:
            name_dict[group_name] = get_group_name(event.group_id)
        else:
            name_dict[group_name] = "Untitled"
        if event.agent_id:
            name_dict[agent_name] = get_user_name(event.agent_id)
        else:
            name_dict[agent_name] = "Unknown"
        if event.admin_id:
            name_dict[admin_name] = get_user_name(event.admin_id)
        else:
            name_dict[admin_name] = "Unknown"
        if event.created_by:
            name_dict[creator_name] = get_user_name(event.created_by)
        else:
            name_dict[creator_name] = "Unknown"
        if event.action:
            name_dict[action_name] = get_action_name(event.action)
        else:
            name_dict[action_name] = "Unknown"

    return name_dict


def get_task_name(task_id):
    task = Task.query.get(task_id)
    if task:
        if task.title == '':
            return 'Untitled'
        return task.title.encode('utf-8')
    else:
        return "Untitled"


def get_group_name(group_id):
    group = Group.query.get(group_id)
    if group:
        if group.name == '':
            return 'Untitled'
        return group.name.encode('utf-8')
    else:
        return "Untitled"


def get_user_name(username):
    user = User.query.filter_by(username=username).first()
    if user:
        if user.name == '':
            return 'Unknown'
        return user.name.encode('utf-8')
    else:
        return "Unknown"


def get_action_name(action):
    if action:
        return action.lower()
    else:
        return 'Unknown'


@app.route('/v3/phone_event', methods=['POST'])
@auth.login_required
def add_phone_event():
    user = g.user
    agent_id = user.username
    gps = request.json.get('gps')
    phone_status = request.json.get('phone_status')
    shift = request.json.get('shift')
    battery = request.json.get('battery')
    internet = request.json.get('internet')
    trigger = request.json.get('trigger')
    if request.json.get('time'):
        date_string = request.json.get('time')
        time = datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%fZ")
    phone_event = PhoneEvent(
        agent_id=agent_id,
        gps=gps,
        phone_status=phone_status,
        shift=shift,
        battery=battery,
        time=time,
        internet=internet,
        trigger=trigger
    )

    db.session.add(phone_event)
    db.session.commit()

    return jsonify(
        status=200,
        message='Ok'
    )


@app.route('/v3/phone_event/<agent_id>/latest', methods=['GET'])
@auth.login_required
def get_latest_phone_event(agent_id):
    user = g.user
    phoneevent = PhoneEvent.query.filter_by(agent_id=agent_id).order_by(desc(PhoneEvent.time)).first()

    if not phoneevent:
        return jsonify(
            status=404,
            message='Not found'
        )

    return jsonify(
        status=200,
        message='Ok',
        phone_event=phoneevent.serialize
    )


@app.route('/v3/phone_event/<agent_id>/all', methods=['GET'])
@auth.login_required
def get_all_phone_event(agent_id):
    user = g.user
    phoneevent = PhoneEvent.query.filter_by(agent_id=agent_id).order_by(desc(PhoneEvent.time))

    if not phoneevent:
        return jsonify(
            status=404,
            message='Not found'
        )

    return jsonify(
        status=200,
        message='Ok',
        phone_event=[event.serialize for event in phoneevent]
    )




