# -*- coding: utf-8 -*-
from odyssey import db
import datetime
from odyssey.utils import simple_datetime_formatter


class Event(db.Model):
    # Event Id
    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    group_id = db.Column(db.Integer, db.ForeignKey('group.id', ondelete='CASCADE'))
    agent_id = db.Column(db.String, db.ForeignKey('user.username'))
    admin_id = db.Column(db.String, db.ForeignKey('user.username'))
    created_by = db.Column(db.String, db.ForeignKey('user.username'))
    action = db.Column(db.String)
    event_ocurred_time = db.Column(db.DateTime)

    def __init__(self, task_id, group_id, agent_id, admin_id, created_by, action):
        self.task_id = task_id
        self.group_id = group_id
        self.agent_id = agent_id
        self.admin_id = admin_id
        self.created_by = created_by
        self.action = action
        self.event_ocurred_time = datetime.datetime.now()

    def __repr__(self):
        return """Event(
            task_id: {0},\tgroup_id: {1},
            \taction: {2},\tocurred_at: {3}
            \tagent: {4})""".format(
            self.task_id,
            self.group_id,
            self.action,
            self.event_ocurred_time,
            self.agent_id
        )


class UserEvent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(
        db.String, db.ForeignKey('user.username', ondelete='CASCADE')
    )
    event_id = db.Column(
        db.Integer, db.ForeignKey('event.id', ondelete='CASCADE')
    )
    event_ocurred_time = db.Column(db.DateTime)
    last_seen_by_user = db.Column(db.DateTime)
    read_by_user = db.Column(db.Boolean, default=False)

    def __init__(self, username, event_id, event_time):
        self.username = username
        self.event_id = event_id
        self.event_ocurred_time = event_time

    def __repr__(self):
        return 'UserEvent({0}, {1}, {2})'.format(
            self.username, self.event_id, self.event_ocurred_time
        )

    def get_event_id(self):
        return self.event_id

    def get_username(self):
        return self.username

    def is_read(self):
        return self.read_by_user

    def read(self):
        self.last_seen_by_user = datetime.datetime.utcnow()
        self.read_by_user = True


class PhoneEvent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    agent_id = db.Column(db.String, db.ForeignKey('user.username'))
    gps = db.Column(db.String)
    phone_status = db.Column(db.String)
    shift = db.Column(db.String)
    battery = db.Column(db.String)
    internet = db.Column(db.String)
    time = db.Column(db.DateTime)
    trigger = db.Column(db.String)

    def __repr__(self):
        return "<agentPhone:{0}|battery:{1}>".format(self.agent_id, self.battery)

    @property
    def serialize(self):
        return {
            "agent_id": self.agent_id,
            "gps": self.gps,
            "phone": self.phone_status,
            "shift": self.shift,
            "battery": self.battery,
            "time": simple_datetime_formatter(self.time),
            "internet": self.internet,
            "trigger": self.trigger,
        }

    def __init__(self, agent_id, gps, phone_status, shift, battery, time, internet, trigger):
        self.agent_id = agent_id
        self.gps = gps
        self.phone_status = phone_status
        self.shift = shift
        self.battery = battery
        self.internet = internet
        self.time = time
        self.trigger = trigger

