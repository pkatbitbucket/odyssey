import jinja2
import flask

blueprint = flask.Blueprint('filters', __name__)

# using the method
@jinja2.contextfilter
@blueprint.app_template_filter()
def get_value_by_key(var, dictionary, key):
    value = dictionary[str(key)]
    value = ', '.join(value)
    return value