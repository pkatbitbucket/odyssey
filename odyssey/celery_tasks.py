# -*- coding: utf-8 -*-
from celery import Celery
from odyssey import db, app
from odyssey.locations.models import UserLocation
from odyssey.user.models import User


celery = Celery(__name__, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


@celery.task
def add_location(locations):
    db.engine.execute(UserLocation.__table__.insert(), locations)
