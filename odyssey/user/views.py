#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from flask import jsonify, request, g, abort
from odyssey import app, db
from odyssey import auth
from pyfcm import FCMNotification
from config import FCM_API_KEY
from odyssey.user.models import User,VerificationCode, Agent, Admin
from odyssey.utils import generate_random_code, send_to_phone, write_to_s3, call_to_phone
AMAZON_S3_AVATAR_URL = app.config['AMAZON_S3_AVATAR_URL']


@app.route('/v3/login/<country>/<phone>')
def send_verification_code(country, phone):
    find_or_update_verification_code(country+phone)
    return jsonify(status=200, message="OK")


@app.route('/v3/login/<country>/<phone>/call')
def verification_over_call(country, phone):
    phone_number = str(country) + str(phone)
    call_to_phone(phone_number)
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v3/login/<app_type>/<country>/<phone_number>/<user_code>')
def login(app_type, country, phone_number, user_code):
    """
    Validates two factor authentication code sent to user.
    :param app_type: Agent or Admin. App being verified.
    :param country: Country code like 91 for India, 1 for US
    :param phone_number:
    :param user_code: code sent by user
    :return: User details on success and 400 on failure
    """
    phone = country + phone_number
    server_code = VerificationCode.query.filter_by(phone=phone).first()
    # If user submitted code is same as the code sent via sms.
    if server_code and user_code == server_code.code:
        user = find_or_create_user(phone, app_type, enabled=True)
        status = 200
        message = "OK"
        if app_type == 'admin':
            find_or_create_admin(user_id=user.id)
        if app_type == 'agent':
            #send_fcm_to_previous_device(country + phone_number)
            agent = find_or_create_agent(user_id=user.id)

        driving_license = ''
        if app_type == 'agent' and agent is not None:
            driving_license = agent.driving_license

        return jsonify(
            status=status,
            message=message,
            user = {
                "user_id": user.id,
                "name": user.name,
                "email": user.email,
                "organization": user.organization,
                "description": user.description,
                "avatar": user.avatar,
                "driving_license": driving_license
            },
            external_storage_url=AMAZON_S3_AVATAR_URL
        )
    return jsonify(status=400, message='Invalid OTP')


@app.route('/v3/token')
@auth.login_required
def get_auth_token():
    """
    generates a token
    :return: returns auth token
    """
    token = g.user.generate_auth_token()
    return jsonify(token=token.decode('ascii'))


@app.route('/v3/admin', methods=['POST'])
@auth.login_required
def update_admin_details():
    """
    Update admin details
    :return:
    """
    name = request.json.get('name')
    company = request.json.get('organization')
    description = request.json.get('description')
    email = request.json.get('email')
    avatar = request.json.get('avatar')

    user = get_user_from_username(g.user.username)
    user.name = name
    user.description = description
    user.organization = company
    user.email = email
    user.avatar = avatar

    db.session.commit()
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v3/agent', methods=['POST'])
@auth.login_required
def update_agent_details():
    """
    Update admin details
    :return:
    """
    driving_license = request.json.get('driving_license')
    name = request.json.get('name')
    avatar = request.json.get('avatar')

    user = g.user

    user.name = name
    user.avatar = avatar
    agent = Agent.query.filter_by(user_id=user.id).first()
    if agent:
        agent.update_driving_license(driving_license=driving_license)
        db.session.commit()
    else:
        return jsonify(
                status=404,
                message="Agent not found"
            )
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v3/agent/fcm', methods=['POST'])
@auth.login_required
def update_agent_gcm():
    """
    Update agent fcm
    :return:
    """
    registration_id = request.json.get('id', '')
    agent = find_or_create_agent(user_id=g.user.id)
    if agent:
        agent.update_gcm(gcm_registration_id=registration_id)
    db.session.commit()
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v3/admin/fcm', methods=['POST'])
@auth.login_required
def update_admin_gcm():
    """
    Update admin fcm
    :return:
    """
    registration_id = request.json.get('id', '')
    if registration_id:
        admin = find_or_create_admin(user_id=g.user.id)
        if admin:
            admin.update_gcm(gcm_registration_id=registration_id)
        db.session.commit()

    return jsonify(
        status=200,
        message="OK"
    )


def find_or_create_user(username, app_type, enabled=False):
    """
    Create user if it doesn't exist else return corresponding user
    :param phone:
    :param app_type: Agent or Admin. Create user for that odyssey
    :param enabled: If user is logging in, then enabled is True. If added by someone in group, then false.
    :return: User object
    """
    if username is None:
        abort(400)
    user = User.query.filter_by(username=username).first()
    if user is not None:
        user.set_user_type(app_type)
        return user

    user = User(username=username)
    user.hash_password(username)
    user.set_user_type(app_type)
    user.enable_user(enabled)
    db.session.add(user)
    db.session.commit()
    return user


@auth.verify_password
def verify_password(username_or_token, password):
    # Try to see if it's a token first
    user_id = User.verify_auth_token(username_or_token)
    if user_id:
        user = User.query.filter_by(id = user_id).one()
    else:
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


def find_or_update_verification_code(phone):
    """
    Updates the verification code.
    :param phone:
    :return: None
    """
    code = str(generate_random_code())
    verification_code = VerificationCode.query.filter_by(phone=phone).first()
    if verification_code:
        verification_code.code = code
    else:
        verificationCode = VerificationCode(phone, code)
        db.session.add(verificationCode)
    db.session.commit()
    try:
        write_to_s3(phone, code)
        send_to_phone(phone, code)
    except:
        pass


def get_user_id_from_username(username):
    user = User.query.filter_by(username=username).first()
    return user.id


def get_user_from_username(username):
    return User.query.filter_by(username=username).first()


def get_agent_from_username(username):
    user = get_user_from_username(username=username)
    return Agent.query.filter_by(user_id=user.id).first()


def get_admin_from_username(username):
    user = get_user_from_username(username=username)
    return Admin.query.filter_by(user_id=user.id).first()


# TODO: to be modified to get user object
def get_username_from_user_id(user_id):
    user = User.query.filter_by(id=user_id).first()
    return user.username


def find_or_create_admin(user_id):
    admin = Admin.query.filter_by(user_id=user_id).first()
    if admin is None:
        admin = Admin(user_id=user_id)
        db.session.add(admin)
        db.session.commit()
    return admin


def find_or_create_agent(user_id):
    agent = Agent.query.filter_by(user_id=user_id).first()
    if agent is None:
        agent = Agent(driving_license='', user_id=user_id)
        db.session.add(agent)
        db.session.commit()
    return agent


def agent_override_message():
    override_message = {
        "notification_version": "1",
        "notification_type": "agent_reset",
        "notification_action": "reset_app",
        "notification_title": "You have logged in from other device using this number",
        "notification_text": "Please relogin in order to continue from this device",
        "notification_time": str(datetime.datetime.now())
    }
    return override_message


def send_fcm_to_previous_device(username):
    user = User.query.filter_by(username=username).first()
    try:
        agent = Agent.query.filter_by(user_id=user.id).first()
        override_message = agent_override_message()
        send_fcm_message_to_single_device(
            override_message, agent.gcm_registration_id
        )
    except:
        pass


def send_fcm_message_to_single_device(message, send_to):
    push_service = FCMNotification(api_key=FCM_API_KEY)
    if send_to:
        push_service.notify_single_device(
            registration_id=send_to, data_message=message
        )

############ v2 apis starts here ##################

@app.route('/v2/login/<country>/<phone>')
def v2_send_verification_code(country, phone):
    find_or_update_verification_code(country+phone)
    return jsonify(status=200, message="OK")


@app.route('/v2/login/<country>/<phone>/call')
def v2_verification_over_call(country, phone):
    phone_number = str(country) + str(phone)
    call_to_phone(phone_number)
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v2/login/<app_type>/<country>/<phone_number>/<user_code>')
def v2_login(app_type, country, phone_number, user_code):
    """
    Validates two factor authentication code sent to user.
    :param app_type: Agent or Admin. App being verified.
    :param country: Country code like 91 for India, 1 for US
    :param phone_number:
    :param user_code: code sent by user
    :return: User details on success and 400 on failure
    """
    phone = country + phone_number
    server_code = VerificationCode.query.filter_by(phone=phone).first()
    # If user submitted code is same as the code sent via sms.
    if server_code and user_code == server_code.code:
        user = find_or_create_user(phone, app_type, enabled=True)
        status = 200
        message = "OK"
        if app_type == 'admin':
            find_or_create_admin(user_id=user.id)
        if app_type == 'agent':
            #send_fcm_to_previous_device(country + phone_number)
            agent = find_or_create_agent(user_id=user.id)

        driving_license = ''
        if app_type == 'agent' and agent is not None:
            driving_license = agent.driving_license

        return jsonify(
            status=status,
            message=message,
            user = {
                "user_id": user.id,
                "name": user.name,
                "email": user.email,
                "organization": user.organization,
                "description": user.description,
                "avatar": user.avatar,
                "driving_license": driving_license
            },
            external_storage_url=AMAZON_S3_AVATAR_URL
        )
    return jsonify(status=400, message='Invalid OTP')


@app.route('/v2/token')
@auth.login_required
def v2_get_auth_token():
    """
    generates a token
    :return: returns auth token
    """
    token = g.user.generate_auth_token()
    return jsonify(token=token.decode('ascii'))


@app.route('/v2/admin', methods=['POST'])
@auth.login_required
def v2_update_admin_details():
    """
    Update admin details
    :return:
    """
    name = request.json.get('name')
    company = request.json.get('organization')
    description = request.json.get('description')
    email = request.json.get('email')
    avatar = request.json.get('avatar')

    user = get_user_from_username(g.user.username)
    user.name = name
    user.description = description
    user.organization = company
    user.email = email
    user.avatar = avatar

    db.session.commit()
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v2/agent', methods=['POST'])
@auth.login_required
def v2_update_agent_details():
    """
    Update admin details
    :return:
    """
    driving_license = request.json.get('driving_license')
    name = request.json.get('name')
    avatar = request.json.get('avatar')

    user = g.user

    user.name = name
    user.avatar = avatar
    agent = Agent.query.filter_by(user_id=user.id).first()
    if agent:
        agent.update_driving_license(driving_license=driving_license)
        db.session.commit()
    else:
        return jsonify(
                status=404,
                message="Agent not found"
            )
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v2/agent/fcm', methods=['POST'])
@auth.login_required
def v2_update_agent_gcm():
    """
    Update agent fcm
    :return:
    """
    registration_id = request.json.get('id', '')
    agent = find_or_create_agent(user_id=g.user.id)
    if agent:
        agent.update_gcm(gcm_registration_id=registration_id)
    db.session.commit()
    return jsonify(
        status=200,
        message="OK"
    )


@app.route('/v2/admin/fcm', methods=['POST'])
@auth.login_required
def v2_update_admin_gcm():
    """
    Update admin fcm
    :return:
    """
    registration_id = request.json.get('id', '')
    if registration_id:
        admin = find_or_create_admin(user_id=g.user.id)
        if admin:
            admin.update_gcm(gcm_registration_id=registration_id)
        db.session.commit()

    return jsonify(
        status=200,
        message="OK"
    )
