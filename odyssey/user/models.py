# -*- coding: utf-8 -*-
import random
import string
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from odyssey import db, app

secret_key = ''.join(random.choice(string.ascii_uppercase + string.digits) \
    for x in range(32))

# Many to many relationship
shared_with = db.Table(
    'shared_with',
    db.Column(
        'owner', db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    ),
    db.Column(
        'sharer', db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # username is same as phone number
    username = db.Column(db.String, nullable=False, unique=True, index=True)
    avatar = db.Column(db.String)
    password_hash = db.Column(db.String)

    # determine the odyssey from which user is logged in
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    is_agent = db.Column(db.Boolean, nullable=False, default=False)

    name = db.Column(db.String, default='')
    organization = db.Column(db.String, default='')
    description = db.Column(db.String, default='')
    email = db.Column(db.String, default='')
    enabled = db.Column(db.Boolean(), default=False)
    group_maps = db.relationship('GroupUserMapping', backref='user',)

    creator_info = db.relationship(
        'Task',
        backref='user_creator',
        lazy='dynamic',
        foreign_keys='Task.creator'
    )
    assignee_info = db.relationship(
        'Task',
        backref='user_assignee',
        lazy='dynamic',
        foreign_keys='Task.admin'
    )
    assigned_to_info = db.relationship(
        'Task',
        backref='user_assigned_to',
        lazy='dynamic',
        foreign_keys='Task.agent'
    )

    shared_info = db.relationship(
        'User',
        secondary='shared_with',
        primaryjoin=shared_with.c.owner == id,
        secondaryjoin=shared_with.c.sharer == id,
        backref='user_creator',
        lazy='dynamic',
        foreign_keys=[shared_with.c.owner, shared_with.c.sharer]
    )

    # Check if user has logged in or just received the request.
    # If enabled=False, the user has received a group request but haven't signed up.

    def __repr__(self):
        return self.username

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(secret_key, expires_in=expiration)
        return s.dumps({'id': self.id})

    def set_user_type(self, app_type):
        """
        Determines the odyssey from which user is logged in
        :param app_type: Agent or Admin
        :return: None
        """
        if app_type == 'admin':
            self.is_admin = True
        elif app_type == 'agent':
            self.is_agent = True
        else:
            app.logger.info("Invalid odyssey type")

    def enable_user(self, status=False):
        self.enabled = status

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(secret_key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            # Valid Token, but expired
            return None
        except BadSignature:
            # Invalid Token
            return None
        user_id = data['id']
        return user_id


# The Agent DataModel.
class Agent(db.Model):
    """
    Stores Agent's driving license and gcm registration ID.
    """
    agent_id = db.Column(db.Integer, primary_key=True)              # Required for primary key. Not used anywhere.
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    # Agent's driving license information
    driving_license = db.Column(db.String, nullable=False, default='')
    gcm_registration_id = db.Column(db.String, nullable=False, default='')
    username = db.relationship('User')
    shift_start_time = db.Column(db.Time, nullable=True)
    shift_end_time = db.Column(db.Time, nullable=True)

    def __init__(self, driving_license, user_id):
        self.driving_license = driving_license
        self.user_id = user_id

    def __repr__(self):
        return str(self.agent_id)

    def update_driving_license(self, driving_license):
        self.driving_license = driving_license
        db.session.commit()

    def update_gcm(self, gcm_registration_id):
        self.gcm_registration_id = gcm_registration_id
        db.session.commit()

    def get_gcm(self):
        return self.gcm_registration_id

    def update_shift(self, shift_start_time, shift_end_time):
        self.shift_start_time = shift_start_time
        self.shift_end_time = shift_end_time
        db.session.commit()

    @property
    def serialize(self):
        return {
            "driving_license": self.driving_license,
            "user_id": self.user_id,
            "shift_start_time": self.shift_start_time,
            "shift_end_time": self.shift_end_time
        }


# The Admin DataModel.
class Admin(db.Model):
    """
    Stores Admin's gcm registration ID.
    """
    admin_id = db.Column(db.Integer, primary_key=True)              # Required for primary key. Not used anywhere.

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    gcm_registration_id = db.Column(db.String, nullable=False, default='')

    def __init__(self, user_id):
        self.user_id = user_id

    def update_gcm(self, gcm_registration_id):
        self.gcm_registration_id = gcm_registration_id

    def get_gcm(self):
        return self.gcm_registration_id

    def __repr__(self):
        return str(self.user_id)


class VerificationCode(db.Model):
    """ Verification code generated during login/signup """
    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String)
    code = db.Column(db.String, nullable=False, default=100000)

    def __init__(self, phone, code):
        self.phone = phone
        self.code = code

    def __repr__(self):
        return self.code
