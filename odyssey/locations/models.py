# -*- coding: utf-8 -*-
from geoalchemy2.types import Geometry
from geoalchemy2.elements import WKTElement
from odyssey import db
import pytz


# TODO: Refacor UserLocations for distance calculation may be by adding one More class
class UserLocation(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    accuracy = db.Column(db.Float, nullable=False)      # GPS accuracy received from phone
    timestamp = db.Column(db.DateTime, nullable=False)
    location = db.Column(Geometry(geometry_type='POINT', srid=4326))
    bearing = db.Column(db.Float, nullable=True)
    battery_status = db.Column(db.Float, nullable=True)
    altitude = db.Column(db.Float, nullable=True)
    speed = db.Column(db.Float, nullable=True)
    # chances in % for "device is in vehicle such as car"
    in_vehicle = db.Column(db.Float, nullable=True)
    # chances in % for "device is on bicycle" 
    on_bicycle = db.Column(db.Float, nullable=True)
    # chances in % for "the device is on a user who is walking or running"
    on_foot = db.Column(db.Float, nullable=True)
    # chances in % for "the device is on a user who is running"
    running = db.Column(db.Float, nullable=True)
    # chances in % for "the device is still"
    still = db.Column(db.Float, nullable=True)
    # changes in % for "the device angle relative to gravity changes significantly"
    tilting = db.Column(db.Float, nullable=True)
    # changes in % for "unable to detect the current activity"
    unknown = db.Column(db.Float, nullable=True)
    # chances in % for "the device is on a user who is walking"
    walking = db.Column(db.Float, nullable=True)
    username = db.relationship('User', backref='user_locations')
    
    def __init__(
        self, user_id, latitude, longitude, accuracy, timestamp, bearing,
        battery_status, altitude, speed, in_vehicle, on_bicycle, on_foot,
        running, still, tilting, unknown, walking
            ):
        self.user_id = user_id
        self.latitude = latitude
        self.longitude = longitude
        self.accuracy = accuracy
        self.timestamp = timestamp
        self.add_location_from_coordinates(longitude=longitude, latitude=latitude)
        self.bearing = bearing
        self.battery_status = battery_status
        self.altitude = altitude
        self.speed = speed
        self.in_vehicle = in_vehicle
        self.on_bicycle = on_bicycle
        self.on_foot = on_foot
        self.running = running
        self.still = still
        self.tilting = tilting
        self.unknown = unknown
        self.walking = walking

    def add_location_from_coordinates(self, longitude, latitude):
        """Converts location to Point in WKT format."""
        self.location = WKTElement('POINT({0} {1})'.format(longitude, latitude), srid=4326)

    @property
    def serialize(self):
        if '.' in self.timestamp.isoformat():
            time_stamp = self.timestamp.isoformat() + 'Z'
        else:
            time_stamp = self.timestamp.isoformat() + '.0' + 'Z'
        return {
            "id": self.id,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "accuracy": self.accuracy,
            "timestamp": time_stamp,
            "bearing": self.bearing,
            "battery_status": self.battery_status
        }
