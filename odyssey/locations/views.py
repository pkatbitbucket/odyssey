#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import json
from geopy.geocoders import Nominatim
from haversine import haversine
from odyssey import app, sockets, auth
from odyssey.groups.models import GroupUserMapping, Group
from odyssey.locations.models import UserLocation
from odyssey.user.models import User
from flask import jsonify
from odyssey import redis_store
from sqlalchemy.sql import and_
from geoalchemy2.elements import WKTElement
from odyssey import celery_tasks

LOCATION_FLUSH_TIME = app.config['LOCATION_FLUSH_TIME']
GPS_ACCURACY = app.config['GPS_ACCURACY']

# Maintains a list of all active agent socket connections
websocket_queue = []


@sockets.route('/v3/location')
def add_locations(ws):
    user_locations = []
    """
    Accepts websocket connection for agent location.
    :param ws:
    :return: None
    """
    # on connect
    websocket_queue.append(ws)
    # while connected
    while True:
        location_data = ws.receive()
        
        if location_data is None:
            # socket has closed/errored
            break

        raw_user_location = json.loads(location_data)
        latitude = raw_user_location.get('latitude')
        longitude = raw_user_location.get('longitude')
        accuracy = raw_user_location.get('accuracy')
        timestamp = raw_user_location.get('timestamp')
        username = raw_user_location.get('phone')
        bearing = raw_user_location.get('bearing')
        battery_status = raw_user_location.get('battery_status')
        speed = raw_user_location.get('speed')
        altitude = raw_user_location.get('altitude')
        in_vehicle = raw_user_location.get('in_vehicle')
        on_bicycle = raw_user_location.get('on_bicycle')
        on_foot = raw_user_location.get('on_foot')
        running = raw_user_location.get('running')
        still = raw_user_location.get('still')
        tilting = raw_user_location.get('tilting')
        unknown = raw_user_location.get('unknown')
        walking = raw_user_location.get('walking')

        user = User.query.filter_by(username=username).first()

        location = WKTElement(
            'POINT({0} {1})'.format(longitude, latitude), srid=4326
        )
        user_location = {
            'user_id': user.id,
            'latitude': latitude,
            'longitude': longitude,
            'accuracy': accuracy,
            'timestamp': timestamp,
            'bearing': bearing,
            'battery_status': battery_status,
        }
        # add to cache
        date = datetime.datetime.strptime(
            timestamp, '%Y-%m-%dT%H:%M:%S.%fZ').date()
        redis_store.rpushx(
            user.username + '_' + str(date), user_location
        )
        user_location['location'] = location
        user_location['speed'] = speed
        user_location['altitude'] = altitude
        user_location['in_vehicle'] = in_vehicle
        user_location['on_bicycle'] = on_bicycle
        user_location['on_foot'] = on_foot
        user_location['running'] = running
        user_location['still'] = still
        user_location['tilting'] = tilting
        user_location['unknown'] = unknown
        user_location['walking'] = walking

        user_locations.append(user_location)
        # save the location in bulk
        if len(user_locations) > app.config['LOCATION_LENGTH_MAX']:
            celery_tasks.add_location.apply_async(args=(user_locations,))
            user_locations = []
    # save the locations socket closes
    if len(user_locations) > 0:
        celery_tasks.add_location.apply_async(args=(user_locations,))
    # disconnected
    websocket_queue.remove(ws)
    ws.close()


@sockets.route('/v3/groups/location')
def get_group_user_locations(ws):
    """
    Get location of all the group members. Send the data in JSON format on a websocket connection.
    :param ws:
    :return: None
    """
    # on connect
    websocket_queue.append(ws)

    # while connected
    while True:
        users_current_location = []
        raw_group_data = ws.receive()
        if raw_group_data is None:
            # socket has closed/errored
            break

        group_data = json.loads(raw_group_data)
        group_id = group_data['group_id']

        group = Group.query.get(group_id)
        if not group:
            output = json.dumps({"error": "Group Not available"})
            ws.send(output)
            websocket_queue.remove(ws)
            ws.close()
        # TODO: Get latitude, longitude for this location.
        for group_member in group.users:
            group_user_mapping = GroupUserMapping.query.filter(
                and_(GroupUserMapping.group_id == group_id),
                (GroupUserMapping.user_id == group_member.id)
            ).first()
            if (group_user_mapping and group_user_mapping.is_agent) \
            and group_user_mapping.has_agent_joined_group:
                # get from cache
                user_current_location = get_current_location_from_cache(
                    group_member.username,
                    group_user_mapping.agent_joined_group_on
                )
                if not user_current_location:
                    # get from database
                    user_current_location = UserLocation.query.filter(
                        and_(UserLocation.user_id==group_member.id),
                        (UserLocation.timestamp >= \
                            group_user_mapping.agent_joined_group_on),
                    ).order_by(UserLocation.timestamp.desc()).first()
                    if user_current_location:
                        user_current_location = user_current_location.serialize
                if user_current_location:
                    users_current_location.append(
                        {
                            'phone': group_member.username,
                            'latitude': user_current_location['latitude'],
                            'longitude': user_current_location['longitude']
                        }
                    )
                output = json.dumps({"locations": users_current_location,
                                    "error": ""})
                ws.send(output)
    # disconnected
    websocket_queue.remove(ws)
    ws.close()


@app.route('/v3/location/<group_id>/<username>/<query_date>', methods=['GET'])
@auth.login_required
def get_user_locations(group_id, username, query_date):
    total_distance = 0
    user_locations = []
    status = 404
    user, group_usr_mapping = get_user_and_group_mapping(username, group_id)
    if (not user) or (not group_usr_mapping):
        status_message = "You have entered wrong username and or group"
        return jsonify(
            status=status,
            status_message=status_message,
            userLocations=user_locations,
            distance=total_distance
        )

    if not (group_usr_mapping.is_agent and \
            group_usr_mapping.has_agent_joined_group):
        status_message = "User has not joined as agent"
        return jsonify(
            status=status,
            status_message=status_message,
            userLocations=user_locations,
            distance=total_distance
        )
    query_date = parse_str_date_to_date_obj(query_date)
    if query_date > \
            group_usr_mapping.agent_joined_group_on.date():
        user_locations, total_distance = get_location_from_cache_or_db(
            query_date, username, user
        )
        status = 200
        status_message = "OK"
    elif query_date == \
            group_usr_mapping.agent_joined_group_on.date():
        user_locations, total_distance = get_location_distance_on_joining(
            user, query_date, group_usr_mapping.agent_joined_group_on.time()
        )
        status = 200
        status_message = "OK"
    else:
        status = 423
        status_message = "The resource that is been accessed is being locked"

    return jsonify(
        status=status,
        status_message=status_message,
        userLocations=user_locations,
        distance=total_distance
    )


@app.route('/v3/stats/<group_id>/<query_date>', methods=['GET'])
@auth.login_required
def get_group_agents_location(group_id, query_date):
    group_user_maps = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group_id),
        (GroupUserMapping.is_agent == True),
    ).all()
    users_stat = []

    starting_point = None
    ending_point = None
    distance = 0

    group_user_maps = list(group_user_maps)
    query_date = parse_str_date_to_date_obj(query_date)
    for group_user_map in group_user_maps:
        if group_user_map.has_agent_joined_group:
            user = User.query.get(group_user_map.user_id)
            if query_date > \
            group_user_map.agent_joined_group_on.date():

                locations, distance = get_location_from_cache_or_db(
                    query_date, user.username, user
                )
            elif parse_str_date_to_date_obj(query_date) == \
                    group_user_map.agent_joined_group_on.date():
                locations, distance = get_location_distance_on_joining(
                    user, query_date,
                    group_user_map.agent_joined_group_on.time()
                )
            if len(locations) > 0:
                geolocator = Nominatim()
                starting_point = geolocator.reverse(
                    '{}, {}'.format(
                        locations[0]['latitude'], locations[0]['longitude']
                    )).address
                ending_point = geolocator.reverse(
                    '{}, {}'.format(
                        locations[-1]['latitude'], locations[-1]['longitude']
                    )).address
            stat = {
                'name': user.name,
                'phone': user.username,
                'avatar': user.avatar,
                'starting_point': starting_point,
                'ending_point': ending_point,
                'total_distance': distance,
                'agent_status': group_user_map.has_agent_joined_group
            }
            users_stat.append(stat)

    return jsonify(
        status=200,
        status_message="OK",
        users_stat=users_stat,
    )


def get_current_location_from_cache(username, joined_on):
    user_current_location = redis_store.lindex(
        username + '_' + \
        str(datetime.datetime.now().date()), -1
    )
    if user_current_location:
        user_current_location = eval(user_current_location)
        try:
            latest_seen_at = datetime.datetime.strptime(
                str(user_current_location['timestamp']), '%Y-%m-%dT%H:%M:%S.%fZ'
            )
        except:
            latest_seen_at = datetime.datetime.strptime(
                str(user_current_location['timestamp']), '%Y-%m-%d %H:%M:%S'
            )
        if latest_seen_at < joined_on:
            return None
    return user_current_location


def get_locations(user_id, query_date_parsed, start_time=None):
    """
    Fetches location for given user id for specific date
    :param user_id: User whose location need to be fetched
    :param query_date_parsed: Date for which location need to be fetched
    :param start_time: we need it for the the day of group request acceptence
    :return: User location for that date along with distance user travelled that day.
    """
    # Start time for day is 00:00:00 and end time is 23:59:59
    if start_time is None:
        start_time = datetime.time.min
    end_time = datetime.time.max
    try:
        query_date_parsed = parse_str_date_to_date_obj(query_date_parsed)
        query_time_eod = datetime.datetime.combine(query_date_parsed, start_time)
        
    except:
        query_time_eod = datetime.datetime.combine(
            datetime.datetime.now().date(), start_time
        )

    next_date = datetime.datetime.combine(
        query_time_eod.date(), end_time
    )
    user_locations = UserLocation.query.filter(
            and_(UserLocation.timestamp >= query_time_eod),
            (UserLocation.timestamp < next_date),
            (UserLocation.user_id == user_id),
            (UserLocation.accuracy <= GPS_ACCURACY)
    ).order_by(UserLocation.timestamp.asc()).all()

    return user_locations


def calculate_total_distance(user_locations):
    """
    Computes total distance between a set of points
    :param user_locations: Points for which distance need to be calculated
    :return: Total distance
    """
    distance = 0
    lat1 = user_locations[0]['latitude']
    long1 = user_locations[0]['longitude']
    for location in user_locations:
        lat2 = float(location['latitude'])
        long2 = float(location['longitude'])
        point1 = (lat1, long1)
        point2 = (lat2, long2)
        distance += haversine(point1=point1, point2=point2, miles=False)
        lat1 = lat2
        long1 = long2
    return distance


def parse_str_date_to_date_obj(input_date):
    """ This function expects the date format "2016-06-17"
    """
    output_date = datetime.datetime.strptime(input_date, "%Y-%m-%d").date()
    return output_date


def get_user_and_group_mapping(username, group_id):
    user = User.query.filter_by(username=username).first()
    group_usr_mapping = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group_id),
        (GroupUserMapping.user_id == user.id)
    ).first()
    return user, group_usr_mapping


def get_location_distance_on_joining(user, query_date, start_time):
    raw_user_locations = get_locations(user.id, str(query_date), start_time)
    user_locations = [
        user_location.serialize for user_location in raw_user_locations
    ]
    if len(user_locations) > 0:
        total_distance = calculate_total_distance(user_locations)
    else:
        total_distance = 0
    return user_locations, total_distance


def get_location_from_cache_or_db(query_date, username, user):
    user_locations = []
    cached_location = redis_store.lrange(
        username + '_' + str(query_date), 0, -1
    )
    if len(cached_location) > 0:
        user_locations = [
            eval(location) for location in cached_location
        ]
    else:
        user_locations = get_location_from_db_and_cache_location(
            query_date, username, user
        )
    if len(user_locations) > 0:
        total_distance = calculate_total_distance(user_locations)
    else:
        total_distance =  0
    return user_locations, total_distance


def get_location_from_db_and_cache_location(query_date, username, user):
    user_locations = []
    raw_user_locations = get_locations(
        user.id, str(query_date)
    )
    # redis_store.set(
    #     username + '_' + str(query_date) + '_' + 'distance', \
    #     total_distance
    # )
    # redis_store.expire(
    #     username + '_' + str(query_date) + '_' + 'distance',
    #     LOCATION_FLUSH_TIME
    # )
    for userLocation in raw_user_locations:
        redis_store.rpush(
            username + '_' + str(query_date), userLocation.serialize
        )
        redis_store.expire(username + '_' + str(query_date),
                           LOCATION_FLUSH_TIME)
        user_locations.append(userLocation.serialize)
    return user_locations

########### v2 apis starts here ##############################

@sockets.route('/v2/location')
def v2_add_locations(ws):
    user_locations = []
    """
    Accepts websocket connection for agent location.
    :param ws:
    :return: None
    """
    # on connect
    websocket_queue.append(ws)
    # while connected
    while True:
        location_data = ws.receive()
        
        if location_data is None:
            # socket has closed/errored
            break

        raw_user_location = json.loads(location_data)
        latitude = raw_user_location.get('latitude')
        longitude = raw_user_location.get('longitude')
        accuracy = raw_user_location.get('accuracy')
        timestamp = raw_user_location.get('timestamp')
        username = raw_user_location.get('phone')
        bearing = raw_user_location.get('bearing')
        battery_status = raw_user_location.get('battery_status')
        speed = raw_user_location.get('speed')
        altitude = raw_user_location.get('altitude')
        in_vehicle = raw_user_location.get('in_vehicle')
        on_bicycle = raw_user_location.get('on_bicycle')
        on_foot = raw_user_location.get('on_foot')
        running = raw_user_location.get('running')
        still = raw_user_location.get('still')
        tilting = raw_user_location.get('tilting')
        unknown = raw_user_location.get('unknown')
        walking = raw_user_location.get('walking')

        timestamp = datetime.datetime.strptime(
            timestamp, '%Y-%m-%d %H:%M:%S'
        ) - datetime.timedelta(hours=5, minutes=30)
        user = User.query.filter_by(username=username).first()

        location = WKTElement(
            'POINT({0} {1})'.format(longitude, latitude), srid=4326
        )
        user_location = {
            'user_id': user.id,
            'latitude': latitude,
            'longitude': longitude,
            'accuracy': accuracy,
            'timestamp': timestamp,
            'bearing': bearing,
            'battery_status': battery_status,
        }
        # add to cache
        date = datetime.datetime.strptime(
            str(timestamp), '%Y-%m-%d %H:%M:%S').date()
        redis_store.rpushx(
            user.username + '_' + str(date), user_location
        )
        user_location['location'] = location
        user_location['speed'] = speed
        user_location['altitude'] = altitude
        user_location['in_vehicle'] = in_vehicle
        user_location['on_bicycle'] = on_bicycle
        user_location['on_foot'] = on_foot
        user_location['running'] = running
        user_location['still'] = still
        user_location['tilting'] = tilting
        user_location['unknown'] = unknown
        user_location['walking'] = walking

        user_locations.append(user_location)
        # save the location in bulk
        if len(user_locations) > app.config['LOCATION_LENGTH_MAX']:
            celery_tasks.add_location.apply_async(args=(user_locations,))
            user_locations = []
    # save the locations socket closes
    if len(user_locations) > 0:
        celery_tasks.add_location.apply_async(args=(user_locations,))
    # disconnected
    websocket_queue.remove(ws)
    ws.close()


@sockets.route('/v2/groups/location')
def v2_get_group_user_locations(ws):
    """
    Get location of all the group members. Send the data in JSON format on a websocket connection.
    :param ws:
    :return: None
    """
    # on connect
    websocket_queue.append(ws)

    # while connected
    while True:
        users_current_location = []
        raw_group_data = ws.receive()
        if raw_group_data is None:
            # socket has closed/errored
            break

        group_data = json.loads(raw_group_data)
        group_id = group_data['group_id']

        group = Group.query.get(group_id)
        if not group:
            output = json.dumps({"error": "Group Not available"})
            ws.send(output)
            websocket_queue.remove(ws)
            ws.close()
        # TODO: Get latitude, longitude for this location.
        for group_member in group.users:
            group_user_mapping = GroupUserMapping.query.filter(
                and_(GroupUserMapping.group_id == group_id),
                (GroupUserMapping.user_id == group_member.id)
            ).first()
            if (group_user_mapping and group_user_mapping.is_agent) \
            and group_user_mapping.has_agent_joined_group:
                # get from cache
                user_current_location = get_current_location_from_cache(
                    group_member.username,
                    group_user_mapping.agent_joined_group_on
                )
                if not user_current_location:
                    # get from database
                    user_current_location = UserLocation.query.filter(
                        and_(UserLocation.user_id==group_member.id),
                        (UserLocation.timestamp >= \
                            group_user_mapping.agent_joined_group_on),
                    ).order_by(UserLocation.timestamp.desc()).first()
                    if user_current_location:
                        user_current_location = user_current_location.serialize
                if user_current_location:
                    users_current_location.append(
                        {
                            'phone': group_member.username,
                            'latitude': user_current_location['latitude'],
                            'longitude': user_current_location['longitude']
                        }
                    )
                output = json.dumps({"locations": users_current_location,
                                    "error": ""})
                ws.send(output)
    # disconnected
    websocket_queue.remove(ws)
    ws.close()


@app.route('/v2/location/<group_id>/<username>/<query_date>', methods=['GET'])
@auth.login_required
def v2_get_user_locations(group_id, username, query_date):
    total_distance = 0
    user_locations = []
    status = 404
    user, group_usr_mapping = get_user_and_group_mapping(username, group_id)
    if (not user) or (not group_usr_mapping):
        status_message = "You have entered wrong username and or group"
        return jsonify(
            status=status,
            status_message=status_message,
            userLocations=user_locations,
            distance=total_distance
        )

    if not (group_usr_mapping.is_agent and \
            group_usr_mapping.has_agent_joined_group):
        status_message = "User has not joined as agent"
        return jsonify(
            status=status,
            status_message=status_message,
            userLocations=user_locations,
            distance=total_distance
        )
    query_date = parse_str_date_to_date_obj(query_date)
    if query_date > \
            group_usr_mapping.agent_joined_group_on.date():
        user_locations, total_distance = get_location_from_cache_or_db(
            query_date, username, user
        )
        status = 200
        status_message = "OK"
    elif query_date == \
            group_usr_mapping.agent_joined_group_on.date():
        user_locations, total_distance = get_location_distance_on_joining(
            user, query_date, group_usr_mapping.agent_joined_group_on.time()
        )
        status = 200
        status_message = "OK"
    else:
        status = 423
        status_message = "The resource that is been accessed is being locked"
    # send_formated_location = []
    for usr_location in user_locations:
        try:
            formated_time = datetime.datetime.strptime(
                str(usr_location['timestamp']), '%Y-%m-%dT%H:%M:%S.%fZ'
            ) + datetime.timedelta(hours=5, minutes=30)
        except:
            formated_time = datetime.datetime.strptime(
                str(usr_location['timestamp']), '%Y-%m-%d %H:%M:%S'
            ) + datetime.timedelta(hours=5, minutes=30)
        formated_time = datetime.datetime.strftime(
            formated_time, '%Y-%m-%d %H:%M:%S'
        )
        usr_location['timestamp'] = formated_time
        # send_formated_location.append(formated_location)

    return jsonify(
        status=status,
        status_message=status_message,
        userLocations=user_locations,
        distance=total_distance
    )


@app.route('/v2/stats/<group_id>/<query_date>', methods=['GET'])
@auth.login_required
def v2_get_group_agents_location(group_id, query_date):
    group_user_maps = GroupUserMapping.query.filter(
        and_(GroupUserMapping.group_id == group_id),
        (GroupUserMapping.is_agent == True),
    ).all()
    users_stat = []

    starting_point = None
    ending_point = None
    distance = 0

    group_user_maps = list(group_user_maps)
    query_date = parse_str_date_to_date_obj(query_date)
    for group_user_map in group_user_maps:
        if group_user_map.has_agent_joined_group:
            user = User.query.get(group_user_map.user_id)
            if query_date > \
            group_user_map.agent_joined_group_on.date():

                locations, distance = get_location_from_cache_or_db(
                    query_date, user.username, user
                )
            elif parse_str_date_to_date_obj(query_date) == \
                    group_user_map.agent_joined_group_on.date():
                locations, distance = get_location_distance_on_joining(
                    user, query_date,
                    group_user_map.agent_joined_group_on.time()
                )
            if len(locations) > 0:
                geolocator = Nominatim()
                starting_point = geolocator.reverse(
                    '{}, {}'.format(
                        locations[0]['latitude'], locations[0]['longitude']
                    )).address
                ending_point = geolocator.reverse(
                    '{}, {}'.format(
                        locations[-1]['latitude'], locations[-1]['longitude']
                    )).address
            stat = {
                'name': user.name,
                'phone': user.username,
                'avatar': user.avatar,
                'starting_point': starting_point,
                'ending_point': ending_point,
                'total_distance': distance,
                'agent_status': group_user_map.has_agent_joined_group
            }
            users_stat.append(stat)

    return jsonify(
        status=200,
        status_message="OK",
        users_stat=users_stat,
    )
