from odyssey import db


class Asset(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	admin = db.Column(db.Integer, db.ForeignKey('admin.id'))
    name = db.Column(db.String, nullable=False)
    asset_type = db.Column(db.String, nullable=True)
    capacity = db.Column(db.String, nullable=True)
    last_service_date = db.Column(db.Date, nullable=True)
    next_service_date = db.Column(db.Date, nullable=True)
    created_at = db.Column(db.Datetime, nullable=True)
    modified_at = db.Column(db.Datetime, nullable=True)