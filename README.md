# Odyssey server for Loktra app



### Setup:
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python setup.py
    setup AWS keys using aws-configure


### Run:
    sh runserver.sh