"""
before running this script make sure tables are ready:
	steps:
		1. take a production database dump
		2. restore database in new dadabase
		3. remove start_time and ends_time columns from group table
			use:
				ALTER TABLE "group" DROP COLUMN end_time;
				ALTER TABLE "group" DROP COLUMN start_time;
		4. add new columns created_by and created_at to group table:
			use:
				ALTER TABLE "group" ADD COLUMN created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
				ALTER TABLE "group" ADD created_by INTEGER CONSTRAINT FOREIGN_KEY REFERENCES "user" (id) ON DELETE CASCADE;
		5. add admin_status, agent_status, admin_act_on, agent_act_on columns to group_user_mapping columns:
			use:
				ALTER TABLE group_user_mapping ADD agent_act_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
				ALTER TABLE group_user_mapping ADD admin_act_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
				ALTER TABLE group_user_mapping ADD admin_status INTEGER DEFAULT 0;
				ALTER TABLE group_user_mapping ADD agent_status INTEGER DEFAULT 0;
"""
from odyssey import db
from odyssey.groups.models import Group, GroupUserMapping, UserRelationship

groups = Group.query.all()
for gr in groups:
    print gr.id
    ur = UserRelationship.query.filter_by(group_id=gr.id).first()
    if ur:
        print ur.user_id_one
        gr.created_by = ur.user_id_one
        db.session.commit()

gr_mappings = GroupUserMapping.query.all()
for gr_mapping in gr_mappings:
    ur = UserRelationship.query.filter(
        UserRelationship.group_id == gr_mapping.group_id,
        UserRelationship.action_user_id == gr_mapping.user_id).first()
    if ur:
        print ur.agent_status, ur.admin_status
        gr_mapping.agent_status = ur.agent_status
        gr_mapping.admin_status = ur.admin_status
        db.session.commit()
