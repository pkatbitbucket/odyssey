import os.path
import sys
import re
import datetime
import base64
import json
import unittest
from flask import g

import boto3
from sqlalchemy.sql import and_

sys.path.append(os.path.join(
    os.path.dirname(os.path.realpath(__file__)), os.pardir
    ))

import odyssey
from odyssey.user.models import User, Agent, Admin, VerificationCode
from odyssey.locations.models import UserLocation
from odyssey.groups.models import Group, GroupUserMapping
from odyssey import utils
from odyssey.user import views as user_views
from odyssey.groups import views as group_views
from odyssey.locations import views as loc_views
from odyssey.tasks import views as task_views
from odyssey.tasks.models import Task

print "working on ", os.environ['APP_SETTINGS']
if odyssey.app.config["TESTING"]:
    odyssey.app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['TEST_DATABASE_URL']

print "using database", odyssey.app.config['SQLALCHEMY_DATABASE_URI']


class SetUpTearDown(unittest.TestCase):

    def setUp(self):
        """Set up a blank temp database before each test"""
        self.app = odyssey.app.test_client()
        odyssey.db.drop_all()
        odyssey.db.create_all()

    def tearDown(self):
        """Destroy blank temp database after each test"""
        odyssey.db.session.commit()
        odyssey.db.session.close()
        odyssey.db.drop_all()


class ModelTestCase(SetUpTearDown):

    def test_user_setup(self):
        create_user()

    def test_agent_setup(self):
        create_user()
        create_agent()

    def test_admin_setup(self):
        create_user()
        create_admin()

    # def test_verification_code_setup(self):
    #     add_verification_code()

    def test_user_location_setup(self):
        create_user()

        location_test = UserLocation(
            user_id=1,
            latitude=90.77,
            longitude=90.90,
            accuracy=89.90,
            timestamp="2016-06-18 09:09:00",
            bearing=120
        )
        odyssey.db.session.add(location_test)
        odyssey.db.session.commit()

    def test_group_setup(self):
        create_user()
        create_group()

    def test_group_user_mapping(self):
        create_user()
        create_group()

        group_user_mapping_test = GroupUserMapping(
            group_id=1,
            user_id=1,
        )
        odyssey.db.session.add(group_user_mapping_test)
        odyssey.db.session.commit()

    def test_task_creation(self,):
        create_user()
        create_group()
        task_obj = Task(task_name="Packers and Movers",
                        task_creation_time="",
                        created_by="1234567898",
                        task_type="Multiple",
                           task_status="Unassigned")
        odyssey.db.session.add(task_obj)
        odyssey.db.commit.all()

class ViewFunTestCase(SetUpTearDown):

    def test_get_user_id_from_username(self):
        create_user()
        self.assertEquals(user_views.get_user_id_from_username(
            username='7506134455'
        ), 1)

    def test_get_user_from_username(self):
        create_user()
        self.assertEquals(isinstance(
            user_views.get_user_from_username('7506134455'), User
        ), True)

    def test_get_agent_from_username(self):
        create_user()
        create_agent()
        self.assertEquals(isinstance(
            user_views.get_agent_from_username('7506134455'), Agent
        ), True)

    def test_get_admin_from_username(self):
        create_user()
        create_admin()
        self.assertEquals(isinstance(
            user_views.get_admin_from_username('7506134455'), Admin
        ), True)

    def test_get_username_from_user_id(self):
        create_user()
        self.assertEquals(
            user_views.get_username_from_user_id('1'), '7506134455')

    def test_find_or_create_user(self):
        create_user()
        self.assertEquals(isinstance(
            user_views.find_or_create_user(
                username='7506134455', app_type='agent'
            ), User
        ), True)

        User.query.filter_by(id=1).delete()
        self.assertEquals(isinstance(
            user_views.find_or_create_user(
                username='7506134455', app_type='agent'
            ), User
        ), True)

    def test_find_or_create_admin(self):
        create_user()
        create_admin()
        self.assertEquals(isinstance(
            user_views.find_or_create_admin(1), Admin
        ), True)

        Admin.query.filter_by(user_id=1).delete()
        self.assertEquals(isinstance(
            user_views.find_or_create_admin(1), Admin
        ), True)

    def test_find_or_create_agent(self):
        create_user()
        create_agent()

        self.assertEquals(isinstance(
            user_views.find_or_create_agent(1), Agent
        ), True)

        Agent.query.filter_by(user_id=1).delete()
        self.assertEquals(isinstance(
            user_views.find_or_create_agent(user_id=1), Agent
        ), True)
    
    # group functions test 
    def test_update_or_create_group_user_mapping(self):
        create_user()
        create_group()
        self.assertEquals(isinstance(
            group_views.update_or_create_group_user_mapping(1, 1),
            GroupUserMapping
        ), True)

    def test_admin_notify_message(self):
        user = create_user()
        group = create_group()
        message = group_views.admin_notify_message(user, group, 2)
        self.assertEquals("You can track him now", message["notification_text"])

    def test_admin_invite_message(self):
        user = create_user()
        group = create_group()
        message = group_views.admin_invite_message(group, user)
        self.assertEquals("Enjoy the group activities", message["notification_text"])

    def test_agent_invite_message(self):
        user = create_user()
        group = create_group()
        message = group_views.agent_invite_message(group, user)
        self.assertEquals('Would you like to join?', message["notification_text"])

    def test_create_user_mapping(self):
        user_id = create_user().id
        group_id = create_group().id
        # Test agent add
        group_user_mapping = group_views.create_user_mapping(
            group_id, user_id, agent=True, admin=False, agent_status=1, admin_status=0
        )
        self.assertEquals(True, group_user_mapping.is_agent)
        self.assertEquals(False, group_user_mapping.is_admin)
        # Test admin add
        group_user_mapping = group_views.create_user_mapping(
            group_id, user_id, agent=False, admin=True, agent_status=0, admin_status=2
        )
        self.assertEquals(True, group_user_mapping.is_admin)
        self.assertEquals(False, group_user_mapping.is_agent)

    def test_update_user_mapping(self):
        user_id = create_user().id
        group_id = create_group().id
        # check if update dosnt change the accepted user to pending user
        group_user_mapping = group_views.create_user_mapping(
            group_id, user_id, agent=True, admin=False, agent_status=2, admin_status=0
        )
        group_user_mapping = group_views.update_user_mapping(
            group_user_mapping, agent=True, admin=False, agent_status=1, admin_status=0
        )
        self.assertEquals(2, group_user_mapping.agent_status)
        # check status update
        group_user_mapping = group_views.create_user_mapping(
            group_id, user_id, agent=True, admin=False, agent_status=1, admin_status=0
        )
        group_user_mapping = group_views.update_user_mapping(
            group_user_mapping, agent=True, admin=False, agent_status=2, admin_status=0
        )
        self.assertEquals(2, group_user_mapping.agent_status)

    def test_update_or_create_group_user_mapping(self):
        user_id = create_user().id
        group_id = create_group().id
        # check group_user_mapping creation
        group_user_mapping = group_views.update_or_create_group_user_mapping(
            group_id, user_id
        )
        self.assertEquals(isinstance(group_user_mapping, GroupUserMapping), True)
        # check group_user_mapping update
        group_user_mapping = group_views.create_user_mapping(
            group_id, user_id, agent=True, admin=False, agent_status=1, admin_status=0
        )
        group_user_mapping = group_views.update_or_create_group_user_mapping(
            group_id, user_id, agent=True, admin=False, agent_status=2, admin_status=0
        )
        self.assertEquals(2, group_user_mapping.agent_status)

    def test_add_member(self):
        user = create_user()
        group = create_group()
        user = group_views.add_member(
            group=group, user="917506134455", agent=False,
            admin=True, agent_status=0, admin_status=2
        )
        group_user_mapping = GroupUserMapping.query.filter(
            and_(GroupUserMapping.group_id==group.id),
            (GroupUserMapping.user_id==user.id)
        ).first()
        self.assertEquals(group_user_mapping.admin_status, 2)

    def test_remove_member(self):
        user = create_user()
        group = create_group()
        group_user_mapping = group_views.create_user_mapping(
            group.id, user.id, agent=True, admin=False, agent_status=1, admin_status=0
        )
        group_views.remove_member(group, user, "agent")
        group_user_mapping = GroupUserMapping.query.filter(
            and_(GroupUserMapping.group_id==group.id),
            (GroupUserMapping.user_id==user.id)
        ).all()
        self.assertEquals(0, len(group_user_mapping))

    def test_calculate_total_distance(self):
        create_user()
        add_locations()
        user_locations = UserLocation.query.all()
        dis = loc_views.calculate_total_distance(user_locations=user_locations)
        self.assertEquals(dis, 392.21671780659625)


class ViewTestCase(SetUpTearDown):

    def test_login(self):
        login = self.app.get('/v2/login/91/7506134455')
        assert b'200' in login.data
        assert b'OK' in login.data

    # def test_verification_over_call(self):
    #     call = self.app.get('/v2/login/91/7506134455')
    #     assert b'200' in call.data
    #     assert b'OK' in call.data

    # def test_verify_code(self):
    #     add_verification_code()
    #     verify_code = self.app.get(
    #         '/v2/login/agent/91/7506134455/100000')
    #     assert b'200' in verify_code.data
    #     assert b'OK' in verify_code.data
    #     verify_code = self.app.get(
    #         '/v2/login/agent/91/7506134455/1000000')
    #     assert b'400' in verify_code.data
    #     assert b'Invalid OTP' in verify_code.data

    def test_get_auth_token(self):
        add_verification_code()
        self.app.get('/v2/login/agent/91/7506134455/100000')
        token = self.app.get('/v2/token')
        if len(token.data) > 32:
            assert True
        if re.match('[a-zA-Z0-9_.-]', token.data):
            assert True
        else:
            assert False

    def test_update_admin_details(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        data = dict(
            name='PK',
            company='Loktra',
            description='Testing',
            email='pramod@loktra.com',
            avatar='/testing/s3/'
        )
        details = self.app.post(
            '/v2/admin',
            data=json.dumps(data),
            content_type = 'application/json',
            follow_redirects=True,
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'200' in details.data
        assert b'OK' in details.data

    def test_update_agent_details(self):
        add_verification_code()
        self.app.get('/v2/login/agent/91/7506134455/100000')
        data=dict(
            name='PK',
            driving_license='Get DL',
            avatar='/testing/s3/'
        )
        details = self.app.post(
            '/v2/admin',
            data=json.dumps(data),
            content_type = 'application/json',
            follow_redirects=True,
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'200' in details.data
        assert b'OK' in details.data

    def test_update_agent_gcm(self):
        add_verification_code()
        self.app.get('/v2/login/agent/91/7506134455/100000')

        gcm = self.app.post(
            '/v2/agent/fcm',
            data=json.dumps(dict(
                id='Random string'
            )),
            content_type = 'application/json',
            headers= {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'200' in gcm.data
        assert b'OK' in gcm.data

    def test_update_admin_gcm(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        gcm = self.app.post(
            '/v2/admin/fcm',
            data=json.dumps(dict(
                id='Random string'
            )),
            content_type = 'application/json',
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'200' in gcm.data
        assert b'OK' in gcm.data

    def test_get_locations(self):
        # login user
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        create_user()
        create_group()
        create_group_user_mapping_agent()
        add_locations()
        # try case
        loc = self.app.get(
            '/v2/location/1/917506134455/2016-07-27',
            headers={'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455'
            )
            }
        )
        assert b'The resource that is been accesed is being locked' in loc.data
        # except case
        loc = self.app.get(
            '/v2/location/1/917506134455/2016-06-24',
            headers={'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455'
            )
                     }
        )
        assert b"48.8567" not in loc.data
    
    # Test group views
    def test_initialize_admin(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        group_id = create_group().id
        user_id = User.query.all()[0].id
        group_views.update_or_create_group_user_mapping(
            group_id, user_id, agent=True, admin=True, agent_status=2,
            admin_status=2
        )
        response = self.app.get('/v2/admin/initialize',
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'members' in response.data

    def test_get_agent_groups(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        group_id = create_group().id
        user_id = User.query.all()[0].id
        group_views.update_or_create_group_user_mapping(
            group_id, user_id, agent=True, agent_status=2
        )
        groups = self.app.get('/v2/agent/groups',
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'groups' in groups.data

    def test_get_agent_groups(self):
        user_id = create_user().id
        group_id = create_group().id
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        group_views.update_or_create_group_user_mapping(
            group_id, user_id, agent_status=2
        )
        groups = self.app.get('/v2/agent/groups',
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'OK' in groups.data

    def test_get_pending_agent_requests(self):
        add_verification_code()
        self.app.get('/v2/login/agent/91/7506134455/100000')
        user = User.query.first()
        group = create_group()
        group_user_mapping = group_views.create_user_mapping(
            group.id, user.id, agent=True, admin=False, agent_status=1, admin_status=0
        )
        response = self.app.get('/v2/agent/requests',
            headers={'Authorization': 'Basic ' + base64.b64encode(
               '917506134455' + ":" + '917506134455'
        )})
        
        assert b'200' in response.data

    def test_create_new_group(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        created_group = self.app.post(
            '/v2/groups',
            data=json.dumps(dict(
                name='Test group',
                avatar='RAMA avatar',
                description='To test views',
                agents=["917506134455"],
                admins=["919799719444"]
            )),
            content_type = 'application/json',
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'200' in created_group.data

    def test_display_group(self):
        user_id = create_user().id
        group_id = create_group().id
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        group_views.update_or_create_group_user_mapping(
            group_id, user_id, agent_status=2
        )
        groups = self.app.get('/v2/groups/1',
            headers = {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        assert b'200' in groups.data

    def test_update_group_user_mapping(self):
        add_verification_code()
        self.app.get('/v2/login/agent/91/7506134455/100000')
        group_id = create_group().id
        user = User.query.all()[0]
        group_views.update_or_create_group_user_mapping(
            group_id, user.id, agent=True, agent_status=1
        )

        gcm = self.app.post(
            '/v2/agent/fcm',
            data=json.dumps(dict(
                id='Random string'
            )),
            content_type = 'application/json',
            headers= {'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            }
        )
        data = dict(
                group_id="1",
                status="2",
                app_type="agent"
            )
        self.app.put('/v2/groups/relationship',
            data=json.dumps(data),
            method='PUT',
            content_type = 'application/json',
            headers={'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455')
            },
        )
        user = User.query.all()[0]
        group_user_mapping = GroupUserMapping.query.filter(
            and_(GroupUserMapping.group_id == group_id),
            (GroupUserMapping.user_id == user.id)
        ).first()
        self.assertEquals(2, group_user_mapping.agent_status)

    def test_delete_group(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        group = create_group()
        group_res = self.app.delete('/v2/groups/1',
            data=dict(
                group_id='1'
            ),
            method='DELETE',
            headers={'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455'),
            },
            follow_redirects=True
        )
        groups = Group.query.all()
        self.assertEquals(0, len(groups))

    def test_display_group(self):
        add_verification_code()
        self.app.get('/v2/login/admin/91/7506134455/100000')
        group = create_group()
        group_res = self.app.get('/v2/groups/1',
            data=dict(
                group_id='1'
            ),
            headers={'Authorization': 'Basic ' + base64.b64encode(
                '917506134455' + ":" + '917506134455'),
            },
            follow_redirects=True
        )
        assert b'200' in group_res.data



class UtilsTestCase(unittest.TestCase):

    # def test_get_id(self):
    #     print "here is id len:", len(utils.get_id())
    #     if len(utils.get_id()) == 11:
    #         assert True
    #     elif len(utils.get_id()) == 12:
    #         assert True
    #     else:
    #         assert False

    def test_random_code(self):
        self.assertEquals(len(str(utils.generate_random_code())), 6)

    def test_n_digit_random_code(self):
        self.assertEquals(len(str(utils.generate_n_digit_random_code(10))), 10)

    # def test_send_sms(self):
    #     response = utils.send_to_phone('917506134455', '123456')
    #     if response[0] == 202:
    #         assert True
    #     elif response[0] == 200:
    #         assert True
    #     else:
    #         assert False

    def test_convert_no_to_text(self):
        out = utils.convert_number_to_text("0123456789")
        self.assertEquals(out, "zero one two three four five six seven eight nine")

    def test_write_to_s3(self):
        phone = '917506134455'
        code = '123456'
        utils.write_to_s3(phone, code)
        client = boto3.client('s3')
        read = client.get_object(
            Bucket='com.loktra.verification',
            Key=str(phone) + ".xml"
        )
        self.assertEquals(read['Body'].read().count('one two three four five six'), 3)

    # def test_call_to_phone(self):
    #     response = utils.call_to_phone('917506134455')
    #     if response[0] == 201:
    #         assert True
    #     elif response[0] == 200:
    #         assert True
    #     else:
    #         assert False

    def test_validate_phone(self):
        self.assertEquals(utils.validate_phone("+917506134455"), True)
        self.assertEqual(utils.validate_phone("+910000134455"), False)
        self.assertEquals(utils.validate_phone("12345678"), False)

    def test_format_phone_number(self):
        f_number = utils.format_phone_number("7506134455")
        self.assertEquals(f_number[:2], '91')


def create_user():
    user_test = User(
        username="7506134455",
        avatar="https://s3-ap-southeast-1.amazonaws.com/com.loktra.testavatars/u_1",
        password_hash="123456",
        is_admin=False,
        is_agent=True,
        name="Pramod",
        organization="Loktra",
        description="For testing",
        email="pramod@loktra.com",
        enabled=True,
    )
    odyssey.db.session.add(user_test)
    odyssey.db.session.commit()
    return user_test


def create_group():
    group_test = Group(
        name="Test Group",
        description="Test group",
        avatar="http://goo.gl/2pJynl",
        created_by=1
    )
    odyssey.db.session.add(group_test)
    odyssey.db.session.commit()
    return group_test


def create_group_user_mapping_agent():
    group_user_mapping = GroupUserMapping(
        group_id=1,
        user_id=1,
    )
    odyssey.db.session.add(group_user_mapping)
    odyssey.db.session.commit()
    group_user_mapping.make_agent()
    group_user_mapping.update_agent_status(2)
    group_user_mapping.update_agent_join_group()


def create_agent():
    agent_test = Agent(
            driving_license="pksingh12345",
            user_id=1,
        )
    odyssey.db.session.add(agent_test)
    odyssey.db.session.commit()


def create_admin():
    admin_test = Admin(
            user_id=1,
        )
    odyssey.db.session.add(admin_test)
    odyssey.db.session.commit()


def add_verification_code():
    code_test = VerificationCode(
            phone="917506134455",
            code="100000"
        )
    odyssey.db.session.add(code_test)
    odyssey.db.session.commit()


def add_locations():
    location1 = UserLocation(
        user_id=1,
        latitude=45.7597,
        longitude=4.8422,
        accuracy=01.09,
        timestamp='2016-07-28 21:09:00',
        bearing=120
    )
    location2 = UserLocation(
        user_id=1,
        latitude=48.8567,
        longitude=2.3508,
        accuracy=01.09,
        timestamp='2016-06-23 09:09:00',
        bearing=120
    )
    odyssey.db.session.add_all([location1, location2])
    odyssey.db.session.commit()

# def create_task():
#     Task()


if __name__ == '__main__':
    unittest.main()