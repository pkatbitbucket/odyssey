# -*- coding: utf-8 -*-

import unittest
import pyclbr
import re
import urlparse
import psycopg2
from app.config import CONNECTION_STRING


class DatabaseSetupTest(unittest.TestCase):

    def get_all_model_classes(self):
        user_models = [clss.name for clss in pyclbr.readmodule(
            'app.user.models').values()]

        location_models = [clss.name for clss in pyclbr.readmodule(
            'app.locations.models').values()]

        groups_models = [clss.name for clss in pyclbr.readmodule(
            'app.groups.models').values()]

        all_models = user_models
        all_models.extend(location_models)
        all_models.extend(groups_models)
        return all_models

    def check_if_table_exists(self, table_str):

        result = urlparse.urlparse(CONNECTION_STRING)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(
            database = database,
            user = username,
            password = password,
            host = hostname
        )

        cur = conn.cursor()
        cur.execute(
            "select exists(select relname from pg_class where relname='" + table_str + "')")
        exists = cur.fetchone()[0]
        print exists

    def get_table_name_from_class_name(self, class_name):
        return re.sub('(?<!^)(?=[A-Z])', '_', class_name).lower()

    def test_if_tables_created(self):
        for table in self.get_all_model_classes():
            if self.check_if_table_exists(self.get_table_name_from_class_name(table)):
                self.assertTrue
            else:
                self.assertFalse

if __name__ == '__main__':
    unittest.main()