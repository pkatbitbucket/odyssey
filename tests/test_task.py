import os.path
import sys
import re
import base64
import json
import unittest
from datetime import datetime

import boto3

sys.path.append(os.path.join(
    os.path.dirname(os.path.realpath(__file__)), os.pardir
    ))

import odyssey
from odyssey.user.models import User, Agent, Admin, VerificationCode
from odyssey.locations.models import UserLocation
from odyssey.groups.models import Group, GroupUserMapping
from odyssey import utils
from odyssey.user import views as user_views
from odyssey.groups import views as group_views
from odyssey.locations import views as loc_views
from odyssey.tasks import views as task_views
from odyssey.tasks.models import Task, SubTask

print "working on ", os.environ['APP_SETTINGS']
print "using database", odyssey.app.config['SQLALCHEMY_DATABASE_URI']


class SetUpTearDown(unittest.TestCase):

    def setUp(self):
        """Set up a blank temp database before each test"""
        self.app = odyssey.app.test_client()
        odyssey.db.drop_all()
        odyssey.db.create_all()

    def tearDown(self):
        """Destroy blank temp database after each test"""
        odyssey.db.session.commit()
        odyssey.db.session.close()
        odyssey.db.drop_all()


class TaskModelTests(SetUpTearDown):

    def test_task_setup(self):
        create_user()
        create_agent()
        create_task()

    def test_task_setup(self):
        create_user()
        create_agent()
        task_test = create_task()
        create_subtask(task_test)


def create_user():
    user_test = User(
        username="7506134455",
        avatar="https://s3-ap-southeast-1.amazonaws.com/com.loktra.testavatars/u_1",
        password_hash="123456",
        is_admin=False,
        is_agent=True,
        name="Pramod",
        organization="Loktra",
        description="For testing",
        email="pramod@loktra.com",
        enabled=True
    )
    odyssey.db.session.add(user_test)
    odyssey.db.session.commit()


def create_group():
    group_test = Group(
        name="Test Group",
        description="Test group",
        avatar="http://goo.gl/2pJynl",
        created_by=1
    )

    odyssey.db.session.add(group_test)
    odyssey.db.session.commit()


def create_agent():
    agent_test = Agent(
            driving_license="pksingh12345",
            user_id=1
    )

    odyssey.db.session.add(agent_test)
    odyssey.db.session.commit()


def create_admin():
    admin_test = Admin(
            user_id=1
    )

    odyssey.db.session.add(admin_test)
    odyssey.db.session.commit()


def create_task():
    task_test = Task(
        task_name="Sweets Packets",
        task_creation_time=datetime.now(),
        created_by='7506134455',
        task_type='SINGLE',
    )

    odyssey.db.session.add(task_test)
    odyssey.db.session.commit()

    return task_test


def create_subtask(task_obj):
    subtask_test1 = SubTask(
        subtask_type='PICK',
        parent_task=task_obj.id
    )
    subtask_test2 = SubTask(
        subtask_type='DROP',
        parent_task=task_obj.id
    )
    odyssey.db.session.add(subtask_test1)
    odyssey.db.session.add(subtask_test2)
    odyssey.db.session.commit()


if __name__ == '__main__':
    unittest.main()