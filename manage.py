# -*- coding: utf-8 -*-
import os
from odyssey import app, db
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, prompt_bool
from odyssey.exceptions import *

# import views
from odyssey.user.views import *
from odyssey.locations.views import *
from odyssey.groups.views import *
from odyssey.tasks.views import *
from odyssey.admin_dashboard.models import *
from odyssey.admin_dashboard.admin import admin
from odyssey.events.views import *
from odyssey.web_app.views import *
from odyssey.events.models import *
# import models
from odyssey.user import models as user_models
from odyssey.groups import models as group_models
from odyssey.locations import models as location_models
from odyssey.tasks import models as tasks_models
from odyssey.customer import models as customer_models
from odyssey.events import models as events_models

app.config.from_object(os.environ['APP_SETTINGS'])
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.command
def runserver():
    """ overridding the runserver command
    """
    app.run(host='0.0.0.0', port=8001, debug=True)


@manager.shell
def make_shell_context():
    return dict(
        app=app,
        db=db,
        user_models=user_models,
        group_models=group_models,
        location_models=location_models,
        tasks_models=tasks_models,
        customer_models=customer_models,
        events_models=events_models
    )

@manager.command
def create_admin():
    """Creates the admin user."""
    db.session.add(LoktraUser(
        email="pramod@loktra.com",
        password="admin",
        active=True,
    ))
    db.session.commit()

@manager.command
def drop_all():
    "Drops database tables"
    if prompt_bool("Are you sure you want to lose all your data"):
        db.drop_all()


@manager.command
def create_all():
    "Creates database tables"
    db.create_all()


@manager.command
def initialize_db():
    """
    Creates database tables as described in models. If table exists, it will delete those.
    :return: None
    """
    db.drop_all()
    db.create_all()

if __name__=='__main__':
    manager.run()
