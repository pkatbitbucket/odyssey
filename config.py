# -*- coding: utf-8 -*-
import os

basedir = os.path.abspath(os.path.dirname(__file__))

# ***********************************
# Settings all environments
"""
To make config work add following lines to your ~/.bashrc or ~/.zshrc
export APP_SETTINGS="config.DevelopmentConfig"
export DATABASE_URL="postgresql+psycopg2://odyssey:odyssey@localhost/homer"
export TEST_DATABASE_URL="postgresql+psycopg2://postgres:pksingh@localhost/test_new"
export APP_SECRET_KEY="\xb5\x1ePn\xc7\n\xcd\x96C\xc6\xa8\x05\xe9\x7fF\x8fz\xa1\x19\x17\x19\x10g\x85"
export AMAZON_S3_AVATAR_URL="https://s3-ap-southeast-1.amazonaws.com/com.loktra.avatars/"
export TEST_AMAZON_S3_AVATAR_URL="https://s3-ap-southeast-1.amazonaws.com/com.loktra.testavatars/"
export AMAZON_S3_VERIFICATION_CODE_URL="https://s3-ap-southeast-1.amazonaws.com/com.loktra.verification/"
export REDIS_URL="redis://localhost:6379/0"

"""
# ***********************************

VERSION_TWO = '/v2/'

PLIVO_AUTH_ID = "MAOWZMOWY3OGMWOTY0MD"
PLIVO_AUTH_TOKEN = "MTVjNzc3MDYxYTg2NTk1N2E3Y2M0ZTZjYzEyMDJi"
PLIVO_PHONE_NUMBER = "12094396463"
FCM_API_KEY = 'AIzaSyACv-ceTVmfjqM7dMQcqrzqMeR_HV5Og_s'
AGENT_APP_PLAY_STORE_LINK = "https://goo.gl/xq2EwM"
ADMIN_APP_PLAY_STORE_LINK = "https://goo.gl/c2JX6n"
LOGGING_API_KEY = "e4c5aeb4-aa23-4c4c-87d5-a3ea5238b34f"

SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
SECURITY_PASSWORD_SALT = 'poiuytresdfghjkloiuytrescvbnml;p98765rdcvbnmloiuytr'
SECURITY_EMAIL_SENDER = 'no-reply@loktra.com'
MAIL_SERVER = 'email-smtp.us-west-2.amazonaws.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'loktraserver@gmail.com'
MAIL_PASSWORD = 'LoktraServer1'


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SECRET_KEY = os.environ['APP_SECRET_KEY']
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    AMAZON_S3_AVATAR_URL = os.environ['AMAZON_S3_AVATAR_URL']
    AMAZON_S3_VERIFICATION_CODE_URL = os.environ[
        'AMAZON_S3_VERIFICATION_CODE_URL'
    ]
    REDIS_URL = os.environ['REDIS_URL']
    CELERY_BROKER_URL = REDIS_URL
    CELERY_RESULT_BACKEN = REDIS_URL
    LOCATION_LENGTH_MAX = 2
    LOCATION_FLUSH_TIME = 3600
    OPBEAT_ORGANIZATION_ID = 'b2edf9c17aff4f0dbed5b63b87060fea'
    OPBEAT_APP_ID = 'ca110eab1e'
    OPBEAT_SECRET_TOKEN = 'a94388d8b7c4aa5109cb5c047143d7fe83b54aa1'
    GPS_ACCURACY = 200


class ProductionConfig(Config):
    DEBUG = False
    LOCATION_LENGTH_MAX = 5


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    TESTING = True


class TestingConfig(Config):
    TESTING = True
    if os.environ.get('TEST_DATABASE_URL'):
        SQLALCHEMY_DATABASE_URI = os.environ['TEST_DATABASE_URL']
